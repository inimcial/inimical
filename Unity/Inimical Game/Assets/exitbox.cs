﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class exitbox : MonoBehaviour {

    ProtagController protag;
        
    private bool showExit = false;

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider col)
    {
        if(col.name.Contains("Protag") && showExit == true)
        {
            EndCondition();
        }
    }

    private void EndCondition()
    {
        foreach(GameObject obj in GameObject.FindGameObjectsWithTag("ExitVent"))
        {
            if(obj.name == "ExitSign")
            {
                obj.GetComponent<Renderer>().enabled = true;
            }
        }

    }

    public bool SetExitCondition(bool exitShow)
    {
        showExit = exitShow;
        return showExit;
    }
    
    public bool GetExitCondition()
    {
        return showExit;
    }
}
