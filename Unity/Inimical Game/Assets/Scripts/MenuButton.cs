﻿using UnityEngine;
using System.Collections;

public class MenuButton : MonoBehaviour {

    public string selectedChar;

    public void PerformAction()
    {
        if(name == "ThingButton")
        {
            AudioSource source = GetComponent<AudioSource>();
            source.Play();
            transform.GetChild(0).gameObject.SetActive(true);
        }
        if (name == "AntagButton")
            selectedChar = "Antag";
        if (name == "ProtagButton")
            selectedChar = "Protag";
    }
}
