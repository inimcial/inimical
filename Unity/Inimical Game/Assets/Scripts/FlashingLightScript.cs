﻿using UnityEngine;
using System.Collections;

public class FlashingLightScript : MonoBehaviour {

	public float duration = 1.0F;
	public float MaxIntensity = 1.0F;
	public Light lt;

	void Start() {
		lt = GetComponent<Light>();
		if (MaxIntensity > 8.0F) MaxIntensity = 8.0F;
		if (duration < 0.0F) duration = 0.1F; 
	}

	void Update() {
		float phi = Time.time / duration * 2 * Mathf.PI;
		float amplitude = Mathf.Cos(phi) * (MaxIntensity/2) + MaxIntensity/2;
		lt.intensity = amplitude;
	}

}
