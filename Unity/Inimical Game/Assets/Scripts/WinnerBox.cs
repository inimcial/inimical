﻿using UnityEngine;
using System.Collections;

public class WinnerBox : MonoBehaviour {

    void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("Protag"))
        {
            SpawnRoom spawnRoom = GameObject.Find("Network Manager").transform.FindChild("Spawn Room").GetComponent<SpawnRoom>();
            spawnRoom.CmdDoGameOver("Protagonist");
        }
    }
}
