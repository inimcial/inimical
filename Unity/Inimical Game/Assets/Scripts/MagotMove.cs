﻿using UnityEngine;
using System.Collections;

public class MagotMove : MonoBehaviour {

    NavMeshAgent nav;
    GameObject player;
    Transform protag;
    bool health = true;
    bool protagInRange = false;
    bool bitten = false;
    int biteDamage = 10;
    int biteCounter = 0;

    public Transform[] patrolPoints;

    private int patrolIndexMax = 0;
    private int patrolIndex = 0;

    [SerializeField]
    private float roamRadius = 100f;

    [SerializeField]
    private float idleAnimSpeed = 0.8f;

    [SerializeField]
    private float runAnimSpeed = 1.3f;

    Vector3 randomPosition;

    [SerializeField, FMODUnity.EventRef]
    private string[] maggotSounds;
    FMOD.Studio.EventInstance moveEvent;

    void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        randomPosition = transform.position;

        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GrubStretch"))
            GetComponent<Animator>().speed = idleAnimSpeed;

        patrolIndexMax = patrolPoints.Length;

        moveEvent = FMODUnity.RuntimeManager.CreateInstance(maggotSounds[Random.Range(0, maggotSounds.Length - 1)]);
        moveEvent.setVolume(0.6f);
        //moveEvent.start();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
        {
            protagInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject == player)
        {
            protagInRange = false;
        }
    }

    void PatrolCrawl()
    {
        if (patrolIndexMax < 2)
        {
            FreeRoam();
        }
        else
        {
            nav.SetDestination(patrolPoints[patrolIndex].position);
            if (nav.remainingDistance < 1.3F)
            {
                patrolIndex++;
                patrolIndex = patrolIndex % patrolIndexMax;
                nav.SetDestination(patrolPoints[patrolIndex].position);

                if (Random.value > 0.85f)
                    patrolIndex = Random.Range(0, patrolIndexMax);
            }
        }
    }

    void FreeRoam()
    {
        nav.SetDestination(randomPosition);
        Vector3 randomDirection = Random.insideUnitSphere * roamRadius;
        randomDirection += randomPosition;
        NavMeshHit hit;
        NavMesh.SamplePosition(randomDirection, out hit, roamRadius, 1);
        Vector3 finalPosition = hit.position;
        nav.destination = finalPosition;
    }

    // Update is called once per frame
    void Update()
    {

        FMOD.Studio.PLAYBACK_STATE moveEventPlaying;
        moveEvent.getPlaybackState(out moveEventPlaying);
        if(moveEventPlaying != FMOD.Studio.PLAYBACK_STATE.PLAYING)
        {
            moveEvent = FMODUnity.RuntimeManager.CreateInstance(maggotSounds[Random.Range(0, maggotSounds.Length - 1)]);
            moveEvent.set3DAttributes(FMODUnity.RuntimeUtils.To3DAttributes(gameObject));
            moveEvent.start();
        }
            


        if (!player)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject p in players)
            {
                if (p.name.Contains("Protag"))
                    player = p;
            }
        }
        else
        {
            if (!protag)
                protag = player.transform;

            if (health)
            {
                if (protagInRange)
                {
                    if (GetComponent<Animator>().speed != runAnimSpeed)
                        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GrubStretch"))
                            GetComponent<Animator>().speed = runAnimSpeed; 

                    nav.SetDestination(protag.position);

                    if (nav.remainingDistance < 1.4F) // if close enough to bite
                    {
                        if (bitten)
                        {
                            biteCounter++;  // if bitten, then do delay between bites
                            if (biteCounter > 180)
                            {
                                biteCounter = 0;
                                bitten = false;
                            }
                        }
                        else
                        {
                            bitten = true;
                            if (!player.GetComponent<ProtagController>().TooBitten())  //if not too bitten
                            {
                                player.GetComponent<ProtagController>().Cmd_DoBite(biteDamage); //Do bite for bite damage
                            }
                            else
                            {
                                protagInRange = false;
                                FreeRoam();
                                if (!player.GetComponent<ProtagController>().IsDead())
                                {
                                    player.GetComponent<ProtagController>().Cmd_Kill();
                                    BasePlayerController bpc = player.GetComponent<BasePlayerController>();
                                    //bpc.Cmd_SpawnAtEnd(0, player.gameObject);  // else send player to death area
                                    GameObject.Find("Spawn Room").transform.Find("Spawn Manager").GetComponent<SpawnManager>().EndGame(0);
                                }
                            }
                        }
                    }
                }
                else
                {
                     if (GetComponent<Animator>().speed != idleAnimSpeed)
                        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("GrubStretch"))
                            GetComponent<Animator>().speed = idleAnimSpeed; 

                    PatrolCrawl();
                }
            }
        }
    }
}
