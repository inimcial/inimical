﻿using UnityEngine;
using System.Collections;

public class StartingText : MonoBehaviour
{
    BasePlayerController protag;

    void update()
    {
        if (protag == null)
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (player.name.Contains("Protag"))
                {
                    protag = player.GetComponent<BasePlayerController>();
                }
            }
        }
    }
    

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.name.Contains("Protag"))
        {
            protag.SetTutorial(false);
            //Destroy(GameObject.FindGameObjectWithTag("AntagExit"));
        }

        else
        {
            Destroy(gameObject, 3f);
        }
        
    }

}
