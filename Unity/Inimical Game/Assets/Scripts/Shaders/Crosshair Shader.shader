﻿// File: MotionBlur.shader
// Author: Ross Brown - r.brown@qut.edu.au
// Updated: 30/03/2015
// Description: Implementation of simple resampling motion blur method.

Shader "Crosshair"
{
	Properties
	{
		_MainTex("Crosshair Texture", 2D) = "black" {}
		_cutoff("Alpha cutoff", float) = 0.1
	}

	SubShader
	{
		ZTest Always
		Pass
		{
			CGPROGRAM

			#pragma vertex VS_Crosshair 
			#pragma fragment PS_Crosshair
			#pragma target 3.0

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float _cutoff;

			float4 _MainTex_ST;

			float4 _remcol;


			struct VSInput
			{
				float4 pos	: POSITION;
				float2 tex	: TEXCOORD0;
			};

			struct VSOutput
			{
				float4 pos		: SV_POSITION;
				float2 tex		: TEXCOORD0;
			};

			//Vertex Shader
			VSOutput VS_Crosshair(VSInput input)
			{
				VSOutput output;

				output.pos = mul(UNITY_MATRIX_MVP, input.pos);

				//output.tex = input.tex;
				output.tex = TRANSFORM_TEX(input.tex, _MainTex);

				return output;
			}

			//Pixel Shader
			float4 PS_Crosshair(VSOutput a_Input) : COLOR
			{

				float4 colour = tex2D(_MainTex, a_Input.tex);
				if (colour.a < _cutoff)
					discard;

				//if (colour.r == _remcol.r && colour.g == _remcol.g && colour.b == _remcol.b)
				//	discard;

				return colour;
			}

				ENDCG
		}

		//GrabPass{}

		//Pass
		//{
		//	Blend SrcAlpha OneMinusSrcAlpha

		//	CGPROGRAM

		//	#pragma vertex MotionVS
		//	#pragma fragment MotionPS
		//	#pragma target 3.0

		//	sampler2D _GrabTexture;

		//	struct VSInput
		//	{
		//		float4 pos : POSITION;
		//		float4 tex : TEXCOORD0;
		//	};

		//	struct VSOutput
		//	{
		//		float4 pos : SV_POSITION;
		//		float4 tex : TEXCOORD0;
		//		float3 vel : TEXCOORD1;
		//	};

		//	VSOutput MotionVS(VSInput input)
		//	{
		//		VSOutput output;
		//		
		//		
		//	}

		//	ENDCG
		//}


		//GrabPass{} // Quick to program, slow as a wet week to use, be aware of this when using GrabPass.

		//Pass
		//{ 

		//	Blend SrcAlpha OneMinusSrcAlpha     // Alpha blending

		//	CGPROGRAM 

		//	#pragma vertex MotionVS 
		//	#pragma fragment MotionPS
		//	#pragma target 3.0


		//	sampler2D _GrabTexture;
		//	uniform float4x4 _presMV;
		//	uniform float4x4 _prevMV;
		//	uniform float4x4 _prevP;

		//	struct VSInput
		//	{
		//		float4 pos: POSITION;
		//		float3 norm: NORMAL;
		//		float2 tex: TEXCOORD0;
		//	};

		//	// motion blur vertex shader output
		//	struct VSMotionOutput
		//	{
		//		float4 pos: SV_POSITION;
		//		float4 tex: TEXCOORD0;
		//		float3 vel: TEXCOORD1;
		//	};

		//	VSMotionOutput MotionVS(VSInput a_Input)
		//	{
		//		VSMotionOutput Output;
		//		float4x4 prevMVP;
		//		float4x4 presMVP;
		//		float g_fBlurFactor = 2.0f;  // How big is the deviation from the original size.
		//		bool g_bFrontBlur = true;

		//		// calculate world view positions
		//		float4 pos = mul(_presMV, a_Input.pos);
		//		float4 posPrev = mul(_prevMV, a_Input.pos); //GET PREVIOUS FROM C#

		//		float3 normal = mul(transpose(_World2Object), float4(a_Input.norm, 0.0f)).xyz;
		//		float3 vecMotion = pos.xyz - posPrev.xyz;

		//		prevMVP = mul(_prevMV, _prevP);
		//		presMVP = mul(_presMV, _prevP);

		//		// calculate world view projection positions
		//		pos = mul(presMVP, a_Input.pos);
		//		posPrev = mul(prevMVP, a_Input.pos);  //GET PREVIOUS FROM C#

		//		// is vertex on front of object in relation to current motion
		//		bool bFront = dot(vecMotion, normal) > 0.0f;

		//		// new position depending on vertex facing
		//		float4 newPos;

		//		// if vertex is at front, leave as it
		//		if (bFront) {
		//			newPos = pos;
		//		}
		//		else {
		//			// otherwise set vertex as previous position to stretch object
		//			newPos = pos + (posPrev - pos) * g_fBlurFactor;
		//		}

		//		// output vertex position
		//		Output.pos = newPos;

		//		// transform to normalized device coordinates
		//		newPos.xyz /= newPos.w;
		//		//newPos.y *= -1; // Remove comment if using DX (not Unity 3D) due to inverse y on image access operations.
		//		Output.tex = (newPos + 1) / 2;

		//		pos.xyz /= pos.w;
		//		//pos.y *= -1;
		//		pos = (pos + 1) / 2;

		//		posPrev.xyz /= posPrev.w;
		//		//posPrev.y *= -1;
		//		posPrev = (posPrev + 1) / 2;

		//		// calculate vertex velocity
		//		Output.vel = (pos.xyz - posPrev.xyz) * g_fBlurFactor;

		//		// if not rendering front blur and front vertex, set velocity to zero
		//		if (!g_bFrontBlur && bFront)
		//			Output.vel = float3(0.0f, 0.0f, 0.0f);

		//		return Output;
		//	}

		//	// motion blur pixel shader
		//	float4 MotionPS(VSMotionOutput a_Input) : COLOR
		//	{
		//		float4 colour = float4(0.0f, 0.0f, 0.0f, 1.0f);

		//		// number of samples for each pixel
		//		int nSampleCount = 16;

		//		// sample strength
		//		float fSampleStr = 1.0f / nSampleCount;

		//		for (int i = 0; i < nSampleCount; i++)
		//		{
		//			// velocity offset
		//			float fVelPos = (float)i / ((float)nSampleCount - 1);

		//			// increment colour
		//			colour += tex2D(_GrabTexture, a_Input.tex.xy + a_Input.vel.xy * fVelPos) * fSampleStr;
		//			//colour += tex2D(_GrabTexture, a_Input.tex.xy + float2(200.0, 0.0) * fVelPos) * fSampleStr;
		//		}

		//		return float4(colour.rgb, 0.5f);
		//	}

		//	ENDCG
		//}
	}
}
