﻿Shader "Menu"
{
	Properties
	{
		_tex1("Texture", 2D) = "black" {}
		_cutoff("Alpha cutoff", float) = 0.1
	}

	SubShader
	{
		ZTest Always
		Pass
		{
			CGPROGRAM

			#pragma vertex VS_Crosshair 
			#pragma fragment PS_Crosshair
			#pragma target 3.0

			sampler2D _tex1;
			float _cutoff;

			struct VSInput
			{
				float4 pos	: POSITION;
				float4 tex	: TEXCOORD0;
			};

			struct VSOutput
			{
				float4 pos		: SV_POSITION;
				float4 tex		: TEXCOORD0;
			};

			//Vertex Shader
			VSOutput VS_Crosshair(VSInput input)
			{
				VSOutput output;

				output.pos = mul(UNITY_MATRIX_MVP, input.pos);

				output.tex = input.tex;

				return output;
			}

			//Pixel Shader
			float4 PS_Crosshair(VSOutput a_Input) : COLOR
			{
				float4 colour = tex2D(_tex1, a_Input.tex);
				if (colour.a < _cutoff)
					discard;

				return colour;
			}

				ENDCG
		}
	}
}
