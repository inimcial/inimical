﻿using UnityEngine;
using System.Collections;

public class AlwaysFacePlayer : MonoBehaviour {

    GameObject player;


	// Update is called once per frame
	void Update () {
        if (!player)
            SearchForPlayer();
        else
            transform.LookAt(player.transform);
	}

    void SearchForPlayer()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject p in players)
        {
            if (p.name.Contains("Protag"))
                player = p;
        }
    }
}
