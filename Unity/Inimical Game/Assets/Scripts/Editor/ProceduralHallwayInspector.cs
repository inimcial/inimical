﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ProceduralHallway))]
public class ProceduralHallwayInspector : Editor
{
    SerializedProperty hallLengthProp;
    SerializedProperty hallSegmentProp;
    SerializedProperty hallCornerProp;
    SerializedProperty hallTProp;
    SerializedProperty hallCrossProp;
    SerializedProperty hallStartCap;
    SerializedProperty hallEndCap;

    ProceduralHallway currentHallway;

    private bool showPrefabChange;

    public void OnEnable()
    {
        currentHallway = (ProceduralHallway)target;

        hallLengthProp = serializedObject.FindProperty("hallwayLength");
        hallSegmentProp = serializedObject.FindProperty("hallwaySegment");
        hallCornerProp = serializedObject.FindProperty("hallCorner");
        hallTProp = serializedObject.FindProperty("hallTIntersection");
        hallCrossProp = serializedObject.FindProperty("hallCrossroad");
        hallStartCap = serializedObject.FindProperty("startCapType");
        hallEndCap = serializedObject.FindProperty("endCapType");

    }

    public override void OnInspectorGUI()
    {
        //Always do this first
        serializedObject.Update();

        //Custom GUI
        EditorGUILayout.IntSlider(hallLengthProp, 1, 50, "Hallway Length");

        hallStartCap.enumValueIndex = EditorGUILayout.Popup("Start Cap Type", hallStartCap.enumValueIndex, new string[] { "Left Turn", "Right Turn", "T Intersection", "Crossroads", "None"});
        hallEndCap.enumValueIndex = EditorGUILayout.Popup("End Cap Type", hallEndCap.enumValueIndex, new string[] { "Left Turn", "Right Turn", "T Intersection", "Crossroads", "None"});

        showPrefabChange = EditorGUILayout.ToggleLeft("Change Prefabs", showPrefabChange);
        if (showPrefabChange)
        {
            hallSegmentProp.objectReferenceValue = EditorGUILayout.ObjectField("Hall Segment", hallSegmentProp.objectReferenceValue, typeof(GameObject), true);
            hallCornerProp.objectReferenceValue = EditorGUILayout.ObjectField("Hall Corner", hallCornerProp.objectReferenceValue, typeof(GameObject), true);
            hallTProp.objectReferenceValue = EditorGUILayout.ObjectField("Hall T Intersection", hallTProp.objectReferenceValue, typeof(GameObject), true);
            hallCrossProp.objectReferenceValue = EditorGUILayout.ObjectField("Hall Crossroad", hallCrossProp.objectReferenceValue, typeof(GameObject), true);
        }

        //Apply Edits
        serializedObject.ApplyModifiedProperties();

        if(GUI.changed)
            currentHallway.RefreshHall();
    }
}