﻿using UnityEngine;
using UnityEditor;

[CanEditMultipleObjects]
[CustomEditor(typeof(EditableWall))]
public class EditableWallInspector : Editor
{
    SerializedProperty wallTypeProp;
    SerializedProperty blankWallProp;
    SerializedProperty singleDoorProp;
    SerializedProperty doubleDoorProp;
    SerializedProperty windowWallProp;
    SerializedProperty capsWallProp;

    SerializedProperty airconTypeProp;
    SerializedProperty mediumAirconProp;
    SerializedProperty noAirconProp;
    SerializedProperty largeAirconProp;

    private EditableWall currentWall;
    private bool showPrefabChange;

    public void OnEnable()
    {
        currentWall = (EditableWall)target;

        wallTypeProp = serializedObject.FindProperty("wallType");
        blankWallProp = serializedObject.FindProperty("blankWall");
        singleDoorProp = serializedObject.FindProperty("singleDoor");
        doubleDoorProp = serializedObject.FindProperty("doubleDoor");
        windowWallProp = serializedObject.FindProperty("windowWall");
        capsWallProp = serializedObject.FindProperty("capsWall");

        airconTypeProp = serializedObject.FindProperty("airconType");
        mediumAirconProp = serializedObject.FindProperty("mediumAircon");
        largeAirconProp = serializedObject.FindProperty("largeAircon");
        noAirconProp = serializedObject.FindProperty("noAircon");


    }

    public override void OnInspectorGUI()
    {
        //Always do this first
        serializedObject.Update();
        //EditorGUI.BeginChangeCheck();

        wallTypeProp.enumValueIndex = EditorGUILayout.Popup("Wall Type", wallTypeProp.enumValueIndex, new string[] {"Blank", "Window", "Single Door", "Double Door", "Caps Wall"});
        airconTypeProp.enumValueIndex = EditorGUILayout.Popup("Aircon Type", airconTypeProp.enumValueIndex, new string[] {"Medium", "Large", "None"});

        showPrefabChange = EditorGUILayout.ToggleLeft("Change Prefabs", showPrefabChange);

        if (showPrefabChange)
        {
            blankWallProp.objectReferenceValue = EditorGUILayout.ObjectField("Blank Wall", blankWallProp.objectReferenceValue, typeof(GameObject), true);
            windowWallProp.objectReferenceValue = EditorGUILayout.ObjectField("Window Wall", windowWallProp.objectReferenceValue, typeof(GameObject), true);
            singleDoorProp.objectReferenceValue = EditorGUILayout.ObjectField("Single Door", singleDoorProp.objectReferenceValue, typeof(GameObject), true);
            doubleDoorProp.objectReferenceValue = EditorGUILayout.ObjectField("Double Door", doubleDoorProp.objectReferenceValue, typeof(GameObject), true);
            capsWallProp.objectReferenceValue = EditorGUILayout.ObjectField("Caps Wall", capsWallProp.objectReferenceValue, typeof(GameObject), true);

            mediumAirconProp.objectReferenceValue = EditorGUILayout.ObjectField("Medium Aircon", mediumAirconProp.objectReferenceValue, typeof(GameObject), true);
            largeAirconProp.objectReferenceValue = EditorGUILayout.ObjectField("Large Aircon", largeAirconProp.objectReferenceValue, typeof(GameObject), true);
            noAirconProp.objectReferenceValue = EditorGUILayout.ObjectField("No Aircon", noAirconProp.objectReferenceValue, typeof(GameObject), true);
        }
        
        //Apply Edits
        serializedObject.ApplyModifiedProperties();

        //if(EditorGUI.EndChangeCheck())
        //{
        //    foreach (Object t in targets)
        //    {
        //        EditableWall ew = t as EditableWall;


        //    }
        //}


        if(GUI.changed)
            if(targets.Length > 1)
                foreach (EditableWall t in targets)
                {
                    t.RefreshWall();
                }
            else
                currentWall.RefreshWall();
    }
}
