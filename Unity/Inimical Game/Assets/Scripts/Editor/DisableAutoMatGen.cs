﻿using UnityEditor;

public class DisableAutoMatGen : AssetPostprocessor{

	public void OnPreprocessModel()
    {
        ModelImporter importer = (ModelImporter)assetImporter;
        importer.importMaterials = false;
    }
}
