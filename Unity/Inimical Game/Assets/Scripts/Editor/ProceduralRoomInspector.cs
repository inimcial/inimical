﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ProceduralRoom))]
public class ProceduralRoomInspector : Editor {

    SerializedProperty widthProp;
    SerializedProperty heightProp;
    SerializedProperty wallProp;
    SerializedProperty cornerProp;
    SerializedProperty floorProp;

    private ProceduralRoom currentRoom;
    private bool showPrefabChange;

    public void OnEnable()
    {
        currentRoom = (ProceduralRoom)target;

        widthProp = serializedObject.FindProperty("roomWidth");
        heightProp = serializedObject.FindProperty("roomHeight");
        wallProp = serializedObject.FindProperty("wallPiece");
        cornerProp = serializedObject.FindProperty("wallCorner");
        floorProp = serializedObject.FindProperty("floorCeiling");
    }

    public override void OnInspectorGUI()
    {
        //Always do this first
        serializedObject.Update();

        //wallTypeProp.enumValueIndex = EditorGUILayout.Popup("Wall Type", wallTypeProp.enumValueIndex, new string[] { "Blank", "Single Door", "Double Door" });

        EditorGUILayout.IntSlider(widthProp, 2, 12, "Room Width (x)");
        EditorGUILayout.IntSlider(heightProp, 2, 12, "Room Height (z)");

        showPrefabChange = EditorGUILayout.ToggleLeft("Change Prefabs", showPrefabChange);
        if (showPrefabChange)
        {
            wallProp.objectReferenceValue = EditorGUILayout.ObjectField("Wall Piece", wallProp.objectReferenceValue, typeof(GameObject), true);
            cornerProp.objectReferenceValue = EditorGUILayout.ObjectField("Corner Piece", cornerProp.objectReferenceValue, typeof(GameObject), true);
            floorProp.objectReferenceValue = EditorGUILayout.ObjectField("Floor and Ceiling", floorProp.objectReferenceValue, typeof(GameObject), true);
        }


        //Apply Edits
        serializedObject.ApplyModifiedProperties();

        if(GUI.changed)
            currentRoom.RefreshRoom();
    }

}
