﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System.IO;

public class NewImportWizard : ScriptableWizard
{

    private static GameObject origModel;

    private static Texture2D diffuseTex;
    private static Texture2D normalTex;
    private static Texture2D specularTex;
    private static Texture2D ambientTex;


    private static RuntimeAnimatorController animController;

    private static string prefabName;
    private static string materialName;

    private static bool showOptionalOptions;
    private static bool hasAnimation;
    private static bool showGenAnimContDialog;

    private TextureImporter texImporter;
    private ModelImporter modImporter;

    private GameObject newPrefab;
    private Material newMat;

	private string prefabLoc;
	private string materialLoc;


    [MenuItem("Steven/New Import Wizard")]
    static void CreateWizard()
    {
        ScriptableWizard.DisplayWizard<NewImportWizard>("Import Wizard", "Close Without Saving", "Apply & Save");

        origModel = null; diffuseTex = null; normalTex = null; specularTex = null; ambientTex = null; animController = null;
		prefabName = ""; materialName = "";
        showOptionalOptions = false; hasAnimation = false; showGenAnimContDialog = true;

        Object[] selectedObjects = Selection.GetFiltered(typeof(Object), SelectionMode.Assets);
        if (selectedObjects.Length != 0 && selectedObjects != null)
        {
            foreach (Object obj in selectedObjects)
            {
                if (obj is GameObject)
                {
                    origModel = (GameObject)obj;
                    prefabName = origModel.name + "_Prefab";
				}

                if (obj is Texture2D)
                {
                    string type = obj.name.Split('_')[obj.name.Split('_').Length - 1];
                    if (type == "diffuse") { diffuseTex = (Texture2D)obj; }
                    if (type == "normal") { normalTex = (Texture2D)obj; }
                    if (type == "specular") { specularTex = (Texture2D)obj; }
                    if (type == "ao") { ambientTex = (Texture2D)obj; }

                    if (origModel != null)
                        materialName = origModel.name + "_Material";
                }

                if (obj is RuntimeAnimatorController)
                {
                    animController = (RuntimeAnimatorController)obj;
                }
            }
        }

    }

    void OnWizardCreate()
    {
        Debug.Log("Neat.");
    }

    void OnWizardOtherButton()
    {
		if (!Directory.Exists ("Assets/Prefabs/" + materialLoc))
			Directory.CreateDirectory ("Assets/Prefabs/" + materialLoc);

        //Create Material
		string matPath = "Assets/Prefabs/" + materialLoc + materialName + ".mat";
        newMat = new Material(Shader.Find("Standard (Specular setup)"));

        if (diffuseTex != null) { newMat.SetTexture("_MainTex", diffuseTex); }
        if (normalTex != null) { newMat.SetTexture("_BumpMap", normalTex); }
        if (specularTex != null) { newMat.SetTexture("_SpecGlossMap", specularTex); }
        if (ambientTex != null) { newMat.SetTexture("_OcclusionMap", ambientTex); }

        AssetDatabase.CreateAsset(newMat, matPath);



        //Create Prefab
		string prefabPath = "Assets/Prefabs/" + prefabLoc + prefabName + ".prefab";

        GameObject newGO= Instantiate(origModel);
        newPrefab = PrefabUtility.CreatePrefab(prefabPath, newGO);
        DestroyImmediate(newGO);

        //Attach Material to Prefab
        Renderer[] renderers = newPrefab.transform.GetComponentsInChildren<Renderer>(true);
        if(renderers != null && renderers.Length > 0)
        {
            foreach(Renderer renderer in renderers)
            {
                renderer.material = newMat;
            }
        }
        else
        {
            Renderer newRenderer = newPrefab.AddComponent<Renderer>();
            newRenderer.material = newMat;
        }

        //Attach AnimController if needed
        if(animController != null)
        {
            newPrefab.GetComponent<Animator>().runtimeAnimatorController = animController;
        }

    }

    protected override bool DrawWizardGUI()
    {
        prefabName = EditorGUILayout.TextField("Prefab Name:", prefabName);
        materialName = EditorGUILayout.TextField("Material Name:", materialName);

        EditorGUILayout.Separator(); EditorGUILayout.Separator();

        diffuseTex = (Texture2D)EditorGUILayout.ObjectField("Diffuse Texture:", diffuseTex, typeof(Texture2D), true);
        normalTex = (Texture2D)EditorGUILayout.ObjectField("Normal Texture:", normalTex, typeof(Texture2D), true);
        specularTex = (Texture2D)EditorGUILayout.ObjectField("Specular Texture:", specularTex, typeof(Texture2D), true);
        ambientTex = (Texture2D)EditorGUILayout.ObjectField("Ambient Texture:", ambientTex, typeof(Texture2D), true);

        origModel = (GameObject)EditorGUILayout.ObjectField("Model:", origModel, typeof(GameObject), true);

        EditorGUILayout.Separator(); EditorGUILayout.Separator(); EditorGUILayout.Separator(); EditorGUILayout.Separator();


        if(hasAnimation && animController == null && showGenAnimContDialog)
        {
            showOptionalOptions = true;
            EditorGUILayout.LabelField("Animation data detected, generate empty Animator Controller?");
            
            EditorGUILayout.BeginHorizontal();

            Rect r1 = EditorGUILayout.BeginHorizontal();
            if(GUI.Button(r1, GUIContent.none))
            {
				string path = "Assets/Prefabs/" + prefabLoc + origModel.name + "_AnimController.controller";
                if (!Directory.Exists("Assets/Prefabs/" + prefabLoc))
                    Directory.CreateDirectory("Assets/Prefabs/" + prefabLoc);
                animController = UnityEditor.Animations.AnimatorController.CreateAnimatorControllerAtPath(path);
            }
            EditorGUILayout.LabelField("Yes");
            EditorGUILayout.EndHorizontal();

            Rect r2 = EditorGUILayout.BeginHorizontal();
            if (GUI.Button(r2, GUIContent.none))
                showGenAnimContDialog = false;
            EditorGUILayout.LabelField("No");
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.EndHorizontal();
        }

        EditorGUILayout.Separator();

        showOptionalOptions = EditorGUILayout.ToggleLeft("Show Optional Options", showOptionalOptions);

        //Extra block, can be toggled On/Off
        if (showOptionalOptions)
        {
            EditorGUI.indentLevel++;
            animController = (RuntimeAnimatorController)EditorGUILayout.ObjectField("Animator Controller:", animController, typeof(RuntimeAnimatorController), true);

            EditorGUI.indentLevel--;
        }


        if (materialName == "")
            if (origModel != null && materialName != origModel.name + "_Material")
                materialName = origModel.name + "_Material";
        if (prefabName == "")
            if (origModel != null && prefabName != origModel.name + "_Prefab")
                prefabName = origModel.name + "_Prefab";

        if (normalTex != null && texImporter == null)
        {
            string normPath = AssetDatabase.GetAssetPath(normalTex);
            texImporter = AssetImporter.GetAtPath(normPath) as TextureImporter;
            texImporter.textureType = TextureImporterType.Bump;
            AssetDatabase.WriteImportSettingsIfDirty(normPath);
        }

        if (origModel != null)
        {
            string assetPath = AssetDatabase.GetAssetPath(origModel);
            modImporter = AssetImporter.GetAtPath(assetPath) as ModelImporter;
            if (modImporter.animationType != ModelImporterAnimationType.None)
            {
                hasAnimation = true;
            }

			prefabLoc = assetPath.Substring (14, assetPath.Length - origModel.name.Length - 18);
			materialLoc = prefabLoc + "Material/";
        }


        return false;
    }
}



#endif