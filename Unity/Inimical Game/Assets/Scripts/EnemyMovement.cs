﻿using UnityEngine;

public class EnemyMovement : MonoBehaviour {

	NavMeshAgent nav;
	GameObject player;
	Transform protag;
	bool health = true;
	bool protagInRange = false;
    bool bitten = false;
    int biteCounter = 0;
    int biteDamage = 10;

    [SerializeField]
	private float roamRadius = 50f;

    [SerializeField]
    private float idleAnimSpeed = 0.8f;

    [SerializeField]
    private float runAnimSpeed = 1.3f;

	Vector3 randomPosition;

    [SerializeField, FMODUnity.EventRef]
    private string[] babySounds;

	void Awake()
	{
		nav = GetComponent <NavMeshAgent>();
		randomPosition = transform.position;


        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("crawl"))
            GetComponent<Animator>().speed = idleAnimSpeed;

    }

	void OnTriggerEnter (Collider other) {
		if (other.gameObject == player) {
			protagInRange = true;
		}
	}

	void OnTriggerExit (Collider other){
		if (other.gameObject == player) {
			protagInRange = false;
		}
	}

	void FreeRoam()
	{
		nav.SetDestination (randomPosition);
		Vector3 randomDirection = Random.insideUnitSphere * roamRadius;
		randomDirection += transform.position;
		NavMeshHit hit;
		NavMesh.SamplePosition(randomDirection, out hit, roamRadius, 1);
		Vector3 finalPosition = hit.position;        
		nav.destination = finalPosition;
	}
	
	// Update is called once per frame
	void Update () {

        if (!player)
        {
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject p in players)
            {
                if (p.name.Contains("Protag"))
                    player = p;
            }
        }
        else
        {
            if(!protag)
                protag = player.transform;

            if (health)
            {
                if (protagInRange)
                {
                    if(GetComponent<Animator>().speed != runAnimSpeed)
                        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("crawl"))
                            GetComponent<Animator>().speed = runAnimSpeed;

                    nav.SetDestination(protag.position);

                    if (nav.remainingDistance < 1.4F) // if close enough to bite
                    {
                        if (bitten)
                        {
                            biteCounter++;  // if bitten, then do delay between bites
                            if (biteCounter > 180)
                            {
                                biteCounter = 0;
                                bitten = false;
                            }
                        }
                        else
                        {
                            
                            bitten = true;
                            if (!player.GetComponent<ProtagController>().TooBitten())  //if not too bitten
                            {
                                FMODUnity.RuntimeManager.PlayOneShotAttached(babySounds[Random.Range(0, babySounds.Length-1)], gameObject);
                                player.GetComponent<ProtagController>().Cmd_DoBite(biteDamage); //Do bite for bite damage

                            }
                            else
                            {
                                protagInRange = false;
                                FreeRoam();
                                if (!player.GetComponent<ProtagController>().IsDead())
                                {
                                    player.GetComponent<ProtagController>().Cmd_Kill();
                                    BasePlayerController bpc = player.GetComponent<BasePlayerController>();
                                    GameObject.Find("Spawn Room").transform.Find("Spawn Manager").GetComponent<SpawnManager>().EndGame(0);
                                    //bpc.Cmd_SpawnAtEnd(0, player.gameObject);  // else send player to death area
                                }
                            }     
                        }
                    }
                }
                else
                {
                    if (GetComponent<Animator>().speed != idleAnimSpeed)
                        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("crawl"))
                            GetComponent<Animator>().speed = idleAnimSpeed;

                    FreeRoam();
                }
            }
        }
    }
}
