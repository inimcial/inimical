﻿using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;
using UnityEngine.SceneManagement;
using System;

using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

[RequireComponent(typeof(PlayerMotor))]
public class BasePlayerController : NetworkBehaviour
{
    public bool tutorialsOn;

    [SyncVar]
    public float speedModifier = 1;

    protected PlayerMotor motor;

    [SyncVar]
    public string endText = "";

    [SyncVar]
    protected Vector3 velocity;

    protected bool showMenu;

    [SyncVar]
    public string character;

    [SerializeField]
    protected float walkSpeed = 3;
    [SerializeField]
    protected float runSpeed = 6;
    [SerializeField]
    protected float lookSensitivity = 2;

    protected GameObject cam;

    public static bool screenOverlayShowing = false;

    protected float startingLookSensitivity;

    public NetworkConnection localConn;

    [SerializeField, FMODUnity.EventRef]
    private string protagWinMusic = "event:/Music/Good Ending Scene";

    [SerializeField, FMODUnity.EventRef]
    private string antagWinMusic = "event:/Music/Bad Ending Scene";

    private string finalText = "Meme";
    //[SerializeField]
    //protected float fadeToBlackSpeed = 2;

    //protected GameObject ftbSphere;
    //protected Renderer ftbsRenderer;

    protected Vector3 CalculateVelocity(float stamina, bool staminaIsRecharging)
    {
        Vector3 vel;
        //Calculate movement velocity as a 3D vector
        float xMove = Input.GetAxisRaw("Horizontal");
        float zMove = Input.GetAxisRaw("Vertical");

        Vector3 moveHorizontal = transform.right * xMove;
        Vector3 moveVertical = transform.forward * zMove;

        //Movement Velocity
		bool isRunHeld = (stamina != 0 && !staminaIsRecharging) ? Input.GetButton("Run") : false;

        vel = (moveHorizontal + moveVertical).normalized * ((isRunHeld ? runSpeed : walkSpeed) * speedModifier);
        return vel;
    }

    internal void OnNetworkDestroy(GameObject gameObject)
    {
        throw new NotImplementedException();
    }

    protected void DoMovement(float stamina, bool staminaIsRecharging)
    {
		velocity = CalculateVelocity(stamina, staminaIsRecharging);

        //if (velocity != Vector3.zero)
		GetComponent<Animator>().SetFloat("forwardVelocity", Input.GetAxisRaw("Vertical"));

        if (Input.GetAxisRaw("Horizontal") != 0)
            GetComponent<Animator>().SetFloat("sideVelocity", Input.GetAxisRaw("Horizontal"));
        else if (Input.GetAxisRaw("LookHorizontal") > 0)
            GetComponent<Animator>().SetFloat("sideVelocity", 1);
        else if (Input.GetAxisRaw("LookHorizontal") < 0)
            GetComponent<Animator>().SetFloat("sideVelocity", -1);
        else
            GetComponent<Animator>().SetFloat("sideVelocity", 0);


        //print(Input.GetAxisRaw("LookHorizontal"));

        //else
        //GetComponent<Animator>().SetFloat("velocity", 0);

        if (!motor)
            motor = GetComponent<PlayerMotor>();

        //Apply movement
        motor.Move(velocity);

        //Apply Y rotation (turning)
        float yRot = Input.GetAxisRaw("LookHorizontal");
        Vector3 rotation = new Vector3(0f, yRot, 0f) * lookSensitivity;
        motor.Rotate(rotation);

        //Apply X rotation (up and down)
        float xRot = Input.GetAxisRaw("LookVertical");
        float camRotX = xRot * lookSensitivity;
        motor.RotateCamera(camRotX);
    }

    public void SetTutorial(bool enabled)
    {
        tutorialsOn = enabled;
    }

    public void Tutorialoff ()
    {
        foreach (GameObject tut in GameObject.FindGameObjectsWithTag("TutorialText"))
        {
            if (tut.name.Contains("GuideTexts"))
            {
                Cmd_NetworkDestroy(tut);
                Destroy(tut);
            }
            
        }
    }
    protected Vector3 TeleToRes(Vector3 pos)
    {
        SortedDictionary<float, Vector3> dists = new SortedDictionary<float, Vector3>();
        List<float> distGrabber = new List<float>();

        foreach (GameObject p in GameObject.FindGameObjectsWithTag("OutMapRes"))
        {
            float dist = Vector3.Distance(p.transform.position, pos);
            dists.Add(dist, p.transform.position);
            distGrabber.Add(dist);
        }

        float minDist = float.MaxValue;
        foreach (float f in distGrabber)
        {
            if (f < minDist)
                minDist = f;
        }

        return dists[minDist];
    }


    protected void RaycastAll(Ray ray)
    {

        //print(isServer + " " + isLocalPlayer + " " + isClient);

        foreach (RaycastHit allHit in Physics.RaycastAll(ray))
        {
            if (allHit.collider.gameObject.tag == "Menu Button")
            {
                if (Input.GetButtonDown("Interact"))
                {
                    allHit.collider.gameObject.GetComponent<MenuButton>().PerformAction();

                }
            }

            if (allHit.collider.gameObject.tag == "StartGame")
            {
                if (Input.GetButtonDown("Interact"))
                {
                    SceneManager.LoadScene("POTATOS NOT WORKING");
                }
            }
        }
    }

    protected void ChangeLighting()
    {
        if(!VRDevice.isPresent)
        {
            RenderSettings.ambientIntensity = 0.2f;
        }
        else
        {
            RenderSettings.ambientIntensity = 0.08f;
        }
    }

    protected IEnumerator ShowScreenOverlay(float seconds)
    {
        ScreenOverlay so = cam.GetComponent<ScreenOverlay>();

        screenOverlayShowing = true;
        float fadeTime = seconds  * 0.2f;
        float showTime = seconds - (fadeTime * 2);

        while (so.intensity < 0.7f)
        {
            so.intensity += Time.deltaTime;
            yield return new WaitForSeconds(fadeTime * Time.deltaTime);
        }

        yield return new WaitForSeconds(showTime);

        while (so.intensity > 0)
        {
            so.intensity -= Time.deltaTime;
            yield return new WaitForSeconds(fadeTime * Time.deltaTime);
        }
        screenOverlayShowing = false;
    }

    //[Command]
    //protected void CmdDoGameOver(string winner)
    //{
    //    SpawnRoom spawnRoom = GameObject.Find("Network Manager").transform.FindChild("Spawn Room").GetComponent<SpawnRoom>();
    //    GameObject protag = spawnRoom.protag;

    //    foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
    //    {
    //        Cmd_SpawnProtagAtSpawnRoom();
    //    }

    //    spawnRoom.wallText = "Game Over. " + winner + " wins!";
    //}


    [Command]
    public void Cmd_Stun(float seconds)
    {
        Rpc_Stun(seconds);
    }

    [ClientRpc]
    private void Rpc_Stun(float seconds)
    {
        Stun(seconds);
    }

    public void Stun(float seconds)
    {
       // StartCoroutine(iStun(seconds));
    }

    private IEnumerator iStun(float seconds)
    {
        //currentSpeedMod = speedModifier;
        speedModifier = 0;
        yield return new WaitForSeconds(seconds);
        //speedModifier = currentSpeedMod;
    }


    [ClientRpc]
    private void Rpc_ChangeDoorState(GameObject go, bool open)
    {
        Door door = go.GetComponent<Door>();
        if (open)
            door.OpenDoor();
        else
            door.CloseDoor();
    }

    [Command]
    public void Cmd_ChangeDoorState(GameObject go, bool open)
    {
        if (!isLocalPlayer)
        {
            NetworkIdentity objNetId = go.GetComponent<NetworkIdentity>();
            objNetId.AssignClientAuthority(connectionToClient);
            Rpc_ChangeDoorState(go, open);
            objNetId.RemoveClientAuthority(connectionToClient);
        }
    }

    [Command]
    public void Cmd_UnlockDoor(GameObject[] doors, string colour)
    {
        foreach (GameObject door in doors)
        {
            Door d = door.GetComponent<Door>();
            d.SetColour(colour);
        }
    }

    [ClientRpc]
    private void Rpc_SetLaptopColour(GameObject Plane, Color colour)
    {
        foreach (Material mat in Plane.GetComponent<Renderer>().materials)
        {
            if (mat.name.Contains("Laptop"))
            {
                mat.color = colour;
            }
        }
    }

    [Command]
    public void Cmd_SetLaptopColour(GameObject laptop, Color colour)
    {
        NetworkIdentity objNetId = laptop.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        Rpc_SetLaptopColour(laptop, colour);
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [Command]
    public void Cmd_SetLaptopState(GameObject laptop, int state)
    {
        NetworkIdentity objNetId = laptop.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        Rpc_SetLaptopState(laptop, state);
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [ClientRpc]
    public void Rpc_SetLaptopState(GameObject laptop, int state)
    {
        if (state == 1)
            laptop.GetComponent<DoorScreenAnimator>().SetAccessGranted();
        else if (state == 2)
            laptop.GetComponent<DoorScreenAnimator>().SetNeedKey();
    }

    [ClientRpc]
    private void Rpc_SetLarvaeColour(GameObject larvae, Color colour)
    {
        foreach (Renderer r in larvae.GetComponentsInChildren<Renderer>())
        {
            r.material.color = colour;
        }
    }

	[Command]
	public void Cmd_SetLarvaeColour(GameObject larvae, Color colour) {
        NetworkIdentity objNetId = larvae.GetComponent<NetworkIdentity>();        // get the object's network ID
        objNetId.AssignClientAuthority(connectionToClient);     // assign authority to the player who is changing the color
        Rpc_SetLarvaeColour(larvae, colour);                    // use a Client RPC function to "paint" the object on all clients
        objNetId.RemoveClientAuthority(connectionToClient);    // remove the authority from the player who changed the color
    }

    public bool GetStunState(GameObject player)
    {
        if (player)
            return player.GetComponent<ProtagController>().GetStunAntag();
        else
            return false;
    }

    //[Command]
    //public bool Cmd_GetStunState(GameObject player)
    //{
    //    NetworkIdentity objNetId = player.GetComponent<NetworkIdentity>();
    //    objNetId.AssignClientAuthority(connectionToClient);
    //    return player.GetComponent<ProtagController>().GetStunAntag();
    //}

    [Command]
    public void Cmd_SetStunState (GameObject player, bool isstunned)
    {
        NetworkIdentity objNetId = player.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        player.GetComponent<ProtagController>().SetStunAntag(isstunned);
        objNetId.RemoveClientAuthority(connectionToClient);
    }
    
    [Command]
    protected void Cmd_NetworkSetRendererEnabled(GameObject obj, bool enable)
    {

        NetworkIdentity objNetId = obj.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        Rpc_NetworkSetRendererEnabled(obj, enable);                   
        objNetId.RemoveClientAuthority(connectionToClient);  
    }

    [ClientRpc]
    private void Rpc_NetworkSetRendererEnabled(GameObject go, bool e)
    {
        go.GetComponent<Renderer>().enabled = e;
    }

    [Command]
    protected void Cmd_NetworkDestroy(GameObject go)
    {
        NetworkServer.Destroy(go);
    }

    [Command]
    public void Cmd_SpawnAntag(GameObject player, GameObject antag)
    {
		System.Random rand = new System.Random();
		GameObject[] antagSpawns = GameObject.FindGameObjectsWithTag("AntagSpawnPoint");

		int randNum = rand.Next(0, antagSpawns.Length - 1);

		print("Instantiating Antag at " + antagSpawns[randNum].transform.position);
		GameObject newAntag = (GameObject)Instantiate(antag, antagSpawns[randNum].transform.position + new Vector3(0, 1.1f, 0), antagSpawns[randNum].transform.rotation);

		print("Replace Player");
		NetworkServer.DestroyPlayersForConnection(connectionToClient);
		NetworkServer.SpawnWithClientAuthority(newAntag, connectionToClient);  
		NetworkServer.ReplacePlayerForConnection(connectionToClient, newAntag, playerControllerId);

        NetworkIdentity antagObjId = newAntag.GetComponent<NetworkIdentity>();
        antagObjId.AssignClientAuthority(connectionToClient);

        print("Spawn Antag Successful at " +    newAntag.transform.position);

    }


    [Command]
	public void Cmd_SpawnProtag(GameObject player, GameObject protag)
	{
		System.Random rand = new System.Random();
		GameObject[] protagSpawns = GameObject.FindGameObjectsWithTag("ProtagSpawnPoint");

		int randNum = rand.Next(0, protagSpawns.Length - 1);

		print("Instantiate. Protag");
		GameObject newProtag = (GameObject)Instantiate(protag, protagSpawns[randNum].transform.position + new Vector3(0, 1.1f, 0), protagSpawns[randNum].transform.rotation);

		//GetComponent<NetworkIdentity>().AssignClientAuthority(pc.localConn);

		print("Replace Player");
		NetworkServer.DestroyPlayersForConnection(connectionToClient);
        NetworkServer.SpawnWithClientAuthority(newProtag, connectionToClient);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, newProtag, playerControllerId);
        
        NetworkIdentity protagObjId = newProtag.GetComponent<NetworkIdentity>();
        protagObjId.AssignClientAuthority(connectionToClient);
        print("Spawn Protag Successful");
    }

    [Command]
    public void Cmd_SpawnAtEnd2(int winType, GameObject protag) //0 = antag, 1 = vent exit, 2 = computer
    {
        System.Random rand = new System.Random();
        GameObject[] spawns = GameObject.FindGameObjectsWithTag("FinalSpawn");

        int randNum = rand.Next(0, spawns.Length - 1);

        print("Instantiate. Protag");
        GameObject newProtag = (GameObject)Instantiate(protag, spawns[randNum].transform.position + new Vector3(0, 1.1f, 0), spawns[randNum].transform.rotation);


        //GetComponent<NetworkIdentity>().AssignClientAuthority(pc.localConn);

        print("Replace Player");
        NetworkServer.DestroyPlayersForConnection(connectionToClient);
        NetworkServer.SpawnWithClientAuthority(newProtag, connectionToClient);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, newProtag, playerControllerId);

        NetworkIdentity protagObjId = newProtag.GetComponent<NetworkIdentity>();
        protagObjId.AssignClientAuthority(connectionToClient);
        print("Spawn Protag Successful");

      
        foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.GetComponent<BasePlayerController>().DisplayFinalText(winType);
        }

        
        GameObject.Find("Unusued Room").GetComponent<ReloadLevel>().RestartLevel();
    }

    public void DisplayFinalText(int winType)
    {

        foreach (GameObject go in GameObject.FindGameObjectsWithTag("FinalText"))
        {
            if (winType == 0 && go.name == "AntagWin")
            {
                Cmd_ChangeFinalText(go, true);
                FMODUnity.RuntimeManager.PlayOneShot(antagWinMusic, go.transform.position);
            }
            else if (winType == 1 && go.name == "ProtagWinVent")
            {
                Cmd_ChangeFinalText(go, true);
                FMODUnity.RuntimeManager.PlayOneShot(protagWinMusic, go.transform.position);
            }
            else if (winType == 2 && go.name == "ProtagWinComp")
            {
                Cmd_ChangeFinalText(go, true);
                FMODUnity.RuntimeManager.PlayOneShot(protagWinMusic, go.transform.position);
            }
        }
    }


    [Command]
    protected void Cmd_ChangeFinalText(GameObject obj, bool enable)
    {
        NetworkIdentity objNetId = obj.GetComponent<NetworkIdentity>();
        objNetId.AssignClientAuthority(connectionToClient);
        Rpc_ChangeFinalText(obj, enable);
        objNetId.RemoveClientAuthority(connectionToClient);
    }

    [ClientRpc]
    private void Rpc_ChangeFinalText(GameObject go, bool e)
    {
        go.GetComponent<Text>().enabled = e;
    }


    [Command]
    public void Cmd_SpawnAtEnd(int winType, GameObject protag) //0 = antag, 1 = vent exit, 2 = computer
    {
        int playersMoved = 0;
        if (GameObject.Find("SpawnRoom").GetComponent<MainMenu>().multiplayer)
        {
            while (playersMoved < 2)
            {
                foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
                {
                    BasePlayerController bpc = player.GetComponent<BasePlayerController>();
                    if (bpc.isLocalPlayer && playersMoved < 1)
                        continue;
                    if (!bpc.isLocalPlayer && playersMoved > 0)
                        continue;

                    Rpc_SpawnAtEnd(winType, protag);
                    playersMoved++;
                }
            }
        }
        else
        {
            Rpc_SpawnAtEnd(winType, protag);
        }
                
    }

    [ClientRpc]
    public void Rpc_SpawnAtEnd(int winType, GameObject protag)
    {
        System.Random rand = new System.Random();
        GameObject[] endSpawns = GameObject.FindGameObjectsWithTag("FinalSpawn");

        int randNum = rand.Next(0, endSpawns.Length - 1);

        print("Instantiate. Protag");
        GameObject newProtag = (GameObject)Instantiate(protag, endSpawns[randNum].transform.position + new Vector3(0, 1.1f, 0), endSpawns[randNum].transform.rotation);

        //GetComponent<NetworkIdentity>().AssignClientAuthority(pc.localConn);

        print("Replace Player");
        NetworkServer.DestroyPlayersForConnection(connectionToClient);
        NetworkServer.SpawnWithClientAuthority(newProtag, connectionToClient);

        NetworkServer.ReplacePlayerForConnection(connectionToClient, newProtag, playerControllerId);

        NetworkIdentity protagObjId = newProtag.GetComponent<NetworkIdentity>();
        protagObjId.AssignClientAuthority(connectionToClient);
        print("Spawn Protag Successful");
    }

    //[ClientRpc]
    //public void Rpc_SpawnAtEnd(bool protagWin)
    //{
    //    print("Spawing " + (isServer ? "server" : "client") + " at end.");
    //    transform.Find("Graphics").gameObject.SetActive(false);
    //    GameObject fSpawn = GameObject.FindGameObjectWithTag("FinalSpawn");
    //    transform.position = fSpawn.transform.position;
    //    transform.rotation = fSpawn.transform.rotation;
    //    speedModifier = 0;


    //    //GameObject protag = GameObject.Find("Network Manager").transform.FindChild("Spawn Room").GetComponent<SpawnRoom>().protag;
    //    //GameObject spawnPoint = GameObject.FindGameObjectWithTag("FinalSpawn");
    //    //GameObject newProtag = (GameObject)Instantiate(protag,spawnPoint.transform.position + new Vector3(0, 1.1f, 0), spawnPoint.transform.rotation);

    //    //newProtag.transform.Find("Graphics").gameObject.SetActive(false);
    //    //newProtag.GetComponent<PlayerMotor>().enabled = false;

    //    //print("Replace Player");
    //    //NetworkServer.DestroyPlayersForConnection(connectionToClient);
    //    //NetworkServer.Spawn(newProtag);
    //    //NetworkServer.ReplacePlayerForConnection(connectionToClient, newProtag, playerControllerId);
    //}


    [Command]
    public void Cmd_SpawnProtagAtSpawnRoom()
    {
        GameObject protag = GameObject.Find("Network Manager").transform.FindChild("Spawn Room").GetComponent<SpawnRoom>().protag;
        System.Random rand = new System.Random();
        NetworkStartPosition[] spawnPoints = FindObjectsOfType<NetworkStartPosition>();

        int randNum = rand.Next(0, spawnPoints.Length - 1);

        GameObject newProtag = (GameObject)Instantiate(protag, spawnPoints[randNum].transform.position + new Vector3(0, 1.1f, 0), spawnPoints[randNum].transform.rotation);


        print("Replace Player");
        NetworkServer.DestroyPlayersForConnection(connectionToClient);
        NetworkServer.Spawn(newProtag);
        NetworkServer.ReplacePlayerForConnection(connectionToClient, newProtag, playerControllerId);
    }

    //    public void FadeToBlack()
    //    {
    //        ftbsRenderer.material.color = Color.black;
    //        //while(ftbsRenderer.material.color.a <= 0.95f)
    //        //{
    //        //    ftbsRenderer.material.SetColor("_Color", Color.Lerp(ftbsRenderer.material.color, Color.black, fadeToBlackSpeed * Time.deltaTime));
    //        //    yield return null;
    //        //}
    //        //ftbsRenderer.material.SetColor("_Color", Color.black);
    //    }

    //    public void FadeToClear()
    //    {
    //        ftbsRenderer.material.color = Color.clear;

    //           //while (ftbsRenderer.material.color.a >= 0.05f)
    //           //{
    //           //    ftbsRenderer.material.SetColor("_Color", Color.Lerp(ftbsRenderer.material.color, Color.clear, fadeToBlackSpeed * Time.deltaTime));
    //           //    print(ftbsRenderer.material.color.a);
    //           //}
    //           //ftbsRenderer.material.SetColor("_Color", Color.clear);
    //    }
}
