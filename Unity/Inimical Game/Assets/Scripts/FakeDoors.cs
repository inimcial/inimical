﻿using UnityEngine;
using System.Collections;

public class FakeDoors : MonoBehaviour {
    
    public bool redCard;
    public bool greenCard;
    public bool blueCard;

    private Inventory inv;
    private Collider col;
    private ProtagController protag;
    private Renderer doorRenderer;
    private Color greycol;
    private Color GreenCol;
    private Color RedCol;
    private Color BlueCol;

	// Use this for initialization
	void Start () {

        greycol = new Color(1f, 1f, 1f, 0.9f);
        GreenCol = new Color(0f, 1f, 0f, 0.5f);
        BlueCol = new Color(0f, 0f, 1f, 0.5f);
        RedCol = new Color(1f, 0f, 0f, 0.5f);

        col = this.GetComponent<Collider>();
        col.isTrigger = false;
        doorRenderer = col.gameObject.GetComponent<Renderer>();
        doorRenderer.material.color = greycol;
    }
	
	// Update is called once per frame
	void Update () {

        if(protag == null)
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (player.name.Contains("Protag_"))
                {
                    protag = player.GetComponent<ProtagController>();
                }
            }
        }

        if (this.gameObject.tag  == "Green Door" && protag.InventoryItems.Contains("Green"))
        {
            greenCard = true;
            col.isTrigger = true;
            doorRenderer.material.color = GreenCol;
        }
        
        if (this.gameObject.tag == "Blue Door" && protag.InventoryItems.Contains("Blue") || protag.InventoryItems.Contains("BlueHint")) //(bool)protag.GetComponent<Inventory>.inventory["BlueHint"]
        {
            blueCard = true;
            col.isTrigger = true;
            doorRenderer.material.color = BlueCol;
        }
        

        if (this.gameObject.tag == "Red Door" && protag.InventoryItems.Contains("Red"))
        {
            redCard = true;
            col.isTrigger = true;
            doorRenderer.material.color = RedCol;
        }

    }
}
