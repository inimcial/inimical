﻿using UnityEngine;
using System.Collections;

public class DoorScreenAnimator : MonoBehaviour {

	public int doorType = 0; // 0=alpha, 2=beta, 4=delta, 6=gamma
	public float framesPerSecond = 20f;

	private int columns = 8;
	private int columnOffset = 0;
	private int rowOffset = 0;
	private float iX=0;
	private float iY=0;
	private Vector2 _size;
	private Renderer _myRenderer;
	private int _lastIndex = -1;
	private int timeDelay = 0;
    private int state = 0; // 0=Insert, 1=Granted, 2=CardRequired
    private int oldState = 0;

	// initialization
	void Start () {
		_size = new Vector2 (0.125f, 0.125f); //sprite sheet is 8 x 8
		_myRenderer = GetComponent<Renderer>();
		if (_myRenderer == null) enabled = false;
		// _myRenderer.material.SetTextureScale ("_MainTex", _size);
	}
		
	// Allows another script to change the state to "access granted".
	public void SetAccessGranted (){
		state = 1;
	}

	// Allows another script to change the state to "key required".
	public void SetNeedKey () {
		state = 2;
	}

	// Update is called once per frame
	void Update () {

		switch(state){
		case 0:
			columns = 8;
			columnOffset = 0;
			rowOffset = 0;
			break;
		case 1:
			columns = 4;
			columnOffset = 0;
			rowOffset = 1;
			break;
		case 2:
			columns = 8;
			columnOffset = 1;
			rowOffset = 1;
			break;
		}

		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond) % (columns);

		if(index != _lastIndex)
		{
            if (state != oldState)
            {
                oldState = state;
                if (state == 2) iX = iX % 4 + 4; // Ensure if state is "Disk Required", then iX points to it.
            }
            Vector2 offset = new Vector2(iX*_size.x,
				-(_size.y*(iY+doorType+rowOffset)));
			iX++;
			if(iX / columns >= 1) {
				iX = 4 * columnOffset;
			}
			if (state == 2) {  // state 2 ("key required") is only displayed for a short time
				timeDelay++;
				if (timeDelay >= 5) {
					state = 0;
					timeDelay = 0;
				}
			}

			_myRenderer.materials[1].SetTextureOffset ("_MainTex", offset);


			_lastIndex = index;
		}
	}

}
