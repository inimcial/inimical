﻿using UnityEngine;
using System.Collections;

public class ComputerOnDoor : MonoBehaviour {

    private enum Colour { Red, Green, Blue, Yellow };

    private Material[] screen;
    public GameObject Plane;
    public GameObject missingText;
    

    [SerializeField]
    private Colour compColour;

    public string colour;

    [SerializeField]
    private float interactTime = 1;
    private float startingInteractTime;

    [SerializeField]
    [FMODUnity.EventRef]
    private string loadingAudio = "event:/ComputerLoad";
    private FMOD.Studio.EventInstance loadEv;

    [SerializeField]
    [FMODUnity.EventRef]
    private string positiveAudio = "event:/ComputerSuccess";

    public bool doneUnlock = false;

    [SerializeField]
    private GameObject[] doors;

    private bool showGuide = true;

    void Start()
    {
        if (!missingText && transform.Find("MissingKeycard"))
            missingText = transform.Find("MissingKeycard").gameObject;

        if (!Plane && transform.Find("ScreenColor"))
            Plane = transform.Find("ScreenColor").gameObject;

        if (doors.Length < 2)
            Debug.LogError("Computer not assigned both doors");
        else if(!doors[0] || !doors[1])
            Debug.LogError("Computer not assigned both doors");
        

        colour = compColour.ToString();
        startingInteractTime = interactTime;

        loadEv = FMODUnity.RuntimeManager.CreateInstance(loadingAudio);
        FMOD.ATTRIBUTES_3D loadEvAttrib = new FMOD.ATTRIBUTES_3D();
        loadEvAttrib.position = FMODUnity.RuntimeUtils.ToFMODVector(transform.position);
        loadEv.set3DAttributes(loadEvAttrib);

        loadEv.start();
        loadEv.setPaused(true);

        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player"))
        {
            BasePlayerController bpc = p.GetComponent<BasePlayerController>();
            if (bpc.isLocalPlayer)
                bpc.Cmd_SetLaptopColour(Plane, Color.white);
        }

        //thisScreen.color = Color.black;

        if(missingText)
            missingText.GetComponent<Renderer>().enabled = false;


        DoorScreenAnimator dsa = GetComponent<DoorScreenAnimator>();
        if (compColour == Colour.Green)
            dsa.doorType = 0;
        else if (compColour == Colour.Blue)
            dsa.doorType = 2;
        else if (compColour == Colour.Red)
            dsa.doorType = 4;
        else if (compColour == Colour.Yellow)
            dsa.doorType = 6;
        

    }

    void Update()
    {
        if (!doneUnlock && interactTime <= 0)
        {
            FMODUnity.RuntimeManager.PlayOneShot(positiveAudio, transform.position);

            if(colour != "Yellow")
                DoDoorUnlock();


            doneUnlock = true;


            if (transform.FindChild("GuideText"))
            {
                transform.FindChild("GuideText").gameObject.SetActive(false);
                showGuide = false;
            }

            if (colour == "Yellow")
            {
                if (gameObject.name == "YellowComputer")
                {

                    foreach (GameObject obj in GameObject.FindGameObjectsWithTag("ExitVent"))
                    {
                        if (obj.name == "ExitCondition")
                        {
                            obj.GetComponent<BoxCollider>().enabled = true;
                            obj.GetComponent<exitbox>().SetExitCondition(true);
                            print("Exit Condition = " + obj.GetComponent<exitbox>().GetExitCondition().ToString());
                        }

                        if(obj.name == "VentFan")
                        {
                            obj.GetComponent<Animator>().enabled = false;
                        }

                        if(obj.name == "Wall_Vent")
                        {
                            obj.GetComponent<Renderer>().material.SetColor("_EmissionColor", Color.red);
                        }
                    }
                }

                if (gameObject.name == "FinalLaptop")
                {
                    foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
                    {
                        if (player.name.Contains("Protag"))
                        {
                            if (player.GetComponent<ProtagController>().InventoryItems.Contains("YellowHint"))
                            {
                                //player.GetComponent<BasePlayerController>().Cmd_SpawnAtEnd(2, gameObject);
                                GameObject.Find("Spawn Room").transform.Find("Spawn Manager").GetComponent<SpawnManager>().EndGame(2);
                            }
                        }
                    }
                }
            }
        }
    }

    private void DoDoorUnlock()
    {
        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player"))
        {
            p.GetComponent<BasePlayerController>().Cmd_UnlockDoor(doors, colour);
        }
                
    }

    public void SetTime(float newTime)
    {
        interactTime = newTime;
    }

    public float GetTime()
    {
        return interactTime;
    }

    public void SetLoadingPlaying(bool enabled)
    {
        loadEv.setPaused(!enabled);
    }

    public float GetPercentageComplete()
    {
        return 1 - (interactTime / startingInteractTime);
    }

    public bool GetShowGuide()
    {
        return showGuide;
    }

}
