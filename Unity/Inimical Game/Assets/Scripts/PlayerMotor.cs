﻿using UnityEngine;
using UnityEngine.VR;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour
{
    private Rigidbody rb;
    private Transform head;
    private Transform j23;
    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;

    [SerializeField]
    private float rotStep = 10;
    private Quaternion calcRot;


    [SerializeField]
    private Camera cam;
    [SerializeField]
    private float cameraRotationLimit = 85f;
   

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        calcRot = rb.rotation;
    }

    //Run every physics tick, more accurate than Update() for movement
    void FixedUpdate()
    {
        if(!name.Contains("EndPlayer"))
            PerformMovement();

        PerformRotation();
    }


    //Get the current velocity
    public void Move(Vector3 vel)
    {
        velocity = vel;
    }
    //Get the current rotation
    public void Rotate(Vector3 rot)
    {
        rotation = rot;
    }
    //Get the current camera rotation
    public void RotateCamera(float camRotX)
    {
        cameraRotationX = camRotX;
    }

    void PerformMovement()
    {
        if (velocity != Vector3.zero)
            rb.MovePosition(rb.position + velocity * Time.deltaTime);
    }

    void PerformRotation()
    {
        //print(Mathf.Abs((rb.rotation * Quaternion.Euler(rotation)).y)*360f);

        //calcRot =  calcRot * Quaternion.Euler(rotation);

        //print((int)(Mathf.Abs(calcRot.y) * 360f));
        //print(rb.rotation);

        //if ((int)(Mathf.Abs(calcRot.y) * 360) % rotStep == 0)
        //{
        //    print("Yes");
        //    rb.rotation = calcRot;
        //}

        rb.MoveRotation(rb.rotation * Quaternion.Euler(rotation));

        if (cam != null && !VRDevice.isPresent)
        {
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0, 0);
        }
    }
}
