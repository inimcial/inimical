﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections.Generic;

public class PlayerNetworkSetup : NetworkBehaviour {

    List<Behaviour> componentsToDisable;

    Camera menuCamera;

    // Use this for initialization
    void Start()
    {

        componentsToDisable = new List<Behaviour>();

        componentsToDisable.Add(GetComponent<PlayerMotor>());
        componentsToDisable.Add(GetComponent<BasePlayerController>());
        if(transform.Find("CamParent"))
        {
            componentsToDisable.Add(transform.Find("CamParent").Find("Camera").GetComponent<Camera>());
            componentsToDisable.Add(transform.Find("CamParent").Find("Camera").GetComponent<FMODUnity.StudioListener>());
        }
        else
        {
            componentsToDisable.Add(transform.Find("Camera").GetComponent<Camera>());
            componentsToDisable.Add(transform.Find("Camera").GetComponent<FMODUnity.StudioListener>());
        }
        //componentsToDisable.Add(transform.FindChild("Camera").GetComponent<FMOD_Listener>());
        //componentsToDisable.Add(transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").GetComponent<Camera>());
        //componentsToDisable.Add(transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").GetComponent<AudioListener>());


        if (!isLocalPlayer)
        {
            for (int i = 0; i < componentsToDisable.Count; i++)
            {
                componentsToDisable[i].enabled = false;
            }

            if(transform.Find("CamParent"))
                transform.Find("CamParent").Find("Camera").Find("Crosshair").gameObject.SetActive(false);
            else
                transform.Find("Camera").Find("Crosshair").gameObject.SetActive(false);


        }
        else
        {
            menuCamera = Camera.main;
            if (menuCamera != null)
                menuCamera.gameObject.SetActive(false);
        }
    }

    void OnDisable()
    {
        if (menuCamera != null)
            menuCamera.gameObject.SetActive(true);
    }
}
