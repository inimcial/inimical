﻿using System;
using UnityEngine;
using UnityEngine.Networking;

public class NewDoors : NetworkBehaviour
{

    public GameObject door;

    [SyncVar(hook = "OnRedCardChange")]
    public bool redCard;
    [SyncVar(hook = "OnGreenCardChange")]
    public bool greenCard;
    [SyncVar(hook = "OnBlueCardChange")]
    public bool blueCard;
    public bool locked;
    public float doorSpeed = 2;
    public float openSize = 2;

	[SyncVar(hook = "OnDoorIsOpenChange")]
    private bool doorIsOpen;
	[SyncVar(hook = "OnDoOpenOrCloseChange")]
    private bool doOpenOrClose;
    //private GameObject player;


    void Start()
    {
        if (door == null)
            door = transform.FindChild("DOOR").gameObject;

        doorIsOpen = false;
        doOpenOrClose = false;
        //player = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
		//if (player == null)
			//player = GameObject.FindGameObjectWithTag ("Player");
		
        if(doOpenOrClose)
        {
            if (doorIsOpen)
                CloseDoor();
            else
                OpenDoor(); 
        }
    }

    void OnTriggerEnter(Collider obj)
    {
        if (obj.tag == "Player")
        {
            print("Entered");
            if (!locked && !greenCard && !blueCard && !redCard)
            {
                if (doOpenOrClose)
                    doorIsOpen = false;
                if (!doorIsOpen)
                    doOpenOrClose = true;
            }      
        }
    }

    void OnTriggerExit(Collider obj)
    {
        if (obj.tag == "Player")
        {
            if (doOpenOrClose)
                doorIsOpen = true;

            if (doorIsOpen)
                doOpenOrClose = true;
        }
    }

    private void OpenDoor()
    {
        //print(doorTop.transform.localPosition.y);
        //if (doorTop.transform.localPosition.y <= openSize && doorBottom.transform.localPosition.y >= -openSize)
        //{
        //    doorTop.transform.Translate(Vector3.up * Time.deltaTime * doorSpeed, Space.Self);
        //    doorBottom.transform.Translate(Vector3.down * Time.deltaTime * doorSpeed, Space.Self);
        //}
        if(Mathf.Abs(door.transform.localPosition.x) <= openSize)
        {
            door.transform.Translate(Vector3.left * Time.deltaTime * doorSpeed, Space.Self);
        }
        else
        {
            doorIsOpen = true;
            doOpenOrClose = false;
        }
            
    }

    private void CloseDoor()
    {
        //print(doorTop.transform.localPosition.y);
        //if (doorTop.transform.localPosition.y >= 0.01    && doorBottom.transform.localPosition.y <= 0.01)
        //{
        //    doorTop.transform.Translate(Vector3.down * Time.deltaTime * doorSpeed, Space.Self);
        //    doorBottom.transform.Translate(Vector3.up * Time.deltaTime * doorSpeed, Space.Self);
        //}
        if (Mathf.Abs(door.transform.localPosition.x) >= 0.01)
        {
            door.transform.Translate(Vector3.right * Time.deltaTime * doorSpeed, Space.Self);
        }
        else
        {
            doorIsOpen = false;
            doOpenOrClose = false;
        }
    }

    private bool hasKeycard(GameObject player)
    {
        if (redCard)
        {
            if ((bool)player.GetComponent<Inventory>().inventory["Red_Keycard"])
            {
                redCard = false;
                return true;
            }               
            else
                return false;
        }
        if (greenCard)
        {
            if ((bool)player.GetComponent<Inventory>().inventory["Green_Keycard"])
            {
                greenCard = false;
                return true;
            }
            else
                return false;
        }
        if (blueCard)
        {
            if ((bool)player.GetComponent<Inventory>().inventory["Blue_Keycard"])
            {
                blueCard = false;
                return true;
            }
            else
                return false;
        }

        return true;
    }

	private void OnDoorIsOpenChange(bool doorIsOpenChange)
	{
		doorIsOpen = doorIsOpenChange;
	}

	private void OnDoOpenOrCloseChange(bool doOpenOrCloseChange)
	{
		doOpenOrClose = doOpenOrCloseChange;
	}

    private void OnRedCardChange(bool redCardChange)
    {
        redCard = redCardChange;
    }

    private void OnBlueCardChange(bool blueCardChange)
    {
        blueCard = blueCardChange;
    }

    private void OnGreenCardChange(bool greenCardChange)
    {
        greenCard = greenCardChange;
    }
}
