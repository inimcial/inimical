﻿using UnityEngine;

public class AirconCorner : MonoBehaviour {

    [SerializeField]
    private float turnSpeed = 60;

    [SerializeField]
    private bool isSpinning;

    void Start()
    {
        isSpinning = new System.Random(GetInstanceID()).NextDouble() > 0.5;
        if (!transform.Find("W_AIRCON_CORNER_FAN_Prefab"))
            isSpinning = false;
    }

	void Update () {
        if(isSpinning)
            transform.Find("W_AIRCON_CORNER_FAN_Prefab").Rotate(Vector3.forward, turnSpeed * Time.deltaTime);
	}
}
