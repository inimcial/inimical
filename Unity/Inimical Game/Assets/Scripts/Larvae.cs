﻿using UnityEngine;
using System.Collections;

public class Larvae : MonoBehaviour {

    public bool activated;

    [SerializeField, FMODUnity.EventRef]
    private string activeSound = "event:/Larvae/Larvae Chirping";
    private FMOD.Studio.EventInstance activeEv;

    [SerializeField, FMODUnity.EventRef]
    private string dormantSound = "event:/Larvae/Larvae Dormant Continous";
    private FMOD.Studio.EventInstance dormantEv;

    private FMOD.ATTRIBUTES_3D posEvAttribs;

    public bool playingDormant;

    void Start()
    {
        activated = false;

        activeEv = FMODUnity.RuntimeManager.CreateInstance(activeSound);
        dormantEv = FMODUnity.RuntimeManager.CreateInstance(dormantSound);

        posEvAttribs = new FMOD.ATTRIBUTES_3D();
        posEvAttribs.position = FMODUnity.RuntimeUtils.ToFMODVector(transform.position);

        activeEv.set3DAttributes(posEvAttribs);
        dormantEv.set3DAttributes(posEvAttribs);


        dormantEv.start(); activeEv.start();
        dormantEv.setPaused(true); activeEv.setPaused(true);
    }

    void Update()
    {
        if (activated && !GetComponent<Animator>().GetBool("doJiggle"))
        {
            dormantEv.setPaused(false);
            GetComponent<Animator>().SetBool("doJiggle", true);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if(activated && other.name.Contains("Protag"))
        {
            other.gameObject.GetComponent<BasePlayerController> ().Cmd_SetLarvaeColour (this.gameObject, Color.red);
            dormantEv.setPaused(true);
            activeEv.setPaused(false);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(activated && other.name.Contains("Protag"))
        {
            other.gameObject.GetComponent<BasePlayerController>().Cmd_SetLarvaeColour(this.gameObject, Color.green);
            dormantEv.setPaused(false);
            activeEv.setPaused(true);
        }
    }






    //public bool activated;

    //   private bool canHear;

    //   [SerializeField, FMODUnity.EventRef]
    //   private string activeSound = "event:/Larvae/Larvae Chirping";
    //   private FMOD.Studio.EventInstance activeEv;

    //   [SerializeField, FMODUnity.EventRef]
    //   private string dormantSound = "event:/Larvae/Larvae Dormant Continous";
    //   private FMOD.Studio.EventInstance dormantEv;

    //   // Use this for initialization
    //   void Start () {
    //	activated = false;
    //       canHear = true;

    //       FMOD.ATTRIBUTES_3D posEvAttribs = new FMOD.ATTRIBUTES_3D();
    //       posEvAttribs.position = FMODUnity.RuntimeUtils.ToFMODVector(transform.position);

    //       activeEv.set3DAttributes(posEvAttribs);
    //       dormantEv.set3DAttributes(posEvAttribs);

    //       activeEv = FMODUnity.RuntimeManager.CreateInstance(activeSound);
    //       dormantEv = FMODUnity.RuntimeManager.CreateInstance(dormantSound);

    //       dormantEv.start(); activeEv.start();
    //       dormantEv.setPaused(true); activeEv.setPaused(true);
    //   }

    //// Update is called once per frame
    //void Update () {
    //    if(activated && !GetComponent<Animator>().GetBool("doJiggle"))
    //       {
    //           GetComponent<Animator>().SetBool("doJiggle", true);
    //           dormantEv.setPaused(false);
    //       }

    //}

    //   void OnTriggerEnter(Collider obj)
    //   {
    //	if(activated && obj.name.Contains("Protag")) { 
    //		obj.gameObject.GetComponent<BasePlayerController> ().Cmd_SetLarvaeColour (this.gameObject, Color.red);
    //		print ("Triggered");

    //           if (obj.gameObject.GetComponent<BasePlayerController>().isLocalPlayer)
    //               canHear = false;
    //           else
    //               canHear = true;

    //           //Playsound
    //           if(canHear)
    //           {
    //               activeEv.setPaused(false);
    //               dormantEv.setPaused(true);
    //           }
    //       }
    //   }

    //   void OnTriggerExit(Collider obj)
    //   {
    //       if(activated && obj.name.Contains("Protag"))
    //       {
    //           obj.gameObject.GetComponent<BasePlayerController>().Cmd_SetLarvaeColour(this.gameObject, Color.green);


    //           if (canHear)
    //           {
    //               activeEv.setPaused(true);
    //               dormantEv.setPaused(false);
    //           }
    //       }
    //   }
}
