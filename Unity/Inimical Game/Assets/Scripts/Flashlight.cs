﻿using UnityEngine;
using System.Collections;
using System;

public class Flashlight : MonoBehaviour {

    public Light flashlight;
    public float maxlife = 15;
    public float lifetime = 15;
    public float recharge = 0.5f;
    public float lightintencity = 3;
    private bool flickeron = false;
    public float flickerstart = 4;
    

    //[SerializeField]
    //public TextMesh t;
    

    // Use this for initialization
    void Start () {
        flashlight.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        
        LooseCharge();
        Recharge();
        
        if(lifetime <= flickerstart)
        {
            flickeron = true;
            StartCoroutine(Flickeroff());
        }
        else
        {
            flickeron = false;
            StopAllCoroutines();
        }

        if (lifetime == 0)
        {
            flashlight.enabled = false;

        }

        if (lifetime < 0)
        {
            flashlight.enabled = false;
            flashlight.intensity = lightintencity;
            lifetime = 0;
        }
        
        //t.text = "Flashlight Charge: " + Mathf.Ceil(lifetime).ToString();
    }


    void LooseCharge()
    {
        if (flashlight.isActiveAndEnabled == true)
        {
            if (lifetime > 0)
            {
                lifetime -= recharge * Time.deltaTime;
                
            }

        }
    }

    void Recharge() 
    {
        if(flashlight.enabled == false)
        {
            if(lifetime < maxlife)
            {
                lifetime += recharge * Time.deltaTime;
            }
        }
    
    }
    
    IEnumerator Flickeroff()
    {
        while(flickeron == true)
        {
            flashlight.intensity = 0f;
            yield return new WaitForSeconds(0.3f);
            flashlight.intensity = 1.5f;
            yield return new WaitForSeconds(0.3f);
         
        }
    }
    
}
