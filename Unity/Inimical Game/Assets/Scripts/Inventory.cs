﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;

public class Inventory : MonoBehaviour {

    public Hashtable inventory;
    public TextAsset itemList;

    private string itemListText;

	void Start () {
        inventory = new Hashtable();

        itemListText = itemList.text;
		foreach (string item in itemListText.Split(new[] {"\r\n", "\r", "\n"}, System.StringSplitOptions.None))
        {
            inventory.Add(item, false);
        }
	}
}
