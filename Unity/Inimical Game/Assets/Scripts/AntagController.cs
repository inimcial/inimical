﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;
using System.Collections;

[RequireComponent(typeof(PlayerMotor))]
public class AntagController : BasePlayerController
{
    //[Serializable] public class DictFloatFloat : SerializableDictionary<float, float> { }
    //[SerializeField]
    //private DictFloatFloat speedupTimes;

    private Dictionary<float, float> speedupTimes;

    float startingTime;
    protected ProtagController protag;
    protected GameObject protagplayer;

    [SerializeField]
    private float attackRechargeRate;
    private float attackCooldown;
    private bool canAttack;

    [SerializeField]
    private bool isstunned;
    [SerializeField]
    private float stuntime = 5;

	[SerializeField]
	private GameObject projectile;

    [SerializeField]
    private float jumpHeight;
    [SerializeField]
    private float jumpDistance;

    [SerializeField]
    private float attackChargeTime = 2.8f;
    private float startingAttackChargeTime;

    private bool isShooting;
    //private bool doTransform = false;

    private Transform gravDir;

    private Rigidbody rb;


    private bool showInteractHex;
    private SpriteSheet interactHex;
    private bool interactHeld;


    private float currentSpeedMod;


    [SerializeField, FMODUnity.EventRef]
    private string[] antagSounds;


    void Start()
    {
        print("Gets to start.");
        Init();
       
    }
        
    public void Init()
    {
        //if (VRSettings.enabled)
        //{
        //    cam = transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").gameObject;
        //    transform.FindChild("Camera").gameObject.SetActive(false);
        //}
        //else
        //{
        //    cam = transform.FindChild("Camera").gameObject;
        //    transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").gameObject.SetActive(false);
        //}

        ChangeLighting();

        print("Gets to Init");
        print(transform.position);
        if (VRSettings.enabled)
            InputTracking.Recenter();

        cam = transform.Find("Camera").gameObject;
        interactHex = cam.transform.Find("Crosshair").GetChild(0).GetComponent<SpriteSheet>();

        rb = GetComponent<Rigidbody>();
        rb.useGravity = false;

        motor = GetComponent<PlayerMotor>();

        startingLookSensitivity = lookSensitivity;
        
        showMenu = true;
        //ShowMenu();

        attackCooldown = 100;
        canAttack = true;
		isShooting = false;
        startingAttackChargeTime = attackChargeTime;

        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("spit"))
            GetComponent<Animator>().speed = attackChargeTime / 2.8f;

        for (int i = 0; i < GetComponent<Animator>().parameterCount; i++)
            GetComponent<NetworkAnimator>().SetParameterAutoSend(i, true);

        print("Finishes Init.");
        print(transform.position);

        currentSpeedMod = speedModifier;
    }

    void Awake()
    {
        startingTime = Time.realtimeSinceStartup;
    }

    void Update()
    {
        if (protag == null)
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (player.name.Contains("Protag"))
                {
                    protagplayer = player;
                    protag = player.GetComponent<ProtagController>();
                }
            }
        }


        isstunned = GetStunState(protagplayer);

        if (VRDevice.isPresent == true && string.IsNullOrEmpty(VRDevice.model))
        {
            Debug.Log("EMPTY VR DEVICE DETECTED!");
            VRSettings.loadedDevice = VRDeviceType.Oculus;
            VRSettings.enabled = true;
            VRSettings.showDeviceView = true;
        }

        DoMovement(0, true);

        if (Input.GetButtonDown("Menu"))
        {
            //ShowMenu();
        }

        if (Input.GetKeyDown(KeyCode.F9))
            transform.position = TeleToRes(transform.position);

        if (Input.GetButtonDown("ResetVRView") && VRSettings.enabled)
        {
            InputTracking.Recenter();
        }

        float timeSinceSpawn = Time.realtimeSinceStartup - startingTime;

		//print(Input.GetAxisRaw("AntagAttack"));

		if(!isShooting)
        {
            if (Input.GetAxisRaw("AntagAttack") > 0 && canAttack)
            {
                isShooting = true;
                GetComponent<Animator>().SetBool("isSpitting", true);
            }
        }
	        

		if (isShooting)
        {
            speedModifier = 0;
            attackChargeTime -= Time.deltaTime;

            interactHex.SetFrame((int)(1 - (attackChargeTime/startingAttackChargeTime) * (7 * 7))-5);

			if (Input.GetAxisRaw ("AntagAttack") > -0.2f && Input.GetAxisRaw("AntagAttack") < 0.2f) {
				isShooting = false;
                attackChargeTime = startingAttackChargeTime;
				attackCooldown = 0;
				GetComponent<Animator> ().SetBool ("isSpitting", false);
			}

            if(attackChargeTime <= 0)
            {
                Attack();
                speedModifier = currentSpeedMod;
            }
		}

        if (attackCooldown <= 100)
        {
            attackCooldown += attackRechargeRate * Time.deltaTime;
            interactHex.SetFrame((int)(1 - (attackCooldown/100) * (7 * 7))-5);
            speedModifier = currentSpeedMod;
        }
        else
        {
            canAttack = true;
        }

        if (isstunned)
        {
            Stun();
            if(stuntime > 0)
            {
                stuntime -= 1 * Time.deltaTime;
            }
        }

        if(stuntime <= 0)
        {
            unStun();
        }

    }

    void FixedUpdate()
    {
        Ray rayDirection = new Ray(cam.transform.position, cam.transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(rayDirection, out hit, 8.0f))
        {
            if (hit.collider.gameObject.tag == "Larvae")
            {
                if (Input.GetButtonDown("Interact"))
                {
                    hit.collider.gameObject.GetComponent<Larvae>().activated = true;
                    Cmd_SetLarvaeColour(hit.collider.gameObject, Color.green);
                    print("Activated!");
                }
            }

            if(hit.collider.tag == "AntagCollecteble")
            {
                //Prompt
                if(Input.GetButtonDown("Interact"))
                {
                    Destroy(hit.collider.gameObject);
                    speedModifier += 0.1f;
                    currentSpeedMod = speedModifier;

                    StartCoroutine(ShowScreenOverlay(0.8f));
                }
            }
        }



        RaycastAll(rayDirection);

        //TO DO
        //Gravity is always -transform.up
        //Rotate to change gravity
        //Work out how
        //gl future me


        //    RaycastHit downwardHit;
        //if (Physics.Raycast(transform.position, -transform.up, out downwardHit))
        //{
        //    //print(downwardHit.collider.name);
        //    GameObject currentSegment = downwardHit.collider.gameObject.transform.parent.gameObject;

        //    Vector3 currentCenter;
        //    //if (!segmentCenters.ContainsKey(currentSegment))
        //    //{
        //    Bounds segmentBounds = new Bounds(Vector3.zero, Vector3.zero);

        //    Collider[] colliders = currentSegment.GetComponentsInChildren<Collider>();
        //    foreach (Collider collider in colliders)
        //    {
        //        if (segmentBounds.Contains(Vector3.zero))
        //        {
        //            segmentBounds = new Bounds(collider.bounds.center, collider.bounds.size);
        //        }
        //        else
        //        {
        //            segmentBounds.Encapsulate(collider.bounds);
        //        }

        //    }

        //    currentCenter = segmentBounds.center;
        //    //currentCenter.x = transform.position.x;
        //    currentCenter.z = transform.position.z;

        //    //foreach (Renderer rend in currentSegment.GetComponentsInChildren<Renderer>())
        //    //    rend.material.color = Color.red;


        //    //GameObject marker = (GameObject)Instantiate(currentSegment, currentCenter, currentSegment.transform.rotation);
        //    //marker.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        //    print(currentCenter - transform.position);

        //    Quaternion tilt = Quaternion.FromToRotation(transform.up, currentCenter - transform.position);


        //    //transform.up = Vector3.Project(transform.up, currentCenter - transform.position);
        //    transform.rotation *= tilt;
        //}



        //GameObject currentFloor;
        //Vector3 currentFloorOut = transform.up;

        //RaycastHit newFloorHit;
        //RaycastHit currentFloorHit;
        //if (Input.GetKeyDown(KeyCode.LeftControl))
        //    //{

        //    if (Physics.Raycast(transform.position, -transform.up, out currentFloorHit)) { }

        //    if (Physics.Raycast(transform.position, transform.forward, out newFloorHit, 2.0f))
        //    {
        //        //currentFloor = currentFloorHit.collider.gameObject;
        //        //currentFloorOut = hit.normal;
        //        //foreach (Renderer rend in currentFloor.GetComponentsInChildren<Renderer>())
        //        //    rend.material.color = Color.red;

        //        //transform.up = currentFloorOut;

        //Quaternion newRot = transform.rotation;

        //if (newFloorHit.collider.name.Contains("Wall"))
        //{
        //    newRot = Quaternion.Euler(transform.eulerAngles.y, transform.eulerAngles.x, 0);
        //}
        //    else if (newFloorHit.collider.name.Contains("Floor"))
        //{
        //    newRot = 
        //}


        //transform.rotation = newRot * Quaternion.FromToRotation(transform.up, newFloorHit.normal);

        //transform.rotation *= Quaternion.FromToRotation(transform.up, currentFloorHit.normal);
        //transform.up = currentFloorHit.normal;

        //        //print(transform.up);
        //        //print(hit.normal);
        //    }
        // }
        //else
        //{
        //    if (Physics.Raycast(transform.position, -transform.up, out currentFloorHit))
        //    {
        //        //currentFloor = currentFloorHit.collider.gameObject;
        //        //currentFloorOut = currentFloor.transform.up;
        //        //foreach (Renderer rend in currentFloor.GetComponentsInChildren<Renderer>())
        //        //   rend.material.color = Color.red;

        //        if(transform.up != currentFloorHit.normal)
        //        {
        //            print(transform.up + " " + currentFloorHit.normal);
        //            transform.up = currentFloorHit.normal;
        //        }
        //   }
        //}



        //float antagTheta = 0;

        //if(Input.GetKeyDown(KeyCode.Keypad4)) { antagTheta -= 10; }
        //if(Input.GetKeyDown(KeyCode.Keypad6)) { antagTheta += 10; }

        //RaycastHit downwardHit;
        //if (Physics.Raycast(transform.position, -transform.up, out downwardHit))
        //{
        //    //print(downwardHit.collider.name);
        //    GameObject currentSegment = downwardHit.collider.gameObject.transform.parent.gameObject;

        //    Vector3 currentCenter;
        //    //if (!segmentCenters.ContainsKey(currentSegment))
        //    //{
        //    Bounds segmentBounds = new Bounds(Vector3.zero, Vector3.zero);
        //    Collider[] colliders = currentSegment.GetComponentsInChildren<Collider>();
        //    foreach (Collider collider in colliders)
        //    {
        //        if(segmentBounds.Contains(Vector3.zero))
        //        {
        //            segmentBounds = new Bounds(collider.bounds.center, collider.bounds.size);
        //        }
        //        else
        //        {
        //            segmentBounds.Encapsulate(collider.bounds);
        //        }

        //    }

        //    currentCenter = segmentBounds.center;
        //    //currentCenter.x = transform.position.x;
        //    currentCenter.z = transform.position.z;

        //    //foreach (Renderer rend in currentSegment.GetComponentsInChildren<Renderer>())
        //    //    rend.material.color = Color.red;


        //    //GameObject marker = (GameObject)Instantiate(currentSegment, currentCenter, currentSegment.transform.rotation);
        //    //marker.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        //    Vector3 newUpDir = currentCenter - transform.position;




        //    //print(Vector3.Distance(transform.position, currentCenter));
        //    Vector3 newForwardDir = Vector3.Cross(transform.right, newUpDir);

        //    //if (Vector3.Distance(transform.position, currentCenter) > 1.5)
        //    //{
        //    //    Quaternion targetRot = Quaternion.LookRotation(transform.forward, newUpDir);
        //    //    transform.rotation = targetRot;
        //    //}

        //    Quaternion targetRot = Quaternion.LookRotation(newForwardDir, newUpDir);
        //    //transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, 2 * Time.deltaTime);
        //    //transform.rotation = new Quaternion(transform.rotation.x, transform.rotation.y, targetRot.z, targetRot.w);
        //    transform.rotation = targetRot;
        //    rb.AddForce(-transform.up * Physics.gravity.magnitude);



        //    //rb.transform.Rotate(0, 0, antagTheta, Space.Self);

        //if (Input.GetKeyDown(KeyCode.Keypad8))
        //    transform.Rotate(0, 0, 180);

        //Vector3 newForward = transform.forward;
        //RaycastHit newFloorHit;
        //RaycastHit currentFloorHit;
        ////doTransform = false;

        //Physics.Raycast(transform.position, -transform.up, out currentFloorHit);

        //if (Physics.Raycast(transform.position, transform.forward, out newFloorHit, 2.0f))
        //{
        //    if (Input.GetKeyDown(KeyCode.LeftControl))
        //    {
        //        string newFloorName = newFloorHit.transform.name;
        //        string currentFloorName = currentFloorHit.transform.name;
        //        if (newFloorName.Contains("Wall"))
        //        {
        //            bool currentlyOnFloor = currentFloorName.Contains("Floor");
        //            newForward = (currentlyOnFloor ? Vector3.up : -Vector3.up) + new Vector3(transform.forward.y, 0, transform.forward.z);
        //            transform.rotation = Quaternion.LookRotation(newForward, newFloorHit.normal);
        //        }

        //        if(newFloorName.Contains("Floor"))
        //        {
        //            newForward = Vector3.right + transform.forward; //new Vector3(transform.forward.y, transform.forward.x, 0);
        //            transform.rotation = Quaternion.LookRotation(newForward, newFloorHit.normal);
        //        }



        //    }
        //}
        //Vector3 currFwd = transform.forward;
        //Quaternion currRot = transform.rotation;

        //Debug.DrawRay(transform.position, -transform.up, Color.black, 2);
        
        //RaycastHit downwardHit;
        //if(Physics.Raycast(transform.position, -transform.up, out downwardHit))
        //{
        //    //Vector3 targetDir = hit.normal - transform.position;
        //    //Vector3 newDir = Vector3.RotateTowards(transform.up, targetDir, 2 * Time.deltaTime, 0.0f);
        //    //transform.rotation = Quaternion.LookRotation(transform.forward, newDir);
        //    //transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal) * Quaternion.Euler(currRot.eulerAngles.x, 1, currRot.eulerAngles.z);
        //    //Vector3 proj = currFwd - (Vector3.Dot(currFwd, hit.normal)) * hit.normal;
        //    //transform.rotation = Quaternion.LookRotation(proj, hit.normal);
        //    transform.up = Vector3.RotateTowards(transform.up, downwardHit.normal, 2*Time.deltaTime, 0.0f);
        //    //transform.rotation = Quaternion.FromToRotation(transform.up, downwardHit.normal) * transform.rotation;
        //}

        rb.AddForce(-transform.up * Physics.gravity.magnitude);

    }


    void Attack()
    {
        //Transform projSpawn = transform.Find("Camera").Find("ProjSpawn");
        //GameObject proj = (GameObject)Instantiate(projectile, projSpawn.position, projSpawn.rotation);
        //      Cmd_NetworkSpawn(proj, projSpawn);
        //proj.GetComponent<Rigidbody> ().AddForceAtPosition (new Vector3 (10, 1, 0), projSpawn.transform.forward - new Vector3 (1, 0, 0));
        //print (proj.transform.position + " " + transform.position);

        //Quaternion tempRot = projSpawn.transform.rotation * InputTracking.GetLocalRotation(VRNode.CenterEye);
        //Transform tempTrans = Instantiate<Transform>(projSpawn.transform);
        //tempTrans.rotation = tempRot;
        //proj.GetComponent<Rigidbody>().AddForce(-tempTrans.forward * 100 + tempTrans.up);


        Cmd_SpawnProj();

        attackCooldown = 0;
        canAttack = false;
        isShooting = false;
        attackChargeTime = startingAttackChargeTime;

        GetComponent<Animator>().SetBool("isSpitting", false);
    }


    [Command]
    void Cmd_SpawnProj()
    {
        Transform projSpawn = transform.Find("Camera").Find("ProjSpawn");
        GameObject proj = Instantiate(projectile, projSpawn.position, projSpawn.rotation) as GameObject;

        if (isServer)        
            NetworkServer.Spawn(proj);
        else
            NetworkServer.SpawnWithClientAuthority(proj, connectionToClient);

        proj.GetComponent<Rigidbody>().AddForce(projSpawn.transform.forward * 10 + projSpawn.transform.up, ForceMode.Impulse);

    }

    void Stun()
    {
        if (isstunned && stuntime >= 5)
        {
            this.gameObject.GetComponent<BasePlayerController>().enabled = false;
        }
    }

    void unStun()
    {
        protag.SetStunAntag(false);
        stuntime = 5;
        this.gameObject.GetComponent<BasePlayerController>().enabled = true;
    }

    void OnTriggerEnter (Collider col)
    {
        if(col.name == "RespawnCube")
        {
            transform.position = TeleToRes(transform.position);
        }
    }
}
