﻿using UnityEngine;
using System.Collections;

public class HeartBeatScript : MonoBehaviour {

	public float framesPerSecond = 24f;

	private int columns = 8;
	private int rows = 3;
	private int rowOffset = 0;
	private float iX=0;
	private float iY=0;
	private Vector2 _size;
	private Renderer _myRenderer;
	private int _lastIndex = -1;
	private int state = 0; // 0=Beating, 1=FlatLine, 2=Scream

	// initialization
	void Start () {
		_size = new Vector2 (0.125f, 0.125f); //sprite sheet is 8 x 8
		_myRenderer = GetComponent<Renderer>();
		if (_myRenderer == null) enabled = false;
		// _myRenderer.material.SetTextureScale ("_MainTex", _size);
	}

	// Allows another script to change the state to "Beating Heart".
	public void SetBeating (){
		state = 0; 
	}

	// Allows another script to change the state to "Flatline Heart".
	public void SetFlatLine () {
		state = 1;
	}

	// Allows another script to change state to "Scream"
	public void SetScream () {
		state = 2;
	}

	// Update is called once per frame
	void Update () {

		switch(state){
		case 0:
			rowOffset = 0;
			rows = 3;
			break;
		case 1:
			rowOffset = 3;
			rows = 3;
			break;
		case 2:
			rowOffset = 6;
			rows = 2;
			break;
		}

		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond) % (columns*rows);

		if(index != _lastIndex)
		{
			Vector2 offset = new Vector2(iX*_size.x,
				-(_size.y*(iY+rowOffset)));
			iX++;
			if(iX / columns >= 1)
			{
				iX = 0;
				iY++;
				if (iY / rows >= 1) {
				iY = 0;
				}

			}

			_myRenderer.materials[1].SetTextureOffset ("_MainTex", offset);


			_lastIndex = index;
		}
	}

}
