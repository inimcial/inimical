﻿using UnityEngine;
using UnityEngine.VR;
using System.Collections;

public class GeneralStuffScript : MonoBehaviour {

    public bool enableVR;

	// Use this for initialization
	void Start () {
        if (!VRDevice.isPresent)
            enableVR = false;

        VRSettings.enabled = enableVR;
    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButton("CenterVR"))
        {
            InputTracking.Recenter();
        }
	}
}
