﻿using UnityEngine;
using System.Collections;

public class TextAnimGeneral : MonoBehaviour {

	public float framesPerSecond = 20f;
	public int rowOffset = 0;      // value range of 0 - 7
	public int columnOffset = 0;   // value range of 0 - 7
	public int numFrames = 64;     // value range of 1 - 64

	private int columns = 8;
	private int frameCount = 0;
	private float iX=0;
	private float iY=0;
	private Vector2 _size;
	private Renderer _myRenderer;
	private int _lastIndex = -1;

	// initialization
	void Start () {
		_size = new Vector2 (0.125f, 0.125f); //sprite sheet is 8 x 8
		_myRenderer = GetComponent<Renderer>();
		if (_myRenderer == null) enabled = false;
		// _myRenderer.material.SetTextureScale ("_MainTex", _size);
		if (columnOffset > 7) columnOffset = 7;
		if (columnOffset < 0) columnOffset = 0;
		if (rowOffset > 7) rowOffset = 7;
		if (rowOffset < 0) rowOffset = 0;
		if (numFrames < 1) numFrames = 1;
		if ((numFrames + columnOffset + rowOffset * 8) > 64) numFrames = 64 - columnOffset - rowOffset * 8;
		iX = columnOffset;
	}

	// Update is called once per frame
	void Update () {

		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond) % (numFrames);

		if(index != _lastIndex)
		{
			Vector2 offset = new Vector2(iX*_size.x,
				-(_size.y*(iY+rowOffset)));
			iX++;
			if(iX / columns >= 1)
			{
				iX = 0;
                iY++;
			}

			_myRenderer.materials[1].SetTextureOffset ("_MainTex", offset);

			frameCount++;
			if (frameCount >= numFrames) {
				iX = columnOffset;
				iY = 0;
				frameCount = 0;
			}
				
			_lastIndex = index;
		}
	}
}
