﻿using UnityEngine;
using System.Collections;

public class PASystemManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
        System.Random rand = new System.Random();

        float seconds = 4;
        int sound = 3;// rand.Next(0, 16);

        GameObject[] paSystems = GameObject.FindGameObjectsWithTag("PA System");
        foreach (GameObject pa in paSystems)
        {
            PASystem paSys = pa.GetComponent<PASystem>();
            paSys.secondsToWait = seconds;
            paSys.sound = sound;
        }
	}
	
}
