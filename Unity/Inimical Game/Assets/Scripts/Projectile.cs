﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class Projectile : NetworkBehaviour {

	[SerializeField]
	private float lifeTime;

	void Start () {
		Destroy (gameObject, lifeTime);
	}
		
	void OnTriggerEnter(Collider other)
	{
		if (!other.name.Contains ("Antag"))
        {
            if(other.name.Contains("Protag_"))
            {
                print(other.GetComponent<ProtagController>().isDamaged());

                if(!other.GetComponent<ProtagController>().isDamaged())
                {
                    other.GetComponent<ProtagController>().Cmd_DoDamage();
                }
                else
                {
                    GameObject.Find("Spawn Room").transform.Find("Spawn Manager").GetComponent<SpawnManager>().EndGame(0);
                    //BasePlayerController bpc = other.GetComponent<BasePlayerController>();
                    //bpc.Cmd_SpawnAtEnd(0, other.gameObject);

                    //int playersMoved = 0;
                    //while (playersMoved < 2)
                    //{
                    //    foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
                    //    {
                    //        BasePlayerController bpc = player.GetComponent<BasePlayerController>();
                    //        if (bpc.isLocalPlayer && playersMoved < 1)
                    //            continue;
                    //        if (!bpc.isLocalPlayer && playersMoved > 0)
                    //            continue;



                    //        bpc.Cmd_SpawnAtEnd(0, other.gameObject);
                    //        playersMoved++;
                    //        //if (bpc.isServer)
                    //        //    bpc.Rpc_SpawnAtEnd(0, other.gameObject);
                    //        //else
                    //        //    bpc.Cmd_SpawnAtEnd(0, other.gameObject);
                    //        ////player.GetComponent<BasePlayerController>().Cmd_SpawnAtEnd(0, other.gameObject);
                    //        //player.GetComponent<BasePlayerController>().Rpc_SpawnAtEnd(false);
                    //    }
                    //}

                }
            }

            //if (other.tag == "Dummy")
            //{
            //    Destroy(other.gameObject);
            //}
            
            //Destroy (gameObject);
		}
	}
}
