﻿using UnityEngine;
using System;
using System.Collections;

public class MusicSphere : MonoBehaviour {

    [SerializeField, FMODUnity.EventRef]
    private string closeMusic;
    [SerializeField, FMODUnity.EventRef]
    private string midMusic;
    [SerializeField, FMODUnity.EventRef]
    private string farMusic;
    [SerializeField, FMODUnity.EventRef]
    private string nowhereCloseMusic;

    private FMOD.Studio.EventInstance musicEv;

    private ArrayList dists;
    private GameObject protag;
    private string musicPlaying;

    void Start()
    {
        dists = new ArrayList();

        for (int i = 0; i < transform.childCount; i++)
            dists.Add(transform.GetChild(i).GetComponent<SphereCollider>().radius);

        dists.Sort();
        dists.Reverse();

        musicEv = FMODUnity.RuntimeManager.CreateInstance(nowhereCloseMusic);
    }

    void Update()
    {

        foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (p.name.Contains("Protag"))
                protag = p;
        }


        if (ProtagDist() > (float)dists[0])
            ChangeMusic(nowhereCloseMusic);
        else if (ProtagDist() < (float)dists[0] && ProtagDist() > (float)dists[1])
            ChangeMusic(farMusic);
        else if (ProtagDist() < (float)dists[1] && ProtagDist() > (float)dists[2])
            ChangeMusic(midMusic);
        else
            ChangeMusic(closeMusic);

    }

    private float ProtagDist()
    {
		if (protag)
			return Vector3.Distance (transform.position, protag.transform.position);
		else
			return 200;
    }

    private void ChangeMusic(string music)
    {
        if(musicPlaying != music)
        {
            int tpos;
            musicEv.getTimelinePosition(out tpos);
            musicEv.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);

            musicEv = FMODUnity.RuntimeManager.CreateInstance(music);
            musicEv.setProperty(FMOD.Studio.EVENT_PROPERTY.MAXIMUM_DISTANCE, 3000);

            FMOD.ATTRIBUTES_3D musicEvAttrib = new FMOD.ATTRIBUTES_3D();
            musicEvAttrib.position = FMODUnity.RuntimeUtils.ToFMODVector(GameObject.Find("Sounds").transform.position);
            musicEv.set3DAttributes(musicEvAttrib);

            musicEv.setTimelinePosition(tpos);
            musicEv.start();

            musicPlaying = music;
        }
    }
}
