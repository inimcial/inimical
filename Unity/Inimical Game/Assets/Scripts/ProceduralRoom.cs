﻿using UnityEngine;

[ExecuteInEditMode]
public class ProceduralRoom : MonoBehaviour
{

    [SerializeField]
    [Range(2, 12)]
    private int roomWidth = 4;
    //private int currentRoomWidth;
    [SerializeField]
    [Range(2, 12)]
    private int roomHeight = 3;
   // private int currentRoomHeight;

    [SerializeField]
    private GameObject wallPiece;
    [SerializeField]
    private GameObject floorCeiling;
    [SerializeField]
    private GameObject wallCorner;

    private GameObject[,] roomLayout;

    private void OnEnable()
    {
        if (transform.FindChild("Walls").childCount == 0)
            roomLayout = new GameObject[roomWidth, roomHeight];
        else
        {
            roomLayout = new GameObject[roomWidth, roomHeight];
            for (int i = 0; i < transform.FindChild("Walls").childCount / roomHeight; i++)
                for (int j = 0; j < transform.FindChild("Walls").childCount / roomWidth; j++)
                    roomLayout[i, j] = transform.FindChild("Walls").GetChild(i * roomHeight + j).gameObject;
        }
    }

    public void RefreshRoom()
    {
        foreach (GameObject obj in roomLayout)
        {
            DestroyImmediate(obj);
        }

        roomLayout = new GameObject[roomWidth, roomHeight];

        roomLayout[0, 0] = (GameObject)Instantiate(wallCorner, transform.position + new Vector3(0, 0, 2), Quaternion.AngleAxis(-90, Vector3.up));
        roomLayout[0, 0].transform.parent = transform.FindChild("Walls");

        roomLayout[0, roomHeight - 1] = (GameObject)Instantiate(wallCorner, transform.position + new Vector3(0, 0, (roomHeight - 1) * 2), transform.rotation);
        roomLayout[0, roomHeight - 1].transform.parent = transform.FindChild("Walls");

        roomLayout[roomWidth - 1, roomHeight - 1] = (GameObject)Instantiate(wallCorner, transform.position + new Vector3(((roomWidth - 1) * 2) - 2, 0, (roomHeight - 1) * 2), Quaternion.AngleAxis(90, Vector3.up));
        roomLayout[roomWidth - 1, roomHeight - 1].transform.parent = transform.FindChild("Walls");

        roomLayout[roomWidth - 1, 0] = (GameObject)Instantiate(wallCorner, transform.position + new Vector3(((roomWidth - 1) * 2) - 2, 0, 2), Quaternion.AngleAxis(180, Vector3.up));
        roomLayout[roomWidth - 1, 0].transform.parent = transform.FindChild("Walls");

        for (int i = 0; i < roomWidth; i++)
        {
            if (roomLayout[i, 0] == null)
            {
                roomLayout[i, 0] = (GameObject)Instantiate(wallPiece, transform.position + new Vector3((i * 2) - 1, 0, 1), Quaternion.AngleAxis(270, Vector3.up));
                roomLayout[i, 0].transform.parent = transform.FindChild("Walls");
            }
            if (roomLayout[i, roomHeight - 1] == null)
            {
                roomLayout[i, roomHeight - 1] = (GameObject)Instantiate(wallPiece, transform.position + new Vector3((i * 2) - 1, 0, ((roomHeight - 1) * 2) + 1), Quaternion.AngleAxis(90, Vector3.up));
                roomLayout[i, roomHeight - 1].transform.parent = transform.FindChild("Walls");
            }
        }

        for (int j = 0; j < roomHeight; j++)
        {
            if (roomLayout[0, j] == null)
            {
                roomLayout[0, j] = (GameObject)Instantiate(wallPiece, transform.position + new Vector3(-1, 0, (j * 2) + 1), Quaternion.AngleAxis(0, Vector3.up));
                roomLayout[0, j].transform.parent = transform.FindChild("Walls");
            }
            if (roomLayout[roomWidth - 1, j] == null)
            {
                roomLayout[roomWidth - 1, j] = (GameObject)Instantiate(wallPiece, transform.position + new Vector3(((roomWidth - 1) * 2) - 1, 0, (j * 2) + 1), Quaternion.AngleAxis(180, Vector3.up));
                roomLayout[roomWidth - 1, j].transform.parent = transform.FindChild("Walls");
            }
        }

        for (int i = 0; i < roomWidth; i++)
            for (int j = 0; j < roomHeight; j++)
            {
                if (roomLayout[i, j] == null)
                {
                    roomLayout[i, j] = (GameObject)Instantiate(floorCeiling, transform.position + new Vector3(i * 2, 0, j * 2), transform.rotation);
                    roomLayout[i, j].transform.parent = transform.FindChild("Walls");
                }
            }

      //  currentRoomHeight = roomHeight;
      //  currentRoomWidth = roomWidth;
    }
}
