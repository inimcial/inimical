﻿
//using UnityEditor;
using UnityEngine;

[ExecuteInEditMode]
public class EditableWall : MonoBehaviour
{

    private enum wallTypes { BLANK, WINDOW, SINGLE_DOOR, DOUBLE_DOOR, CAPSWALL };
    private enum airconTypes { MEDIUM, LARGE, NONE };

    [SerializeField]
    private wallTypes wallType;
    [SerializeField]
    private airconTypes airconType;
    [SerializeField]
    private GameObject singleDoor;
    [SerializeField]
    private GameObject doubleDoor;
    [SerializeField]
    private GameObject blankWall;
    [SerializeField]
    private GameObject windowWall;
    [SerializeField]
    private GameObject capsWall;

    [SerializeField]
    private GameObject mediumAircon;
    [SerializeField]
    private GameObject largeAircon;
    [SerializeField]
    private GameObject noAircon;

    private GameObject currentWall;
    private GameObject currentAircon;

    public void RefreshWall()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).name.Contains(blankWall.name))
                currentWall = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(singleDoor.name) || singleDoor.name.Contains(transform.GetChild(i).name))
                currentWall = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(doubleDoor.name))
                currentWall = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(windowWall.name))
                currentWall = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(capsWall.name))
                currentWall = transform.GetChild(i).gameObject;


            if (transform.GetChild(i).name.Contains(mediumAircon.name))
                currentAircon = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(largeAircon.name))
                currentAircon = transform.GetChild(i).gameObject;

            if (transform.GetChild(i).name.Contains(noAircon.name))
                currentAircon = transform.GetChild(i).gameObject;
        }

        Transform currWallTrans = currentWall.transform;
        Transform currAirconTrans = currentAircon.transform;

        switch (wallType)
        {
            case wallTypes.BLANK:
                if (!currentWall.name.Contains(blankWall.name))
                {
                    GameObject newWall = (GameObject)Instantiate(blankWall, currWallTrans.position, currWallTrans.rotation);
                    //GameObject newWall = (GameObject)Instantiate(blankWall);
                    newWall.transform.localScale = currWallTrans.localScale;
                    newWall.transform.parent = currWallTrans.parent;
                    DestroyImmediate(currentWall);
                }
                break;
            case wallTypes.SINGLE_DOOR:
                if (!currentWall.name.Contains(singleDoor.name))
                {
                    GameObject newWall = (GameObject)Instantiate(singleDoor, currWallTrans.position, currWallTrans.rotation);
                    //GameObject newWall = (GameObject)Instantiate(singleDoor);
                    newWall.transform.localScale = currWallTrans.localScale;
                    newWall.transform.parent = currWallTrans.parent;
                    DestroyImmediate(currentWall);
                }
                break;
            case wallTypes.DOUBLE_DOOR:
                if (!currentWall.name.Contains(doubleDoor.name))
                {
                    GameObject newWall = (GameObject)Instantiate(doubleDoor, currWallTrans.position, currWallTrans.rotation);
                    //GameObject newWall = (GameObject)Instantiate(doubleDoor);
                    newWall.transform.localScale = currWallTrans.localScale;
                    newWall.transform.parent = currWallTrans.parent;
                    DestroyImmediate(currentWall);
                }
                break;
            case wallTypes.WINDOW:
                if(!currentWall.name.Contains(windowWall.name))
                {
                    GameObject newWall = (GameObject)Instantiate(windowWall, currWallTrans.position, currWallTrans.rotation);
                    newWall.transform.localScale = currWallTrans.localScale;
                    newWall.transform.parent = currWallTrans.parent;
                    DestroyImmediate(currentWall);
                }
                break;

            case wallTypes.CAPSWALL:
                if (!currentWall.name.Contains(capsWall.name))
                {
                    GameObject newWall = (GameObject)Instantiate(capsWall, currWallTrans.position, currWallTrans.rotation);
                    newWall.transform.localScale = currWallTrans.localScale;
                    newWall.transform.parent = currWallTrans.parent;
                    DestroyImmediate(currentWall);
                }
                break;
        }

        switch(airconType)
        {
            case airconTypes.LARGE:
                if(!currentAircon.name.Contains(largeAircon.name))
                {
                    GameObject newAircon = (GameObject)Instantiate(largeAircon, currAirconTrans.position, currAirconTrans.rotation);
                    newAircon.transform.localScale = currAirconTrans.localScale;
                    newAircon.transform.parent = currAirconTrans.parent;
                    DestroyImmediate(currentAircon);
                }
                break;

            case airconTypes.MEDIUM:
                if (!currentAircon.name.Contains(mediumAircon.name))
                {
                    GameObject newAircon = (GameObject)Instantiate(mediumAircon, currAirconTrans.position, currAirconTrans.rotation);
                    newAircon.transform.localScale = currAirconTrans.localScale;
                    newAircon.transform.parent = currAirconTrans.parent;
                    DestroyImmediate(currentAircon);
                }
                break;

            case airconTypes.NONE:
                if (!currentAircon.name.Contains(noAircon.name))
                {
                    GameObject newAircon = (GameObject)Instantiate(noAircon, currAirconTrans.position, currAirconTrans.rotation);
                    newAircon.transform.localScale = currAirconTrans.localScale;
                    newAircon.transform.parent = currAirconTrans.parent;
                    DestroyImmediate(currentAircon);
                }
                break;
        }

    }
}