﻿using UnityEngine;

[ExecuteInEditMode]
public class ProceduralHallway : MonoBehaviour {

    [SerializeField][Range(1, 50)]
    private int hallwayLength = 1;
    //private int currentHallwayLength;

    [SerializeField]
    private int stepDistance = 2;
    //private int currentStepDistance;

    [SerializeField]
    private GameObject hallwaySegment;
    private GameObject[] hallSegments;

    [SerializeField]
    private GameObject hallCorner;
    [SerializeField]
    private GameObject hallTIntersection;
    [SerializeField]
    private GameObject hallCrossroad;


    private enum hallCapType {LEFT_TURN, RIGHT_TURN, T_INTERSECTION, CROSSROAD, NONE};
    [SerializeField]
    private hallCapType startCapType = hallCapType.NONE;
    //private hallCapType currentStartCapType;
    [SerializeField]
    private hallCapType endCapType = hallCapType.NONE;
   // private hallCapType currentEndCapType;


    private void OnEnable()
    {
        if (transform.Find("Walls").childCount == 0)
            hallSegments = new GameObject[hallwayLength + 2];
        else
        {
            hallSegments = new GameObject[transform.Find("Walls").childCount];
            for (int i = 0; i < transform.Find("Walls").childCount; i++)
            {
                hallSegments[i] = transform.Find("Walls").GetChild(i).gameObject;
            }
        }
    }

    //public void Update()
    //{
    //    if (Application.isEditor && !Application.isPlaying)
    //    {
    //        RefreshHall();
    //    }  
    //}

    public void RefreshHall()
    {
        //if((currentHallwayLength != hallwayLength || currentStepDistance != stepDistance || currentEndCapType != endCapType || currentStartCapType != startCapType) && hallwaySegment != null)
        //{
            
            foreach(GameObject obj in hallSegments)
            {
                DestroyImmediate(obj);
            }

            hallSegments = new GameObject[hallwayLength + 2];

            float hallRot = transform.rotation.eulerAngles.y;

            float tempRot = hallRot;
            transform.Rotate(Vector3.up, -hallRot);
            hallRot = 0;

            Vector3 endCapPos = Quaternion.AngleAxis(hallRot, Vector3.up) * (transform.position + new Vector3(1, 0, hallwayLength * stepDistance + 1));
            Vector3 startCapPos = Quaternion.AngleAxis(hallRot, Vector3.up) * (transform.position + new Vector3(1, 0, -1));


            for (int i = 1; i < hallwayLength; i++)
            {
				
                hallSegments[i] = (GameObject)Instantiate(hallwaySegment, Quaternion.AngleAxis(hallRot, Vector3.up) * (transform.position + new Vector3(0, 0, i*stepDistance)), transform.rotation);

                hallSegments[i].transform.parent = transform.Find("Walls");
                
            }
            
			
           
            switch (endCapType)
            {
                case hallCapType.LEFT_TURN:
                    hallSegments[hallwayLength + 1] = (GameObject)Instantiate(hallCorner, endCapPos, transform.rotation);
                    hallSegments[hallwayLength + 1].transform.localScale = new Vector3(-1, 1, 1);
                    hallSegments[hallwayLength + 1].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.RIGHT_TURN:
                    hallSegments[hallwayLength + 1] = (GameObject)Instantiate(hallCorner, endCapPos, transform.rotation);
                    hallSegments[hallwayLength + 1].transform.localScale = new Vector3(1, 1, 1);
                    hallSegments[hallwayLength + 1].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.T_INTERSECTION:
                    hallSegments[hallwayLength + 1] = (GameObject)Instantiate(hallTIntersection, endCapPos, transform.rotation);
                    hallSegments[hallwayLength + 1].transform.localScale = new Vector3(1, 1, 1);
                    hallSegments[hallwayLength + 1].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.CROSSROAD:
                    hallSegments[hallwayLength + 1] = (GameObject)Instantiate(hallCrossroad, endCapPos, transform.rotation);
                    hallSegments[hallwayLength + 1].transform.localScale = new Vector3(1, 1, 1);
                    hallSegments[hallwayLength + 1].transform.parent = transform.Find("Walls");
                    break;
                default:
                    break;
            }

            switch (startCapType)
            {
                case hallCapType.LEFT_TURN:
                    hallSegments[0] = (GameObject)Instantiate(hallCorner, startCapPos, transform.rotation);
                    hallSegments[0].transform.localScale = new Vector3(-1, 1, -1);
                    hallSegments[0].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.RIGHT_TURN:
                    hallSegments[0] = (GameObject)Instantiate(hallCorner, startCapPos, transform.rotation);
                    hallSegments[0].transform.localScale = new Vector3(1, 1, -1);
                    hallSegments[0].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.T_INTERSECTION:
                    hallSegments[0] = (GameObject)Instantiate(hallTIntersection, startCapPos, transform.rotation);
                    hallSegments[0].transform.localScale = new Vector3(-1, 1, -1);
                    hallSegments[0].transform.parent = transform.Find("Walls");
                    break;
                case hallCapType.CROSSROAD:
                    hallSegments[0] = (GameObject)Instantiate(hallCrossroad, startCapPos, transform.rotation);
                    hallSegments[0].transform.localScale = new Vector3(-1, 1, -1);
                    hallSegments[0].transform.parent = transform.Find("Walls");
                    break;
                default:
                    break;
            }

            transform.Rotate(Vector3.up, tempRot);
            hallRot = tempRot;

            //currentStepDistance = stepDistance;
            //currentHallwayLength = hallwayLength;
            //currentStartCapType = startCapType;
            //currentEndCapType = endCapType;
        //}
    }
}