﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class CustomNetworkManager : NetworkManager {

    //[SerializeField]
    //GameObject protagPrefab;
    //[SerializeField]
    //GameObject antagPrefab;
    //[SerializeField]
    //GameObject spawnMenu;

    //private string selectedChar;

    public int playersConnected = 0;

    //public override void OnClientConnect(NetworkConnection conn)
    //{
    //    playersConnected++;
    //    base.OnClientConnect(conn);
    //}

    public override void OnServerConnect(NetworkConnection conn)
    {
        
        playersConnected++;
        base.OnServerConnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        playersConnected--;
        base.OnClientDisconnect(conn);
    }

    public override void OnStopHost()
    {
        playersConnected = 0;
        base.OnStopHost();
    }


    //public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    //{
    //    if (protagPrefab == null || antagPrefab == null)
    //    {
    //        if (LogFilter.logError) { Debug.LogError("A player prefab is empty on the NetworkManager. Please setup the player objects."); }
    //        return;
    //    }

    //    if (protagPrefab.GetComponent<NetworkIdentity>() == null || antagPrefab.GetComponent<NetworkIdentity>() == null)
    //    {
    //        if (LogFilter.logError) { Debug.LogError("A player prefab does not have a NetworkIdentity. Please add a NetworkIdentity to the player prefabs."); }
    //        return;
    //    }

    //    if (playerControllerId < conn.playerControllers.Count && conn.playerControllers[playerControllerId].IsValid && conn.playerControllers[playerControllerId].gameObject != null)
    //    {
    //        if (LogFilter.logError) { Debug.LogError("There is already a player at that playerControllerId for this connections."); }
    //        return;
    //    }

    //    spawnMenu.SetActive(true);

    //    StartCoroutine(WaitForCharSelect(conn, playerControllerId));

    //}

    //private void SpawnPlayer(NetworkConnection conn, short playerControllerId)
    //{
    //    GameObject player;
    //    Transform startPos = GetStartPosition();

    //    if (startPos != null)
    //    {
    //        if (selectedChar == "Antag")
    //            player = (GameObject)Instantiate(antagPrefab, startPos.position, startPos.rotation);
    //        else
    //            player = (GameObject)Instantiate(protagPrefab, startPos.position, startPos.rotation);
    //    }
    //    else
    //    {
    //        if (selectedChar == "Antag")
    //            player = (GameObject)Instantiate(antagPrefab, Vector3.zero, Quaternion.identity);
    //        else
    //            player = (GameObject)Instantiate(protagPrefab, Vector3.zero, Quaternion.identity);
    //    }

    //    NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
    //}

    //IEnumerator WaitForCharSelect(NetworkConnection conn, short playerControllerId)
    //{
    //    while (selectedChar != "Antag" && selectedChar != "Protag") 
    //    {
    //        string antagButtonSelectedChar = spawnMenu.transform.FindChild("AntagButton").GetComponent<MenuButton>().selectedChar;
    //        string protagButtonSelectedChar = spawnMenu.transform.FindChild("ProtagButton").GetComponent<MenuButton>().selectedChar;

    //        if (antagButtonSelectedChar == "Antag")
    //            selectedChar = antagButtonSelectedChar;
    //        else if (protagButtonSelectedChar == "Protag")
    //            selectedChar = protagButtonSelectedChar;

    //        yield return null;
    //    }

    //    SpawnPlayer(conn, playerControllerId);

    //}

}
