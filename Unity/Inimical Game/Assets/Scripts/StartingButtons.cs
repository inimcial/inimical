﻿using UnityEngine;
using System.Collections;

public class StartingButtons : MonoBehaviour {
    
    public void DoAction()
    {
        if(name.Contains("Host"))
        {
            CustomNetworkManager.singleton.StartHost();
        }
        else if(name.Contains("Join"))
        {
            CustomNetworkManager.singleton.networkAddress = "127.0.0.1";
            CustomNetworkManager.singleton.networkPort = 7777;
            CustomNetworkManager.singleton.StartClient();
        }
    }
}
