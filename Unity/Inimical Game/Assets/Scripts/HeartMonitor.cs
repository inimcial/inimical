﻿using UnityEngine;

public class HeartMonitor : MonoBehaviour {

    [SerializeField, FMODUnity.EventRef]
    private string beepingSound = "event:/Ambience and Event Sounds/Heart Rate Monitor- Steady Beat";
    private FMOD.Studio.EventInstance beepingEv;

    [SerializeField, FMODUnity.EventRef]
    private string flatlineSound = "event:/Ambience and Event Sounds/Heart Rate Monitor- Fail";

    private bool activateFlatline = false;
    private bool hasActivated = false;
    private bool protagInTrigger = false;

    // Use this for initialization
    void Start () {
        beepingEv = FMODUnity.RuntimeManager.CreateInstance(beepingSound);
        FMOD.ATTRIBUTES_3D beepEvAttrib = new FMOD.ATTRIBUTES_3D();
        beepEvAttrib.position = FMODUnity.RuntimeUtils.ToFMODVector(transform.position);
        beepingEv.set3DAttributes(beepEvAttrib);
        beepingEv.start();
    }
	
	// Update is called once per frame
	void Update () {
        
        if(activateFlatline && !hasActivated)
        {
            beepingEv.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);
            FMODUnity.RuntimeManager.PlayOneShotAttached(flatlineSound, gameObject);
            hasActivated = true;
        }
	}

    public void lookedAt()
    {
        print(protagInTrigger);
        if(protagInTrigger)
            activateFlatline = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("Protag"))
            protagInTrigger = true;
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Protag"))
            protagInTrigger = false;
    }
}
