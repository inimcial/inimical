﻿using UnityEngine;
using UnityEngine.Networking;

public class Door : NetworkBehaviour
{
    [SerializeField]
    private GameObject door;

    [SerializeField]
    [SyncVar]
    private bool redCard;
    [SerializeField]
    [SyncVar]
    private bool greenCard;
    [SerializeField]
    [SyncVar]
    private bool blueCard;
    [SerializeField]
    [SyncVar]
    private bool yellowCard;

    [SerializeField]
    [SyncVar]
    private bool locked;

    [SerializeField]
    private float openSpeed = 2;

    [FMODUnity.EventRef]
    [SerializeField]
    private string doorSound = "event:/DoorOpen";

    private bool playedSound;

    [SyncVar]
    private bool doOpen = false;
    [SyncVar]
    private bool doClose = false;

    // Use this for initialization
    void Start()
    {
        if (!door)
            if(transform.Find("W_STAINLESS_SDOOR_Prefab"))
                door = transform.Find("W_STAINLESS_SDOOR_Prefab").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (!door)
            return;

        if (doOpen)
            OpenDoor();
        //player.GetComponent<BasePlayerController>().Cmd_ChangeDoorState(gameObject, true);

        if (doClose)
            CloseDoor();
           // player.GetComponent<BasePlayerController>().Cmd_ChangeDoorState(gameObject, false);


    }


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            //print(locked + " " + greenCard + " " + blueCard + " " + redCard + " " + yellowCard);
            if (!locked && !greenCard && !blueCard && !redCard && !yellowCard)
            {
                doOpen = true;
                doClose = false;
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            doClose = true;
            doOpen = false;
        }
    }

    public void OpenDoor()
    {
        if (!playedSound)
        {
            FMODUnity.RuntimeManager.PlayOneShot(doorSound, transform.position);
            playedSound = true;
        }

        //if (door.transform.localRotation.eulerAngles.z > 270)
        //    door.transform.Rotate(-Vector3.forward, Time.deltaTime * openSpeed, Space.Self);
        if (door.transform.localPosition.y > 0.3)
            door.transform.Translate(0f, -Time.deltaTime * openSpeed, 0f, Space.Self);
        else
        {
            doOpen = false;
        }
    }

    public void CloseDoor()
    {
        if (playedSound)
            playedSound = false;

        //if (door.transform.localRotation.eulerAngles.z < 359)
        //    door.transform.Rotate(-Vector3.forward, Time.deltaTime * -openSpeed, Space.Self);
        if (door.transform.localPosition.y <= 2)
            door.transform.Translate(0f, Time.deltaTime * openSpeed, 0f, Space.Self);
        else
        {
            doClose = false;
        }
    }

    public void SetColour(string colour, bool enabled = false)
    {
        switch (colour)
        {
            case "Blue":
                blueCard = enabled;
                break;
            case "Red":
                redCard = enabled;
                break;
            case "Green":
                greenCard = enabled;
                break;
            case "Yellow":
                yellowCard = enabled;
                break;
        }
    }
}

