﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class SpawnRoom : NetworkBehaviour {

    public string wallText { set { roomTextTextText = value; } }

    CustomNetworkManager netMan;
    GameObject roomText;
    GameObject characters;
    TextMesh roomTextText;

    public GameObject antag;
    public GameObject protag;

    GameObject[] players;

    //[SyncVar(hook = "OnCharChange")]
    bool showChars;
    //[SyncVar(hook = "OnTextChange")]
    //bool showText;
    [SyncVar(hook = "OnPlayersConnectedChanged")]
    int playersConnected;
    [SyncVar(hook = "OnRoomTextTextTextChanged")]
    string roomTextTextText;
    [SyncVar(hook = "OnGameStartingChanged")]
    int gameStarting;
    [SerializeField][SyncVar(hook = "OnTimeTillStartChanged")]
    int timeTillStart;

    void Awake()
    {
        netMan = transform.parent.GetComponent<CustomNetworkManager>();
        characters = transform.parent.Find("Contents").Find("Characters").gameObject;
        roomText = transform.parent.Find("Contents").Find("SpawnRoomText").gameObject;
        roomTextText = roomText.GetComponent<TextMesh>();
        gameStarting = 0;
        //timeTillStart = 10;
        showChars = false;
        //showText = true;
    }

    void Update()
    {
        playersConnected = netMan.playersConnected;

        if(playersConnected == 0 && gameStarting == 0)
        {
            if (roomText.activeSelf)
                roomText.SetActive(false);
            //characters.SetActive(false);
            showChars = false;
        }
        else if (playersConnected == 2 && gameStarting == 0)
        {
            if (!roomText.activeSelf)
                roomText.SetActive(true);
            characters.SetActive(false);
            showChars = false;
            roomTextTextText = "Waiting for second player";
            roomTextText.text = roomTextTextText;
        }
        else if (playersConnected == 3 && gameStarting == 0)
        {
            //         if(!roomText.activeSelf)
            //             roomText.SetActive(true);

            //players = GameObject.FindGameObjectsWithTag("Player");

            //         if(players.Length > 0)
            //             players[0].GetComponent<BasePlayerController>().CmdSetCharactersActive(characters, true);

            //foreach (GameObject player in players)
            //{
            //    print(player.name);
            //}

            //         roomTextTextText = "Select your character";
            //         roomTextText.text = roomTextTextText;

            //characters.SetActive(true);
            //showChars = true;

            if (!roomText.activeSelf)
                roomText.SetActive(true);

            NetworkServer.Spawn(characters);

            //RpcSetCharactersActive(characters, true);

            players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
                player.GetComponent<BasePlayerController>().character = "none";

            roomTextText.text = "Select your character";


        }
        else if(gameStarting >= 1)
        {
            if (!roomText.activeSelf)
                roomText.SetActive(true);
            roomTextTextText = timeTillStart + " till start.";
            roomTextText.text = roomTextTextText;
        }
        else
        {
            if(!roomText.activeSelf)
                roomText.SetActive(true);
            roomTextTextText = "Too many players connected, or something went terribly wrong";
            roomTextText.text = roomTextTextText;
        }


        if(characters.transform.childCount <= 1 && gameStarting == 0)
        {
            print("Game Starting...");
			StartCoroutine(StartGameTimer());
        }

        if (timeTillStart == 0 && gameStarting == 1)
        {
            roomTextTextText = "";
            roomTextText.text = roomTextTextText;
            StartGame();
        }

        //if (roomText.activeSelf != showText)
        //    showText = roomText.activeSelf;
//        if (characters.activeSelf != showChars)
//            showChars = characters.activeSelf;

        if (isServer && !showChars && characters.activeSelf)
        {
           // RpcSetCharactersActive(characters, characters.activeSelf);
            showChars = characters.activeSelf;
        }
    }

    IEnumerator StartGameTimer()
    {
        gameStarting = 1;

        for(; timeTillStart > 0; timeTillStart--)
        {
            yield return new WaitForSeconds(1); 
        }
    }

    void StartGame()
    {
        gameStarting = 2;

        players = GameObject.FindGameObjectsWithTag("Player");

        print("Start game");

        GameObject otherPlayer;

        for(int i = 0; i < players.Length; i++)
        {
            if (i == 0)
                otherPlayer = players[1];
            else
                otherPlayer = players[0];

            BasePlayerController pc = players[i].GetComponent<BasePlayerController>();
			BasePlayerController otherPc = otherPlayer.GetComponent<BasePlayerController> ();
            //pc.FadeToBlack();
			if(pc.character == "none")
            {
                if (otherPc.character == "protag")
                    pc.character = "antag";
				if (otherPc.character == "antag")
                    pc.character = "protag";
				if (otherPc.character == "none")
                    pc.character = new System.Random().Next(0, 1) == 0 ? "antag" : "protag";
            }

			print ("Pc: " + pc.character + ", " + pc.isServer);
			print ("OtherPc: " + otherPc.character + ", " + otherPc.isServer);

            if(pc.character == "protag")
            {
				print ("Doing Protag Command");
				pc.Cmd_SpawnProtag (players [i], protag);
                //int randNum = rand.Next(0, protagSpawns.Length-1);
                //players[i].transform.position = protagSpawns[randNum].transform.position + new Vector3(0, 1.1f, 0);
                //players[i].transform.rotation = protagSpawns[randNum].transform.rotation;
            }
            else
            {
                print("Doing Antag Command.");
                pc.Cmd_SpawnAntag(players[i], antag);

                //players[i].transform.position = antagSpawns[randNum].transform.position + new Vector3(0, 1.1f, 0);
                //players[i].transform.rotation = antagSpawns[randNum].transform.rotation;

                //Destroy(players[i].GetComponent<ProtagController>());
                //Destroy(players[i].GetComponent<Inventory>());

                //AntagController antagCont = players[i].AddComponent<AntagController>();
                //antagCont.Init();
                //antagCont.character = "antag";

                //players[i].transform.FindChild("Graphics").FindChild("WomanSimpRig").gameObject.SetActive(false);
                //players[i].transform.FindChild("Graphics").FindChild("EXTROscaledrigRIGGEDanimate").gameObject.SetActive(true);

                //players[i].GetComponent<CapsuleCollider>().center = antag.GetComponent<CapsuleCollider>().center; 
                //players[i].GetComponent<CapsuleCollider>().radius = antag.GetComponent<CapsuleCollider>().radius; 
                //players[i].GetComponent<CapsuleCollider>().height = antag.GetComponent<CapsuleCollider>().height; 
                //players[i].GetComponent<CapsuleCollider>().direction = antag.GetComponent<CapsuleCollider>().direction;

                //players[i].transform.FindChild("Camera").position = antag.transform.FindChild("Camera").position;

                //players[i].GetComponent<Animator>().runtimeAnimatorController = antag.GetComponent<Animator>().runtimeAnimatorController;
                //players[i].GetComponent<Animator>().avatar = antag.GetComponent<Animator>().avatar;

                //players[i].GetComponent<NetworkAnimator>().animator = antag.GetComponent<NetworkAnimator>().animator;



            }
        }
    }

    private void OnPlayersConnectedChanged(int playersConnectedChange)
    {
        playersConnected = playersConnectedChange;
    }

    private void OnRoomTextTextTextChanged(string roomTextTextTextChange)
    {
        roomTextTextText = roomTextTextTextChange;
    }

    private void OnGameStartingChanged(int gameStartingChange)
    {
        gameStarting = gameStartingChange;
    }

    private void OnTimeTillStartChanged(int timeTillStartChange)
    {
        timeTillStart = timeTillStartChange;
    }

    private void OnCharChange(bool showCharsChange)
    {
        characters.SetActive(showCharsChange);
        print("1");
    }

    private void OnTextChange(bool showTextChange)
    {
        roomText.SetActive(showTextChange);
        print("2");
    }


    [Command]
    public void CmdDoGameOver(string winner)
    {
        foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.GetComponent<BasePlayerController>().Cmd_SpawnProtagAtSpawnRoom();
        }

        roomTextTextText = "Game Over. " + winner + " wins!";
		roomTextText.text = roomTextTextText;
    }

    [ClientRpc]
    private void RpcSetCharactersActive(GameObject chars, bool active)
    {
        chars.SetActive(active);
    }

    [Command]
    public void CmdSetCharactersActive(GameObject chars, bool active)
    {
        chars.SetActive(active);
        RpcSetCharactersActive(chars, active);
    }

}
