﻿using UnityEngine;
using UnityEngine.VR;
using UnityEngine.Networking;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(PlayerMotor))]
public class ProtagController : BasePlayerController
{
    [SerializeField][FMODUnity.EventRef]
    private string damageSound = "event:/Protag/PlayerDamage";

    [SerializeField]
    [FMODUnity.EventRef]
    private string footstepSound = "event:/Protag/ProtagFootstep";
    private FMOD.Studio.EventInstance footstepEv;

    [SerializeField]
    [FMODUnity.EventRef]
    private string outOfBreathSound = "event:/Protag/OutOfBreath";

    [SerializeField]
    [FMODUnity.EventRef]
    private string deathSound = "event:/Protag/PlayerDeath";

    [SerializeField]
    [FMODUnity.EventRef]
    private string breathingNormalSound = "event:/Protag/ProtagBreathingNormal";

    [SerializeField, FMODUnity.EventRef]
    private string scareStab = "event:/Ambience and Event Sounds/Stabbing Music Event";

    private int maxHealth = 100;

    [SerializeField]
    public bool hasDied = false;

    [SerializeField]
    public int ProtagHealth = 100;

    private Flashlight flashscript;
    public Light flashlight;
    public Light HelperLight;
    [SyncVar]
    public string InventoryItems = "";
    private bool HasHelperLight = false;
    private bool hasKeytoBox = false;
    private bool flickeringOn = false;

    public GameObject[] allHints;
    public GameObject[] allKeys;
    
    [SerializeField]
    private bool StunAntag = false;

    public bool pickedup = false;

    private Inventory inv;

    [SerializeField]
    private float pickupDistance = 3;
    public Transform head;

    private bool hasChosen;

    [SerializeField]
    private float staminaDrainRate;

    [SerializeField]
    private float staminaChargeRate;

    private float stamina;
    private bool staminaIsRecharging;

    private bool takenDamage = false;

    [SerializeField]
    private GameObject pauseMenu;

    private bool isCrouching;

    private bool showInteractHex;
    private SpriteSheet interactHex;

    private bool interactHeld;

    private int shownHint = 0;



    void Start()
    {
        //        if (VRSettings.enabled)
        //        {
        //            cam = transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").gameObject;
        //            transform.FindChild("Camera").gameObject.SetActive(false);
        //        }
        //        else
        //        {
        //            cam = transform.FindChild("Camera").gameObject;
        //            //transform.FindChild("TrackingSpace").FindChild("CenterEyeAnchor").gameObject.SetActive(false);
        //        }

        ChangeLighting();
       
        if (VRSettings.enabled)
            InputTracking.Recenter();
        

        cam = transform.Find("CamParent").FindChild("Camera").gameObject;
        interactHex = cam.transform.Find("Crosshair").GetChild(0).GetComponent<SpriteSheet>();
        //ftbSphere = transform.Find("Camera").Find("FadeToBlackSphere").gameObject;
        //ftbsRenderer = ftbSphere.GetComponent<Renderer>();
        //FadeToClear();

        localConn = connectionToClient;

        //character = "none";

        motor = GetComponent<PlayerMotor>();
        inv = GetComponent<Inventory>();

        startingLookSensitivity = lookSensitivity;

        stamina = 100;
        staminaIsRecharging = false;

        ProtagHealth = maxHealth;
        hasDied = false;

        showMenu = true;
        ShowMenu();
        allHints = new GameObject[4];
        allKeys = new GameObject[4];
        
        FMODUnity.RuntimeManager.PlayOneShotAttached(breathingNormalSound, gameObject);
        footstepEv = FMODUnity.RuntimeManager.CreateInstance(footstepSound);
        footstepEv.start();
        footstepEv.setPaused(true);
        //if(isLocalPlayer)
        //    transform.Find("Graphics").gameObject.SetActive(false);
       
    }

    void Update()
    {

        if (allKeys[0] == null)
        {
            pauseMenu.SetActive(true);
            
            foreach (GameObject key in GameObject.FindGameObjectsWithTag("MenuCards"))
            {
                print(key.name);
                if (key.name == "Green")
                {
                    allKeys[0] = key;
                }

                if (key.name == "Blue")
                {
                    allKeys[1] = key;
                }

                if (key.name == "Red")
                {
                    allKeys[2] = key;
                }

                if (key.name == "Yellow")
                {
                    allKeys[3] = key;
                }
            }
        }

        if (allHints[0] == null)
        {
            foreach (GameObject hint in GameObject.FindGameObjectsWithTag("GameHints"))
            {
                if (hint.name == "GreenHint")
                {
                    allHints[0] = hint;
                }

                if (hint.name == "BlueHint")
                {
                    allHints[1] = hint;
                }

                if (hint.name == "RedHint")
                {
                    allHints[2] = hint;
                }

                if (hint.name == "YellowHint")
                {
                    allHints[3] = hint;
                    pauseMenu.SetActive(false);
                }
            }
        }

        if(gameObject.GetComponent<Animator>().GetBool("takenDamage"))
        {
            gameObject.GetComponent<Animator>().SetBool("takenDamage", false);
        }
        
        if (VRDevice.isPresent == true && string.IsNullOrEmpty(VRDevice.model))
        {
            Debug.Log("EMPTY VR DEVICE DETECTED!");
            VRSettings.loadedDevice = VRDeviceType.Oculus;
            //VRSettings.LoadDeviceByName("Oculus");
            VRSettings.enabled = true;
            VRSettings.showDeviceView = true;
        }

        if (stamina > 0)
        {
            if (staminaIsRecharging)
            {
                GetComponent<Animator>().SetBool("run", false);

                if (stamina < 100)
                    stamina += staminaChargeRate * Time.deltaTime;
                else
                    staminaIsRecharging = false;
            }
            else
            {
                if (Input.GetButton("Run") && velocity != Vector3.zero)
                {
                    GetComponent<Animator>().SetBool("run", true);
                    stamina -= staminaDrainRate * Time.deltaTime;
                }
                else
                {
                    GetComponent<Animator>().SetBool("run", false); 
                }
            }
            if (stamina < 100)
                stamina += (staminaChargeRate / 3) * Time.deltaTime;
        }
        else
        {
            FMODUnity.RuntimeManager.PlayOneShotAttached(outOfBreathSound, gameObject);
            staminaIsRecharging = true;
            stamina = 0.1f;
        }

        FMOD.ATTRIBUTES_3D footstepEvAttrib = new FMOD.ATTRIBUTES_3D();
        footstepEvAttrib.position = FMODUnity.RuntimeUtils.ToFMODVector(transform.position);
        footstepEv.set3DAttributes(footstepEvAttrib);

        if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            bool fsPause;
            footstepEv.getPaused(out fsPause);
            if (fsPause)
                footstepEv.setPaused(false);
        }
        else
            footstepEv.setPaused(true);

        bool isRunning = GetComponent<Animator>().GetBool("run");

        Vector3 runningCamPos = new Vector3(0, 0, 0.05f);
        Vector3 crouchingCamPos = new Vector3(0, -0.6f, 0.08f);
        Vector3 currentCamPos = cam.transform.parent.localPosition;

        //if the player is running or crouching, move their camera infront of where their face ends up
        //Stops them clipping through their own face.
        if (isRunning && !Mathf.Approximately(Vector3.Distance(currentCamPos,runningCamPos), 0))
            cam.transform.parent.localPosition = Vector3.MoveTowards(cam.transform.parent.localPosition, runningCamPos, Time.deltaTime);
        if (isCrouching && !Mathf.Approximately(Vector3.Distance(currentCamPos, crouchingCamPos), 0))
            cam.transform.parent.localPosition = Vector3.MoveTowards(cam.transform.parent.localPosition, crouchingCamPos, Time.deltaTime);

        if (!isRunning && !isCrouching && !Mathf.Approximately(Vector3.Distance(currentCamPos, Vector3.zero), 0))
            cam.transform.parent.localPosition = Vector3.MoveTowards(cam.transform.parent.localPosition, Vector3.zero, Time.deltaTime);


        DoMovement(stamina, staminaIsRecharging);


        if (Input.GetButtonDown("Crouch"))
        {
            isCrouching = !isCrouching;
            GetComponent<Animator>().SetBool("crouch", isCrouching);
            speedModifier = (takenDamage ? (isCrouching ? 0.5f : 0.8f) : (isCrouching ? 0.8f : 1.0f));
        }

        if (Input.GetButtonDown("Menu"))
        {
            ShowMenu();
        }

        if (Input.GetButtonDown("ResetVRView") && VRSettings.enabled)
        {
            InputTracking.Recenter();
        }

        if (Input.GetButtonUp("Interact"))
            interactHeld = false;

        if (Input.GetButtonDown("Interact"))
            interactHeld = true;


        if (Input.GetKeyDown(KeyCode.F9))
            transform.position = TeleToRes(transform.position);

        if (isCrouching)
            footstepEv.setVolume(0.3f);
        else if (isRunning)
            footstepEv.setVolume(1.0f);
        else
            footstepEv.setVolume(0.7f);

        if(head)
        {
            head.rotation = cam.transform.rotation;
            head.position = cam.transform.position;
        }

        //foreach (System.Collections.DictionaryEntry invItem in inv.inventory)
        //{
        //    print(invItem.Key + ": " +invItem.Value);
        //}
        CheckInventory();
        EnableFlashlight();

    }

    public bool GetStunAntag ()
    {
        return StunAntag;
    }

    public bool SetStunAntag (bool value)
    {
        StunAntag = value;
        return StunAntag;
    }

    void FixedUpdate()
    {

        RaycastHit hit;
        Ray rayDirection = new Ray(cam.transform.position, cam.transform.forward);

        if (Physics.Raycast(rayDirection, out hit, 25f))
        {
            if (hit.collider.gameObject.name.Contains("Antag"))
            {
                GameObject antag = hit.collider.gameObject;

                if (InventoryItems.Contains("HintFlashLight") && flashlight.enabled)
                {
                    print("Should be stunned");
                    //if (isServer)
                    //    antag.GetComponent<AntagController>().Stun(2);
                    //else
                    //antag.GetComponent<AntagController>().Cmd_Stun(2);
                    //StunAntag = true;
                    //Cmd_SetStunState(antag, true);
                }
            }
        }


        //if(hit.collider.name == "whitescreen")
        //{
        //    if (Time.deltaTime == 0.5f)
        //    {
        //        float time = 0;
                
        //        FMODUnity.RuntimeManager.PlayOneShotAttached(scareStab, gameObject);

        //        flickeringOn = true;
        //        StartCoroutine(Flickeroff(hit.collider.gameObject));
        //        time += Time.deltaTime * 1;

        //        if (time == 3)
        //        {
        //            StopCoroutine(Flickeroff(hit.collider.gameObject));
        //        }
        //    }
        //}

        if (Physics.Raycast(rayDirection, out hit, pickupDistance))
        {
            if (hit.collider.gameObject.tag == "CollectableItem")
            {
                string hitObjName = hit.collider.gameObject.name;

                if (hit.transform.Find("GuideText"))
                {
                    hit.transform.Find("GuideText").gameObject.SetActive(true);
                }

                //hit.transform.FindChild("Particle System").GetComponent<ParticleSystem>().Play();
                //if (tutorialsOn)
                //hit.transform.FindChild("Tutorial").gameObject.SetActive(true);

                if (Input.GetButtonDown("Interact"))
                {

                    if (hit.collider.gameObject.name == "RedHint")
                    {
                        InventoryItems += hitObjName + " ";
                        inv.inventory["Red"] = true;
                        Destroy(hit.collider.gameObject);
                        Cmd_NetworkDestroy(hit.collider.gameObject);

                    }

                    else if (hit.collider.gameObject.name == "GreenHint")
                    {
                        InventoryItems += hitObjName + " ";
                        inv.inventory[hitObjName] = true;
                        Destroy(hit.collider.gameObject);
                        Cmd_NetworkDestroy(hit.collider.gameObject);

                        foreach (GameObject obj in GameObject.FindGameObjectsWithTag("Menuprompt"))
                        {
                            obj.GetComponent<Renderer>().enabled = true;
                            Destroy(obj, 3f);
                            //Cmd_NetworkDestroy(obj);
                        }
                    }
                   else if (hit.collider.gameObject.name == "BlueHint")
                    {
                        InventoryItems += hitObjName + " ";
                        inv.inventory["Blue"] = true;
                        inv.inventory[hitObjName] = true;
                        Destroy(hit.collider.gameObject);
                        Cmd_NetworkDestroy(hit.collider.gameObject);
                    }

                    else if (inv.inventory.ContainsKey(hitObjName))
                    {
                        InventoryItems += hitObjName + " ";
                        inv.inventory[hitObjName] = true;
                        Destroy(hit.collider.gameObject);
                        Cmd_NetworkDestroy(hit.collider.gameObject);
                    }
                }
            }

            if(hit.collider.tag == "Switch")
            {
                if (Input.GetButtonDown("Interact"))
                {
                    hit.collider.gameObject.GetComponent<Animator>().SetBool("Start", true);

                    foreach (GameObject obj in GameObject.FindGameObjectsWithTag("FinalDoor"))
                    {
                        if (obj.name == "FinalDoor")
                        {
                            InventoryItems += "HintFlashLight" + " ";
                            hit.collider.GetComponent<Animator>().SetBool("Start", true);
                            inv.inventory["HintFlashLight"] = true;
                            Cmd_NetworkDestroy(obj);
                            Destroy(obj);
                            
                        }
                    }
                }
            }

            if (hit.collider.name == "HeartRateMonitor" || hit.collider.tag == "HeartRateMonitor")
            {
                hit.collider.gameObject.GetComponent<HeartMonitor>().lookedAt();
            }

            if (InventoryItems.Contains("RedHint"))
            {
                Destroy(GameObject.FindGameObjectWithTag("SecurityBox"));
                Cmd_NetworkDestroy(GameObject.FindGameObjectWithTag("SecurityBox"));
            }

            if (hit.collider.gameObject.tag == "Larvae")
            {
                if (hit.collider.gameObject.GetComponent<Larvae>().activated && Input.GetButtonDown("Interact"))
                {
                    Destroy(hit.collider.gameObject);
                    Cmd_NetworkDestroy(hit.collider.gameObject);
                }
            }

            if (hit.collider.gameObject.tag == "ExitVent")
            {
                if (hit.collider.gameObject.name == "ExitSign" && hit.transform.parent.GetComponent<exitbox>().GetExitCondition())
                {
                    if (Input.GetButtonDown("Interact"))
                    {
                        print("ExitSign Pressed");
                        GameObject.Find("Spawn Room").transform.Find("Spawn Manager").GetComponent<SpawnManager>().EndGame(1);
                        //Cmd_SpawnAtEnd(1, gameObject);
                    }
                }
            }

            if (hit.collider.tag == "JumpScare" && flashlight.enabled)
            {
                hit.collider.GetComponent<JumpScare>().flashLightOn = true;
            }

            //KNOWN ISSUE
            //Sound plays if you look away from the computer while holding Interact
            if (hit.collider.gameObject.tag == "Computer")
            {
                //Computer comp = hit.collider.GetComponent<Computer>();
                ComputerOnDoor comp = hit.collider.GetComponent<ComputerOnDoor>();

                if (hit.transform.Find("GuideText") && (bool)inv.inventory[comp.colour] && comp.GetShowGuide())
                {
                    hit.transform.Find("GuideText").gameObject.SetActive(true);
                }

                if(comp.GetTime() > 0 && interactHeld && comp.name == "FinalLaptop")
                {
                    if(InventoryItems.Contains("YellowHint"))
                    {
                        comp.SetLoadingPlaying(true);
                        comp.SetTime(comp.GetTime() - Time.deltaTime);
                        interactHex.SetFrame((int)(comp.GetPercentageComplete() * (7 * 7)));
                        
                    }
                }

                else if (comp.GetTime() > 0 && interactHeld && (bool)inv.inventory[comp.colour])
                {
                    comp.SetLoadingPlaying(true);
                    comp.SetTime(comp.GetTime() - Time.deltaTime);
                    interactHex.SetFrame((int)(comp.GetPercentageComplete() * (7 * 7)));
                    //print((int)(comp.GetPercentageComplete() * (7 * 7)));
                    if (comp.missingText)
                        comp.missingText.GetComponent<Renderer>().enabled = false;
                    
                    if (comp.GetPercentageComplete() > 0.98f && !hit.collider.name.Contains("Final"))
                    {
                        Cmd_SetLaptopState(comp.gameObject, 1);
                    }


                    //if (comp.GetPercentageComplete() > 0.97f)
                    //{
                    //    if (comp.colour == "Red")
                    //    {
                    //        Cmd_SetLaptopColour(comp.gameObject, Color.red);
                    //        // comp.thisScreen.color = Color.red;
                    //    }
                    //    else if (comp.colour == "Blue")
                    //    {
                    //        Cmd_SetLaptopColour(comp.gameObject, Color.blue);
                    //        //comp.thisScreen.color = Color.blue;
                    //    }
                    //    else if (comp.colour == "Green")
                    //    {
                    //        Cmd_SetLaptopColour(comp.gameObject, Color.green);
                    //        //comp.thisScreen.color = Color.green;
                    //    }
                    //    else if (comp.colour == "Yellow")
                    //    {
                    //        Cmd_SetLaptopColour(comp.gameObject, Color.yellow);
                    //        //comp.thisScreen.color = Color.yellow;
                    //    }
                    //}

                }
                else if (!(bool)inv.inventory[comp.colour])
                {
                    Cmd_SetLaptopState(comp.gameObject, 2);
                    //if (comp.missingText)
                    //    comp.missingText.GetComponent<Renderer>().enabled = true;
                }

                

                else
                {
                    comp.SetLoadingPlaying(false);
                }
            }

            if (hit.collider.tag != "Computer")
                interactHex.SetFrame(0);


            if (!hasChosen)
            {
                if(hit.collider.name == "ReadyComp")
                {
                    if(Input.GetButtonDown("Interact"))
                    {
                        if(isServer)
                        {
                            character = "protag";
                            Cmd_NetworkDestroy(hit.collider.transform.parent.Find("Characters").Find("Woman").gameObject);
                            Cmd_NetworkSetRendererEnabled(hit.collider.transform.Find("P1_Ready").gameObject, true);
                            hasChosen = true;
                        }
                        else
                        {
                            character = "antag";
                            Cmd_NetworkDestroy(hit.collider.transform.parent.Find("Characters").Find("Xtro").gameObject);
                            Cmd_NetworkSetRendererEnabled(hit.collider.transform.Find("P2_Ready").gameObject, true);
                            hasChosen = true;

                        }



                        //hasChosen = true;
                        //Cmd_NetworkDestroy(hit.collider.transform.parent.Find("Characters").GetChild(0).gameObject);
                        //if (isServer)
                        //    hit.collider.transform.Find("P1_Ready").gameObject.SetActive(true);
                        //else
                        //    hit.collider.transform.Find("P2_Ready").gameObject.SetActive(true);

                    }
                }



                //if (hit.collider.name == "Woman")
                //{
                //    if (Input.GetButtonDown("Interact"))
                //    {
                //        character = "protag";
                //        hasChosen = true;
                //        Cmd_NetworkDestroy(hit.collider.gameObject);
                //    }
                //}

                //if (hit.collider.name == "Xtro")
                //    if (Input.GetButtonDown("Interact"))
                //    {
                //        character = "antag";
                //        hasChosen = true;
                //        Cmd_NetworkDestroy(hit.collider.gameObject);
                //    }
            }
        }
        else
        {
            interactHex.SetFrame(0);
        }

        RaycastAll(rayDirection);


        if (Input.GetButtonDown("MenuRight") && showMenu == true)
        {
            int listLength = -1;

            for (int i = 0; i < allHints.Length; i++)
            {
                if (InventoryItems.Contains(allHints[i].name))
                {
                    listLength = i;
                }
            }

            if (listLength >= 0)
            {
                allHints[shownHint].GetComponent<Renderer>().enabled = false;
                shownHint++;
                if (shownHint > listLength)
                    shownHint = 0;

                allHints[shownHint].GetComponent<Renderer>().enabled = true;
            }

            //allHints[shownHint].GetComponent<Renderer>().enabled = false;
            //shownHint = (shownHint > listLength) ? 0 : shownHint + 1;
            //allHints[shownHint].GetComponent<Renderer>().enabled = true;

            //print(shownHint);

            //for (int i = 0; i < ListLength; i++)
            //{
            //    int next = i + 1;

            //    if (next > ListLength)
            //    {
            //        next = 0;
            //    }


            //    Renderer hintRend = allHints[i].GetComponent<Renderer>();

            //    if (hintRend.enabled == true && InventoryItems.Contains(allHints[next].name))
            //    {
            //        hintRend.enabled = false;
            //        allHints[next].GetComponent<Renderer>().enabled = true;
            //    }
            //}
        }

        if (Input.GetButtonDown("MenuLeft") && showMenu == true)
        {
            int listLength = -1;

            for (int i = 0; i < allHints.Length; i++)
            {
                if (InventoryItems.Contains(allHints[i].name))
                {
                    listLength = i;
                }
            }

            if (listLength >= 0)
            {
                allHints[shownHint].GetComponent<Renderer>().enabled = false;
                shownHint--;
                if (shownHint < 0)
                    shownHint = listLength;

                allHints[shownHint].GetComponent<Renderer>().enabled = true;
            }
        }

        //    for (int i = 0; i < listLength; i++)
        //    {
        //        int Prev = i - 1;

        //        if (Prev < 0)
        //        {
        //            Prev = listLength - 1;
        //        }

        //        Renderer hintRend = allHints[i].GetComponent<Renderer>();

        //        if (hintRend.enabled == true && InventoryItems.Contains(allHints[Prev].name))
        //        {
        //            hintRend.enabled = false;
        //            allHints[Prev].GetComponent<Renderer>().enabled = true;
        //        }
        //    }
        //}
    }

    //Debug.DrawRay(origCam.transform.position, origCam.transform.forward, Color.black, 1); //unless you allow debug to be seen in game, this will only be viewable in the scene view)

    [Command]
    public void Cmd_DoDamage()
    {
        Rpc_DoDamage();
        StartCoroutine(ShowScreenOverlay(0.8f));

        //else
        //{
        //    foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        //    {
        //        player.GetComponent<BasePlayerController>().Cmd_SpawnAtEnd(false);
        //    }
        //}
    }

    [ClientRpc]
    public void Rpc_DoDamage()
    {
        if (!takenDamage)
        {
            takenDamage = true;
            speedModifier = 0.8f;
            FMODUnity.RuntimeManager.PlayOneShot(damageSound, transform.position);
            GetComponent<Animator>().SetBool("takenDamage", true);
        }
    }

    [Command]
    public void Cmd_DoBite(int biteAmount)
    {
        Rpc_DoBite(biteAmount);
        if (!screenOverlayShowing)
        {
            StartCoroutine(ShowScreenOverlay(0.1f));
        }    
    }

    [ClientRpc]
    public void Rpc_DoBite(int biteAmount)
    {
        if (ProtagHealth >0) ProtagHealth -= biteAmount;
        if ((ProtagHealth <= 50) && !takenDamage)
        {
            takenDamage = true;
            speedModifier = 0.8f;
            FMODUnity.RuntimeManager.PlayOneShot(damageSound, transform.position);
            GetComponent<Animator>().SetBool("takenDamage", true);
        }
        
    }

    public bool TooBitten()
    {
        return ProtagHealth <= 0;
    }

    [Command]
    public void Cmd_Kill()
    {
        hasDied = true;
        Rpc_Kill();
    }

    [ClientRpc]
    public void Rpc_Kill()
    {
        hasDied = true;
    }

    public bool IsDead()
    {
        return hasDied;
    }

    public bool isDamaged()
    {
        return takenDamage;
    }

    protected void EnableFlashlight()
    {
        if (Input.GetButtonDown("Flashlight"))
        {
            if (flashlight.enabled == false)
            {

                flashlight.enabled = true;
                flashlight.intensity = 3;
            }
            else
            {
                flashlight.enabled = false;
            }
        }
    
    }

    protected void ShowHints()
    {

        if ((bool)inv.inventory[allKeys[0].name])
        {
            allKeys[0].GetComponent<Renderer>().enabled = true;
        }
        if ((bool)inv.inventory[allKeys[1].name])
        {
            allKeys[1].GetComponent<Renderer>().enabled = true;
        }
        if ((bool)inv.inventory[allKeys[2].name])
        {
            allKeys[2].GetComponent<Renderer>().enabled = true;
        }
        if ((bool)inv.inventory[allKeys[3].name])
        {
            allKeys[3].GetComponent<Renderer>().enabled = true;
        }


        if ((bool)inv.inventory[allHints[0].name])
        {
            foreach (GameObject hint in allHints)
            {
                if (hint.name == "GreenHint")
                {
                    hint.GetComponent<Renderer>().enabled = true;
                }
            }

            if ((bool)inv.inventory[allHints[1].name])
            {
                foreach (GameObject obj in GameObject.FindGameObjectsWithTag("GameHints"))
                {
                    if (obj.name == "LB" || obj.name == "RB")
                    {
                        obj.GetComponent<Renderer>().enabled = true;
                    }
                }

                foreach (GameObject hint in allHints)
                {
                    if (hint.name == "BlueHint")
                    {
                        hint.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        hint.GetComponent<Renderer>().enabled = false;
                    }
                }
            }

            if (InventoryItems.Contains("RedHint"))
            {

                foreach (GameObject hint in allHints)
                {
                    if (hint.name == "RedHint")
                    {
                        hint.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        hint.GetComponent<Renderer>().enabled = false;
                    }
                }
            }

            if (InventoryItems.Contains("YellowHint"))
            {

                foreach (GameObject hint in allHints)
                {
                    if (hint.name == "YellowHint")
                    {
                        hint.GetComponent<Renderer>().enabled = true;
                    }
                    else
                    {
                        hint.GetComponent<Renderer>().enabled = false;
                    }
                }

            }

            if (tutorialsOn == false)
            {
                Tutorialoff();
            }

        }
    }

    protected void CheckInventory()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            print(inv.itemList);
            print(inv.inventory.Keys.ToString());
        }
    }

    protected void ShowMenu()
    {
        ShowMenu(transform.rotation.eulerAngles);
    }

    protected void ShowMenu(Vector3 rot)
    {
        showMenu = !showMenu;

        GetComponent<Animator>().SetBool("invOpen", showMenu);

        speedModifier = (showMenu ? 0 : 1);

        if (VRSettings.enabled)
            lookSensitivity = (showMenu ? 0 : startingLookSensitivity);

        //GameObject menu = transform.Find("Pause Menu").gameObject;
        Rigidbody rb = pauseMenu.GetComponent<Rigidbody>();
        rb.MoveRotation(rb.rotation * Quaternion.Euler(-rot));

        pauseMenu.SetActive(showMenu);

        /*if (tutorialsOn == false)
        {
            foreach (GameObject text in GameObject.FindGameObjectsWithTag("TutorialText"))
            {
                if (text.name == "GuideTexts")
                {
                    text.GetComponent<Renderer>().enabled = false;
                }
                
            }
        }
        else
        {
            foreach (GameObject text in GameObject.FindGameObjectsWithTag("TutorialText"))
            {
                text.GetComponent<Renderer>().enabled = false;   
            }
        }*/


        if (showMenu == true)
        {
            ShowHints();
        }

        else
        {
            foreach (GameObject text in GameObject.FindGameObjectsWithTag("GameHints"))
            {
                text.GetComponent<Renderer>().enabled = false;
            }
        }
    }

    IEnumerator Flickeroff(GameObject obj)
    {
        while (flickeringOn == true)
        {
            obj.GetComponent<Renderer>().enabled = true;
            yield return new WaitForSeconds(0.3f);
            obj.GetComponent<Renderer>().enabled = false;
            yield return new WaitForSeconds(0.3f);

        }
    }

    void OnTriggerEnter (Collider col)
    {
        if(col.name == "RespawnCube")
        {
            transform.position = TeleToRes(transform.position);
        }
    }
   
}
