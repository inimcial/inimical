﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody))]
public class FallFromCeiling : MonoBehaviour {

    Rigidbody rb;

    [SerializeField, FMODUnity.EventRef]
    private string scareStab = "event:/Ambience and Event Sounds/Stabbing Music Event";

    private bool hasPlayed = false;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        if (rb.useGravity)
            rb.useGravity = false;

    }
	
	void OnTriggerEnter(Collider other)
    {
        if(!hasPlayed)
            if (other.name.Contains("Protag"))
            {
                rb.useGravity = true;
                hasPlayed = true;

                if (other.gameObject.GetComponent<BasePlayerController>().isLocalPlayer)
                    FMODUnity.RuntimeManager.PlayOneShotAttached(scareStab, gameObject);

                if (gameObject.transform.Find("Light"))
                {
                    Destroy(gameObject.transform.Find("Light").gameObject, 5f);
                }
                    
            }
    }
}
