﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//Author LoungeKatt http://answers.unity3d.com/questions/376164/hide-a-light-for-a-camera.html 
//Edited by Oliver Nilssen

public class LightingChange : MonoBehaviour
{

    public List<Light> Lights;

    void OnPreCull()
    {
        foreach(Light light in Lights)
        if (light != null)
            light.enabled = false;
    }

    void OnPreRender()
    {
        foreach (Light light in Lights)
        {
            light.enabled = false;
        }
    }

    void OnPostRender()
    {
        foreach (Light light in Lights)
        {
            light.enabled = true;
        }
    }
}
