﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
using UnityEngine.SceneManagement;

public class SpawnManager : NetworkBehaviour {

    [SerializeField]
    private GameObject protag;
    [SerializeField]
    private GameObject antag;

    [SerializeField, SyncVar]
    private int timeTillStart;

    [SyncVar]
    private int playersConnected;

    private enum startType { NO, STARTING, STARTED };
    [SyncVar]
    private startType gameStarting;


    private GameObject wallText;
    private GameObject characterObj;
    private GameObject[] players;
    private CustomNetworkManager netMan;

    private WWW www;
    private string ip = "";

    public bool singlePlayer;

    void Awake()
    {
        netMan = transform.parent.GetComponent<CustomNetworkManager>();
        characterObj = transform.parent.Find("Contents").Find("Characters").gameObject;
        wallText = transform.parent.Find("Contents").Find("SpawnRoomText").gameObject;
        gameStarting = startType.NO;
        

        if(!singlePlayer)
        {
            www = new WWW("https://api.ipify.org/");
            while (!www.isDone)
            {
                //print(www.progress);
            }
            ip = "Your external IP is " + www.text;
            www.Dispose();
        }
    }


    void Update()
    {
        //netMan.playersConnected is always +1 to the actual number, for some reason?
        if (isServer)
            playersConnected = netMan.playersConnected - 3;

        //print(playersConnected);

        if (singlePlayer && gameStarting == startType.NO)
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            BasePlayerController pc = player.GetComponent<BasePlayerController>();
            pc.Cmd_SpawnProtag(player, protag);

            wallText.GetComponent<TextMesh>().text = "";
            gameStarting = startType.STARTED;
        }

        if (playersConnected == 1 && gameStarting == startType.NO)
        {
            //Do something with one person

            wallText.GetComponent<TextMesh>().text = "Waiting for second player...\n" + ip;
        }
        else if (playersConnected == 2 && gameStarting == startType.NO)
        {
            //Do something with two people
            characterObj.SetActive(true);
            wallText.GetComponent<TextMesh>().text = "Ready up at the computer.";

            players = GameObject.FindGameObjectsWithTag("Player");
            foreach (GameObject player in players)
            {
                BasePlayerController pc = player.GetComponent<BasePlayerController>();
                if (pc.character != "protag" && pc.character != "antag")
                    player.GetComponent<BasePlayerController>().character = "none";
            }

        }
        else if (gameStarting == startType.STARTING)
        {
            //Game starting
            wallText.GetComponent<TextMesh>().text = timeTillStart + " seconds remaining.";
            if (timeTillStart == 0)
            {
                gameStarting = startType.STARTED;
                wallText.GetComponent<TextMesh>().text = "";
                StartGame();
            }
        }
        else if (gameStarting == startType.STARTED)
        {
            //Game is started
        }
        else
        {
            //Something went wrong
            //wallText.GetComponent<TextMesh>().text = "Too many players connected, or something went terribly wrong";
        }
        

        if(characterObj.transform.childCount == 0 && gameStarting == startType.NO)
        {
            StartCoroutine(StartGameTimer());
        }
        
    }

    private IEnumerator StartGameTimer()
    {
        gameStarting = startType.STARTING;

        for(; timeTillStart > 0; timeTillStart--)
            yield return new WaitForSeconds(1);
    }

    private void StartGame()
    {
        //This is absolutly disgusting code. Let me walk you through it.

        if(isServer) //If the player is this host, continue on.
        {
            //General setup stuff, this is okay
            int playersSpawned = 0; 
            GameObject[] players = GameObject.FindGameObjectsWithTag("Player");
            string hostChar = "";

            string charToSpawn = ""; //Meant to be set locally but not fucking syncing.

            
            foreach (GameObject player in players) //Cycle though every player
            {
                BasePlayerController pc = player.GetComponent<BasePlayerController>();
                if (pc.isLocalPlayer)
                    hostChar = pc.character; //Work out who the host as selected
            }

            while (playersSpawned < 2) //So while both players haven't spawned
            {
                foreach (GameObject player in players) //Cycle through every player AGAIN
                {
                    BasePlayerController pc = player.GetComponent<BasePlayerController>();

                    if (playersSpawned < 1 && pc.isLocalPlayer) //If the player is the host AND the first one to spawn, skip spawning
                        continue;

                    if (playersSpawned == 1 && !pc.isLocalPlayer) //If the player isn't the host and HAS spawned, skip spawning again
                        continue;

                    if (!pc.isLocalPlayer) //If the player isn't the host
                    {
                        //Set the character to spawn as as the opposite to the host
                        if (hostChar == "antag")
                            charToSpawn = "protag";
                        else
                            charToSpawn = "antag";
                    }
                    else
                        charToSpawn = hostChar; //But if they are the host, spawn them as the host character.

                    if (charToSpawn == "antag") //Finally spawn them as the correct character
                        pc.Cmd_SpawnAntag(player, antag);
                    else
                        pc.Cmd_SpawnProtag(player, protag);

                    playersSpawned++; 

                } //Do it again till both have spawned 
            }

            //foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            //{
            //    BasePlayerController pc = player.GetComponent<BasePlayerController>();
            //    if(pc.character == "antag")
            //        pc.Cmd_SpawnAntag(player, antag);
            //    else
            //        pc.Cmd_SpawnProtag(player, protag);
                
            //}
        }
        //players = GameObject.FindGameObjectsWithTag("Player");
        //foreach(GameObject player in players)
        //{
        //    BasePlayerController pc = player.GetComponent<BasePlayerController>();
        //    if (pc.isLocalPlayer)
        //    {
        //        if(pc.character == "protag")
        //        {
        //            pc.Cmd_SpawnProtag(player, protag);
        //            break;
        //        }
        //        else if(pc.character == "antag")
        //        {
        //            pc.Cmd_SpawnAntag(player, antag);
        //            break;
        //        }
        //    }

        //}

        //foreach(GameObject player in players)
        //{
        //    BasePlayerController pc = player.GetComponent<BasePlayerController>();
        //    print(pc.character);
        //    if(pc.character == "protag")
        //    {
        //        print("Spawning protag for ID: " + Network.player.guid);
        //        pc.Cmd_SpawnProtag(player, protag);
        //    }
        //    else
        //    {
        //        print("Spawning antag  for ID: " + Network.player.guid);
        //        pc.Cmd_SpawnAntag(player, antag);
        //    }

        //}

    }

    public void EndGame(int winType)
    {
        if (!singlePlayer)
        {
            if (isServer)
            {
                int playersSpawned = 0;
                GameObject[] players = GameObject.FindGameObjectsWithTag("Player");

                while (playersSpawned < 2)
                {
                    foreach (GameObject player in players)
                    {
                        BasePlayerController pc = player.GetComponent<BasePlayerController>();

                        if (playersSpawned < 1 && pc.isLocalPlayer)
                            continue;

                        if (playersSpawned == 1 && !pc.isLocalPlayer)
                            continue;

                        pc.Cmd_SpawnAtEnd2(winType, protag);
                        playersSpawned++;

                    }
                }
            }
        }
        else
        {
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            player.GetComponent<BasePlayerController>().Cmd_SpawnAtEnd2(winType, protag);
        }

        //foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        //{
        //    player.transform.Find("Graphics").gameObject.SetActive(false);
        //    player.GetComponent<BasePlayerController>().speedModifier = 0;
        //}

    }
}
