﻿using UnityEngine;
using System.Collections;

public class AnagAISpawnScript : MonoBehaviour {

    
    public GameObject creature;
	public Transform[] spawnPoints;

    [SerializeField]
    private GameObject firstDoor;

	private bool health = true;
    private bool spawnedAI = false;
    private bool isSinglePlayer;
    private bool isUnlockedFirstDoor;


    void Spawn(){

		int spawnPointIndex = Random.Range (0, spawnPoints.Length);
		Instantiate (creature, spawnPoints [spawnPointIndex].position, spawnPoints [spawnPointIndex].rotation);
	}


	// Update is called once per frame
	void Update () {
        isSinglePlayer = !GameObject.Find("Spawn Room").GetComponent<MainMenu>().multiplayer;
        isUnlockedFirstDoor = firstDoor.GetComponent<ComputerOnDoor>().doneUnlock;
        if (!spawnedAI)
        {   
            if (isSinglePlayer && isUnlockedFirstDoor)
            {
                Spawn();
                spawnedAI = true;
            }
        }
        
	}
}
