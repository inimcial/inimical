﻿using UnityEngine;
using System.Collections;

public class SpriteSheet : MonoBehaviour {

    public int columns = 7;
    public int rows = 7;

    private int currentFrame = 5;

    private float xOff = 0;
    private float yOff = 0;
    private Vector2 offset = Vector2.zero;
    private Vector2 size;

    private Material matCopy;
    private Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
        matCopy = new Material(rend.material);
        rend.sharedMaterial = matCopy;

        size = new Vector2(1f / columns, 1f / rows);
        rend.sharedMaterial.SetTextureScale("_MainTex", size);
    }

    public void SetFrame(int frame)
    {
        currentFrame = frame + 4;
        AdvanceFrame();
    }

    public int GetFrame()
    {
        return currentFrame;
    }

    public void AdvanceFrame()
    {
        currentFrame++;

        xOff = (currentFrame % columns) * size.x;
        yOff = 1 - (currentFrame / rows) * size.y;

        offset.Set(xOff, yOff);

        rend.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

    public void RetreatFrame()
    {
        currentFrame -= 2;
        AdvanceFrame();
    }
}
