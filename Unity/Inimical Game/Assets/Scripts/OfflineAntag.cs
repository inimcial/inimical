﻿using UnityEngine;
using System.Collections;

public class OfflineAntag : MonoBehaviour {

    NavMeshAgent nav;
    private GameObject player;
    private Transform girl;
    private GameObject antagy;
    private Transform creature;
    private bool health = true;
    private bool protagInRange = false;
    private int waitCount = 0;
    private int waitLength;
    private bool notLookTooLong = true;

    private float viewAngle = 45F;

    private bool isShooting;
    private Rigidbody rb;

    private float attackCooldown;
    private bool canAttack;
    private bool doAttack;
    
    [SerializeField]
    private GameObject projectile;

    [SerializeField]
    private float attackChargeTime = 2.8f;
    private float startingAttackChargeTime;

    [SerializeField]
    private float attackRechargeRate = 20f;


    void Awake()
    {
        nav = GetComponent<NavMeshAgent>();
        nav.speed = 2;
        antagy = GameObject.FindGameObjectWithTag("AntagAI");
        creature = antagy.transform;
        waitLength = (int)Random.Range(90F, 180F);
        rb = GetComponent<Rigidbody>();

        attackCooldown = 100;
        canAttack = true;
        isShooting = false;
        startingAttackChargeTime = attackChargeTime;

        //Set the antag spit animation to be the same length as the charge time
        if (GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName("spit"))
            GetComponent<Animator>().speed = attackChargeTime / 2.8f;

    }

    void OnTriggerEnter (Collider other)
    {
        if (other.gameObject == player)
        {
            protagInRange = true;
        }
    }

    void OnTriggerExit (Collider other)
    {
        if (other.gameObject == player)
        {
            protagInRange = false;
        }
    }

    bool IsLookingAtObject(Transform looker, Transform target, float FOVAngle)
    {
        Vector3 direction = target.position - looker.position;
        float ang = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;
        float lookerAngle = looker.eulerAngles.y;
        float checkAngle = 0F;

        checkAngle = ang - lookerAngle;

        if (checkAngle < -180f) checkAngle = checkAngle + 360f;

        if (checkAngle <= FOVAngle * .5f && checkAngle >= -FOVAngle * .5f)
            return true;
        else
            return false;
    }


    private void Attack()
    {
        Transform projSpawn = transform.Find("ProjSpawn");
        GameObject proj = (GameObject)Instantiate(projectile, projSpawn.position, projSpawn.rotation);

        proj.GetComponent<Rigidbody>().AddForce(projSpawn.transform.forward * 10 + projSpawn.transform.up, ForceMode.Impulse);

        attackCooldown = 0;
        canAttack = false;
        isShooting = false;
        attackChargeTime = startingAttackChargeTime;

        GetComponent<Animator>().SetBool("isSpitting", false);
    }


    void Update () {

        //Find the protag
        if (!girl)
        {
            foreach (GameObject p in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (p.name.Contains("Protag"))
                {
                    player = p;
                    girl = player.transform;
                }
            }
        }

        if (GetComponent<Animator>().GetBool("isSpitting"))
            GetComponent<Animator>().SetBool("walk", false);
        else
            GetComponent<Animator>().SetBool("walk", true);

        if (health)
        {
            if (protagInRange)   // If in rangeof girl, then try to shoot
            {
                nav.SetDestination(girl.position);
                Vector3 targetPosition = new Vector3(girl.position.x, creature.position.y, girl.position.z);
                creature.LookAt(targetPosition);  //Face target

                if (!isShooting) //If the antag is not shooting, but can, then set shooting and do animation.
                {
                    if (canAttack)
                    {
                        isShooting = true;
                        GetComponent<Animator>().SetBool("isSpitting", true);
                    }
                    else
                    {
                        nav.speed = 1f;
                    }
                }
                else //if already shooting, drain the charge time and attack when done
                {
                    nav.speed = 0f;
                    attackChargeTime -= Time.deltaTime;

                    if (attackChargeTime <= 0)
                    {
                        Attack();
                    }
                }

                //Recharge the attack cooldown
                if (attackCooldown <= 100)
                {
                    attackCooldown += attackRechargeRate * Time.deltaTime;
                }
                else //Otherwise it can attack
                {
                    canAttack = true;
                }
            }
            else if (IsLookingAtObject(girl, creature, viewAngle))
            {
               

                if (notLookTooLong)
                {
                    nav.SetDestination(creature.position);
                    waitCount++;
                    if (waitCount > waitLength)
                    {
                        notLookTooLong = false;
                        waitLength = (int)Random.Range(90f, 180f);
                        waitCount = waitLength;
                    }
                }
                else
                {

                    nav.SetDestination(girl.position);
                    nav.speed = 5f;
                }
            }
            else
            {
                if (waitCount > 0) waitCount = waitCount - 2;
                notLookTooLong = true;
                if (waitCount <= 0) nav.speed = 2f;
                nav.SetDestination(girl.position);
            }
        }

    }

    /* void FixedUpdate()
    {
        Ray rayDirection = new Ray(transform.position, transform.forward);
        RaycastHit hit;

        if (Physics.Raycast(rayDirection, out hit, 3.0f))
        {
            //Does the antag AI need raycasts?
        }
    } */

}
