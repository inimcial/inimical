﻿using UnityEngine;
using System.Collections;

public class Computer : MonoBehaviour {
    private enum Colour { Red, Green, Blue, Yellow, GreenHint};
    [SerializeField]
    private Colour compColour;

    public string colour;

    [SerializeField]
    private float interactTime = 2;
    private float startingInteractTime;

    [SerializeField][FMODUnity.EventRef]
    private string loadingAudio = "event:/ComputerLoad";
    private FMOD.Studio.EventInstance loadEv;

    [SerializeField][FMODUnity.EventRef]
    private string positiveAudio = "event:/ComputerSuccess";

    private bool doneUnlock = false;

    private GameObject[] doors;

    private bool showGuide = true;

    void Start()
    {
        colour = compColour.ToString();
        doors = GameObject.FindGameObjectsWithTag("Door");
        startingInteractTime = interactTime;

        loadEv = FMODUnity.RuntimeManager.CreateInstance(loadingAudio);
        FMOD.ATTRIBUTES_3D loadEvAttrib = new FMOD.ATTRIBUTES_3D();
        FMOD.VECTOR loadEvAttribVec = new FMOD.VECTOR();
        loadEvAttribVec.x = transform.position.x;
        loadEvAttribVec.y = transform.position.y;
        loadEvAttribVec.z = transform.position.z;
        loadEvAttrib.position = loadEvAttribVec;
        loadEv.set3DAttributes(loadEvAttrib);

        loadEv.start();
        loadEv.setPaused(true);
    }

    void Update()
    {
        if(!doneUnlock && interactTime <= 0)
        {
            FMODUnity.RuntimeManager.PlayOneShot(positiveAudio, transform.position);
            DoDoorUnlock();
            doneUnlock = true;


            if(transform.FindChild("GuideText"))
            {
                transform.FindChild("GuideText").gameObject.SetActive(false);
                showGuide = false;
            }
        }
    }

    private void DoDoorUnlock()
    {
        foreach(GameObject p in GameObject.FindGameObjectsWithTag("Player"))
        {
            p.GetComponent<BasePlayerController>().Cmd_UnlockDoor(doors, colour);
        }        
    }

    public void SetTime(float newTime)
    {
        interactTime = newTime;
    }

    public float GetTime()
    {
        return interactTime;
    }

    public void SetLoadingPlaying(bool enabled)
    {
        loadEv.setPaused(!enabled);
    }

    public float GetPercentageComplete()
    {
        return 1 - (interactTime / startingInteractTime);
    }

    public bool GetShowGuide()
    {
        return showGuide;
    }

 //   public bool activated;
 //   private bool doneDoorUnlock;
 //   public string colour;
 //   private AudioSource audioSource;
 //   [SerializeField]
 //   private AudioClip positiveAudio;
 //   [SerializeField]
 //   private AudioClip negativeAudio;
 //   [SerializeField]
 //   private AudioClip loadingAudio;
 //   [SerializeField]
 //   private float doorUnlockTime;

	//// Use this for initialization
	//void Start () {
 //       colour = name.Split('_')[1];
 //       GetComponent<Renderer>().material.color = (colour == "Green" ? Color.green : (colour == "Blue" ? Color.blue : Color.red));
 //       doneDoorUnlock = false;
 //       audioSource = GetComponent<AudioSource>();
	//}
	
	//// Update is called once per frame
	//void Update () {
	//    if(activated && !doneDoorUnlock)
 //       {
 //           StartCoroutine(loadDoorKeycard(doorUnlockTime));
 //           doneDoorUnlock = true;
 //       }
	//}

 //   IEnumerator loadDoorKeycard(float seconds)
 //   {
 //       print("Keycard loaded");
 //       audioSource.clip = loadingAudio;
 //       audioSource.Play();
 //       yield return new WaitForSeconds(seconds-1);
 //       audioSource.Stop();
 //       audioSource.clip = positiveAudio;
 //       audioSource.Play();
 //       UnlockDoor();
 //       yield return new WaitForSeconds(1);
 //   }

 //   void UnlockDoor()
 //   {
 //       print("activated");
 //       if (colour == "Green")
 //       {
 //           foreach (GameObject door in GameObject.FindGameObjectsWithTag("Door"))
 //           {
	//			if (door.GetComponent<NewDoors> ().greenCard) {
	//				door.GetComponent<NewDoors> ().greenCard = false;
	//				for (int i = 0; i < door.transform.childCount; i++) {
	//					if (door.transform.GetChild (i).name.Contains ("LightSquare")) {
	//						door.transform.GetChild (i).FindChild ("Spotlight").GetComponent<Light> ().color = Color.green;
	//						door.transform.GetChild (i).FindChild ("Spotlight (1)").GetComponent<Light> ().color = Color.green;
	//					}
	//				}
	//			}
 //           }
 //       }
 //       if (colour == "Blue")
 //       {
 //           foreach (GameObject door in GameObject.FindGameObjectsWithTag("Door"))
 //           {
	//			if (door.GetComponent<NewDoors> ().blueCard) {
 //               	door.GetComponent<NewDoors>().blueCard = false;
	//				for (int i = 0; i < door.transform.childCount; i++) {
	//					if (door.transform.GetChild (i).name.Contains ("LightSquare")) {
	//						door.transform.GetChild (i).FindChild ("Spotlight").GetComponent<Light> ().color = Color.blue;
	//						door.transform.GetChild (i).FindChild ("Spotlight (1)").GetComponent<Light> ().color = Color.blue;
	//					}
	//				}
	//			}
 //           }
 //       }
 //       if (colour == "Red")
 //       {
 //           foreach (GameObject door in GameObject.FindGameObjectsWithTag("Door"))
 //           {
	//			if (door.GetComponent<NewDoors> ().redCard) {
	//				door.GetComponent<NewDoors> ().redCard = false;
	//				for (int i = 0; i < door.transform.childCount; i++) {
	//					if (door.transform.GetChild (i).name.Contains ("LightSquare")) {
	//						door.transform.GetChild (i).FindChild ("Spotlight").GetComponent<Light> ().color = Color.red;
	//						door.transform.GetChild (i).FindChild ("Spotlight (1)").GetComponent<Light> ().color = Color.red;
	//					}
	//				}
	//			}
 //           }
 //       }
 //   }
}
