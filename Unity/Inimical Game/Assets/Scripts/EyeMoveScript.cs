﻿using UnityEngine;
using System.Collections;

public class EyeMoveScript : MonoBehaviour {

    [SerializeField]
    Transform leftEyeLid;
    [SerializeField]
    Transform rightEyeLid;
    [SerializeField]
    Transform leftEye;
    [SerializeField]
    Transform rightEye;
    private float smooth = 12.0F;
    private float tiltAngle = 44.0F;
    private bool blink = false;
    private int eyeLidMove = 0;
    private float eyeDartX = 0;
    private float eyeDartY = 0;
    
	//void Awake()
 //   {
 //       leftEyeLid = GameObject.FindWithTag("EyeLidLeft").transform;
 //       rightEyeLid = GameObject.FindWithTag("EyeLidRight").transform;
 //       leftEye = GameObject.FindWithTag("ProtagLeftEye").transform;
 //       rightEye = GameObject.FindWithTag("ProtagRightEye").transform;
 //   }


    // Update is called once per frame
    void Update()
    {

        // *** Blinking ***
        if (blink)
        {
            eyeLidMove++;
            if (eyeLidMove < 15)
            {
                tiltAngle = 44.0F;
            }
            else
            {
                tiltAngle = 0.0F;
            }
            if (eyeLidMove > 30)
            {
                eyeLidMove = 0;
                blink = false;
            }
            Quaternion targetL = Quaternion.Euler(tiltAngle, 0F, 0F);
            Quaternion targetR = Quaternion.Euler(tiltAngle + 180F, 0F, 0F);
            leftEyeLid.localRotation = Quaternion.Slerp(leftEyeLid.localRotation, targetL, Time.deltaTime * smooth);
            rightEyeLid.localRotation = Quaternion.Slerp(rightEyeLid.localRotation, targetR, Time.deltaTime * smooth);
        }
        else
        {
             blink = Random.value > 0.995F;
        }

        // *** Random Eye Darting ***
        if (Random.value > 0.98)
        {
            eyeDartX = Random.Range(-3.0F, 3.0F);
            eyeDartY = Random.Range(-3.0F, 3.0F);
        }
        Quaternion targetL2 = Quaternion.Euler(eyeDartX, eyeDartY, 0F);
        Quaternion targetR2 = Quaternion.Euler(eyeDartX + 180F, eyeDartY, 0F);
        leftEye.localRotation = Quaternion.Slerp(leftEye.localRotation, targetL2, Time.deltaTime * smooth);
        rightEye.localRotation = Quaternion.Slerp(rightEye.localRotation, targetR2, Time.deltaTime * smooth);
    }

}
