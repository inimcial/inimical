﻿using UnityEngine;
using System.Collections;

public class PASystem : MonoBehaviour {

    public int sound;
    public float secondsToWait;

    [SerializeField][FMODUnity.EventRef]
    private string paSystem = "event:/PA System";
    FMOD.Studio.EventInstance paSysEv;
    FMOD.Studio.ParameterInstance paSysParam;

	// Use this for initialization
	void Start () {

        paSysEv = FMODUnity.RuntimeManager.CreateInstance(paSystem);
        paSysEv.getParameter("Line", out paSysParam);


        FMOD.ATTRIBUTES_3D paSysEvAttrib = new FMOD.ATTRIBUTES_3D();
        FMOD.VECTOR paSysEvAttribVec = new FMOD.VECTOR();
        paSysEvAttribVec.x = transform.position.x;
        paSysEvAttribVec.y = transform.position.y;
        paSysEvAttribVec.z = transform.position.z;
        paSysEvAttrib.position = paSysEvAttribVec;
        paSysEv.set3DAttributes(paSysEvAttrib);

        paSysParam.setValue(sound);

        StartCoroutine(PlayPaSound());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    IEnumerator PlayPaSound()
    {
        yield return new WaitForSeconds(secondsToWait);
        paSysEv.start();
    }
}
