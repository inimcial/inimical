﻿using UnityEngine;
using System.Collections;

public class MRIAnimControl : MonoBehaviour {

	public float framesPerSecond = 20f;

	private int columns = 8;
	private int rows = 4;
	private float iX=0;
	private float iY=0;
	private Vector2 _size;
	private Renderer _myRenderer;
	private int _lastIndex = -1;

	// initialization
	void Start () {
		_size = new Vector2 (0.125f, 0.25f); //sprite sheet is 8 x 4
		_myRenderer = GetComponent<Renderer>();
		if (_myRenderer == null) enabled = false;
		// _myRenderer.material.SetTextureScale ("_MainTex", _size);
	}

	// Update is called once per frame
	void Update () {

		int index = (int)(Time.timeSinceLevelLoad * framesPerSecond) % (columns * rows);

		if(index != _lastIndex)
		{
			Vector2 offset = new Vector2(iX*_size.x,
				-(_size.y*iY));
			iX++;
			if(iX / columns >= 1)
			{
				iX = 0;
				iY++;
				if (iY / rows >= 1) {
					iY = 0;
				}

			}

			_myRenderer.materials[0].SetTextureOffset ("_MainTex", offset);


			_lastIndex = index;
		}
	}
}
