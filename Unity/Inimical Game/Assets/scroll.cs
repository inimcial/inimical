﻿using UnityEngine;
using System.Collections;

public class scroll : MonoBehaviour {

    private GameObject textfield;
    private RectTransform rt;
    private float height = 5.0f;

    [SerializeField]
    private float scrollSpeed = 10.0f;

    // Use this for initialization
    void Start () {
        textfield = this.gameObject;
        rt = textfield.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(232, 5);

    }

    // Update is called once per frame
    void Update () {
        
        if (height <= 255)
        {
            rt.sizeDelta = new Vector2 (232, height += scrollSpeed * Time.deltaTime);
        }
    }
}
