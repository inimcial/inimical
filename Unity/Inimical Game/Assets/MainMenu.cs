﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.VR;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public bool multiplayer;

    [SerializeField]
    private Texture tiredAndWiredLogo;
    [SerializeField]
    private Texture inimicalLogo;
    [SerializeField]
    private Texture oculusMsgTex;
    [SerializeField]
    private Texture CreditsText;


    [SerializeField]    
    private GUIStyle HostButton;
    [SerializeField]
    private GUIStyle JoinButton;
    [SerializeField]
    private GUIStyle SettingButton;
    [SerializeField]
    private GUIStyle ExitButton;
    [SerializeField]
    private GUIStyle SingleplayerButton;
    [SerializeField]
    private GUIStyle ConnectButton;
    [SerializeField]
    private GUIStyle BackButton;
    [SerializeField]
    private GUIStyle CreditButton;
    [SerializeField]
    private GUIStyle LogoStyle;
    

    [SerializeField]
    private GUISkin guiSkin;

    //[SerializeField]
    //private GUIStyle guiStyle;

    //EventSystem Eventsys;


    private CustomNetworkManager manager;

    private enum MenuTypes {STORY, MAIN, IP, SETTINGS, MESSAGE, CREDITS};
    private MenuTypes currMenu;

    private float hSliderValue = 0.03f;

    private string ip = "Please Enter an IP";
    private string port;

    private float buttonWidth;
    private float buttonHeight;
    private float offset;
    private float spacing;

    private int startType;

    private bool hasDoneOnce = false;

    void Awake()
    {

        //Eventsys = FindObjectOfType<EventSystem>();
        manager = GetComponent<CustomNetworkManager>();
        currMenu = MenuTypes.STORY;

        buttonWidth = Screen.width / 5;
        buttonHeight = buttonWidth / 3.33f;
        offset = buttonHeight + (Screen.height / 30);
        spacing = 10;
        multiplayer = true;
        startType = 0;

        if (VRDevice.isPresent)
        {
            VRSettings.showDeviceView = false;
            InputTracking.Recenter();
        }
        else
        {
            GameObject.Find("VrText").transform.parent.parent.gameObject.SetActive(false);
        }

    }

    void Update()
    {

        bool noConnection = (manager.client == null || manager.client.connection == null ||
                                 manager.client.connection.connectionId == -1);

        if( noConnection )
        {
            if (currMenu == MenuTypes.STORY)
            {
                if (Input.GetButtonDown("Interact"))
                {
                    Camera.main.transform.position -= new Vector3(0, 3, 0);
                    currMenu = MenuTypes.MAIN;
                    
                }

            }
            else if (currMenu == MenuTypes.MESSAGE)
            {
                Camera.main.GetComponent<Light>().enabled = false;
                if (!hasDoneOnce && VRDevice.isPresent)
                {
                    GameObject.Find("VrText").transform.parent.Find("Button").gameObject.SetActive(true);
                    GameObject.Find("VrText").gameObject.SetActive(false);
                    hasDoneOnce = true;
                }


                if (Input.GetButtonDown("Interact") || !VRDevice.isPresent)
                {

                    if (VRDevice.isPresent)
                        VRSettings.showDeviceView = true;

                    if (startType == 0) //Hosting
                    {
                        manager.StartHost();
                    }
                    else if(startType == 1) //Joining
                    {
                        manager.StartClient();
                    }
                    else if(startType == 2) //Single Player
                    {
                        manager.playersConnected += 1;
                        print(manager.playersConnected);
                        transform.Find("Spawn Manager").GetComponent<SpawnManager>().singlePlayer = true;
                        foreach (GameObject baby in GameObject.FindGameObjectsWithTag("Baby"))
                        {
                            baby.transform.GetChild(0).gameObject.SetActive(true);
                        }

                        multiplayer = false;    
                        manager.StartHost();
                    }
                }
            }
            else
            {
                Camera.main.GetComponent<Light>().enabled = true;
            }

              
        }
        
    }

    void OnGUI()
    {

        CheckTextures();

        GUI.skin = guiSkin;

        bool noConnection = (manager.client == null || manager.client.connection == null ||
                                 manager.client.connection.connectionId == -1);

        buttonWidth = Screen.width / 5;
        buttonHeight = buttonWidth / 3.33f;
        offset = buttonHeight + (Screen.height / 30);
        spacing = 10;

        if (!manager.IsClientConnected() && !NetworkServer.active)
        {
            if (noConnection)
            {
                if (currMenu == MenuTypes.MAIN)
                {
                    GUI.Box(new Rect(Screen.width - 400, 0, 400, 250), inimicalLogo, LogoStyle);
                    GUI.Box(new Rect(Screen.width - 200, Screen.height - 100, 200, 100), tiredAndWiredLogo, LogoStyle);

                    //GUI.SetNextControlName("JoinButton");
                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "JoinButton", JoinButton))
                    {
                        
                        currMenu = MenuTypes.IP;
                        
                    }
                    //GUI.FocusControl("JoinButton");

                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", HostButton))
                    {

                        startType = 0;
                        currMenu = MenuTypes.MESSAGE;
                        //manager.StartHost();

                    }
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", SingleplayerButton))
                    {
                        startType = 2;
                        currMenu = MenuTypes.MESSAGE;
                        //manager.playersConnected += 1;
                        //transform.Find("Spawn Manager").GetComponent<SpawnManager>().singlePlayer = true;
                        //foreach (GameObject baby in GameObject.FindGameObjectsWithTag("Baby"))
                        //{
                        //    baby.transform.GetChild(0).gameObject.SetActive(true);
                        //}
                        //multiplayer = false;
                        //manager.StartHost();
                    }
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", SettingButton))
                    {
                        currMenu = MenuTypes.SETTINGS;
                    }
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", CreditButton))
                    {
                        currMenu = MenuTypes.CREDITS;
                    }

                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", ExitButton))
                    {
                        Application.Quit();
                    }
                    spacing += offset;
                }
                else if (currMenu == MenuTypes.SETTINGS)
                {
                    GUI.Label(new Rect(25, 80, 250, 30), "Control Brightness");
                    hSliderValue = GUI.HorizontalSlider(new Rect(25, 100, 350, 75), hSliderValue, 0.0f, 0.1f);
                    RenderSettings.ambientLight = new Color(hSliderValue, hSliderValue, hSliderValue);

                    if (GUI.Button(new Rect(10, 360, buttonWidth, buttonHeight), "", BackButton))
                    {
                        currMenu = MenuTypes.MAIN;
                    }
                }
                else if (currMenu == MenuTypes.IP)
                {
                    GUI.SetNextControlName("ipField");
                    ip = GUI.TextField(new Rect(10, spacing, buttonWidth, buttonHeight), ip);
                    GUI.FocusControl("ipField");
                    if (ip.Split('.').Length == 4)
                    {
                        if (ip.Contains(":"))
                        {
                            manager.networkAddress = ip.Split(':')[0];

                            int port = 7777;
                            if (int.TryParse(ip.Split(':')[1], out port))
                                manager.networkPort = port;
                        }
                        else
                        {
                            manager.networkAddress = ip;
                            manager.networkPort = 7777;
                        }
                        GUI.enabled = true;
                    }
                    else
                    {
                        GUI.enabled = false;
                    }
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", ConnectButton))
                    {
                        startType = 1;
                        currMenu = MenuTypes.MESSAGE;
                        //manager.StartClient();
                    }

                    GUI.enabled = true;

                    spacing += offset;
                    spacing += offset;
                    spacing += offset;
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", BackButton))
                    {
                        currMenu = MenuTypes.MAIN;
                    }

                }
                else if (currMenu == MenuTypes.MESSAGE)
                {
                    GUI.Label(new Rect(Screen.width / 4, Screen.height / 2, Screen.width / 2, Screen.height / 10), oculusMsgTex); //"Please put on your Oculus Rift Headset");
                                                                                                                                  //GUI.Label(new Rect(Screen.width / 2 - 20, Screen.height / 2 + 150, 300, 50), "Press A to Continue");

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", BackButton))
                    {
                        currMenu = MenuTypes.MAIN;
                    }
                }


                else if (currMenu == MenuTypes.CREDITS)
                {
                    GUI.Label(new Rect(Screen.width / 4, Screen.height / 5, Screen.width / 2, Screen.height / 2), CreditsText);

                    spacing += offset;
                    spacing += offset;
                    spacing += offset;
                    spacing += offset;

                    if (GUI.Button(new Rect(10, spacing, buttonWidth, buttonHeight), "", BackButton))
                    {
                        currMenu = MenuTypes.MAIN;
                    }
                }
            }
        }
    }



    private void CheckTextures()
    {
        if (!tiredAndWiredLogo)
            Debug.LogError("Tired and Wired Logo not assigned.");
        if (!inimicalLogo)
            Debug.LogError("Inimical Logo not assigned.");
        if (!oculusMsgTex)
            Debug.LogError("Oculus message not assigned");
    }

}

