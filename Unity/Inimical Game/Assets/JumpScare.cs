﻿using UnityEngine;
using System.Collections;

public class JumpScare : MonoBehaviour {

   

    [SerializeField, FMODUnity.EventRef]
    private string scareStab = "event:/Ambience and Event Sounds/Stabbing Music Event";

    private bool hasPlayed = false;
    Animator anim;
    private bool isInTrigger = false;
    public bool flashLightOn = false;

    GameObject player;
    
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    void Update()
    {
        if(isInTrigger && flashLightOn && !hasPlayed)
        {
            PlayScare();
        }

    }

    
    void LateUpdate () {
        //if (hasPlayed)
        //    anim.SetBool("Start", false);

        if (name == "MUTANTWOMAN_AND_BABY" && anim.GetBool("Start"))
            anim.SetBool("Start", false);


    }

    void PlayScare()
    {
        anim.SetBool("Start", true);
        hasPlayed = true;

        if (player.GetComponent<BasePlayerController>().isLocalPlayer)
            FMODUnity.RuntimeManager.PlayOneShotAttached(scareStab, gameObject);
    }

    void OnTriggerEnter(Collider other)
    { 
        if (tag != "JumpScare")
        {
            if (!hasPlayed)
            {
                if (other.name.Contains("Protag"))
                {
                    isInTrigger = true;
                    player = other.gameObject;
                    PlayScare();

                }
            }
        }

        else if (other.name.Contains("Protag"))
        {
            isInTrigger = true;
            player = other.gameObject;

        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name.Contains("Protag"))
        {
            isInTrigger = false;

        }
    }
}
