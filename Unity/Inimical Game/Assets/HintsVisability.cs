﻿using UnityEngine;
using System.Collections;

public class HintsVisability : MonoBehaviour {

    protected ProtagController protag;
    protected Renderer thisobject;
    public bool hasEntered = false;



    // Use this for initialization
    void Start () {

        thisobject = GetComponent<Renderer>();
        
        if (name == "SecurityBoxDoor" || name == "FinalDoor")
        {
            thisobject.enabled = true;
        }
        else
        {
            thisobject.enabled = false;

            if (gameObject.GetComponentsInChildren<Transform>() != null)
            {
                foreach (Renderer child in gameObject.GetComponentsInChildren<Renderer>())
                {
                    child.enabled = false;
                }
            }
            
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (!protag)
        {
            foreach (GameObject player in GameObject.FindGameObjectsWithTag("Player"))
            {
                if (player.name.Contains("Protag_"))
                {
                    protag = player.GetComponent<ProtagController>();
                }
            }
        }

        else
        {
            if (name == "BlueHint")
            {
                if (protag.InventoryItems.Contains("GreenHint"))
                {
                    thisobject.enabled = true;

                    if (gameObject.GetComponentsInChildren<Transform>() != null)
                    {
                        foreach (Renderer child in gameObject.GetComponentsInChildren<Renderer>())
                        {
                            child.enabled = true;
                        }
                    }
                }
            }

            if (name == "RedHint")
            {
                if (protag.InventoryItems.Contains("BlueHint"))
                {
                    thisobject.enabled = true;

                    if (gameObject.GetComponentsInChildren<Transform>() != null)
                    {
                        foreach (Renderer child in gameObject.GetComponentsInChildren<Renderer>())
                        {
                            child.enabled = true;
                        }
                    }
                }
            }

            if (name == "YellowHint")
            {
                if (protag.InventoryItems.Contains("HintFlashLight"))
                {
                    thisobject.enabled = true;

                    if (gameObject.GetComponentsInChildren<Transform>() != null)
                    {
                        foreach (Renderer child in gameObject.GetComponentsInChildren<Renderer>())
                        {
                            child.enabled = true;
                        }
                    }
                }
            }

            if (name == "SecurityBoxDoor")
            {
                if (protag.InventoryItems.Contains("RedHint"))
                {
                    thisobject.enabled = false;
                }
            }

            if (name == "FinalDoor")
            {
                if (protag.InventoryItems.Contains("HintFlashLight") && hasEntered == true)
                {
                    gameObject.SetActive(false);
                    Destroy(gameObject);
                }
            }
        }
    }

    void OnTriggerEnter (Collider col)
    {
        hasEntered = true;
    }
}
