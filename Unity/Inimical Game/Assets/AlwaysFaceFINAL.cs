﻿using UnityEngine;
using System.Collections;

public class AlwaysFaceFINAL : MonoBehaviour {
   
    public bool hasdoor;
    private GameObject Door;
    public string doorname;

    void Update()
    {
        if (!Door) {
            Door = GameObject.FindGameObjectWithTag("FinalDoor");
            hasdoor = false;
            doorname = Door.ToString();

        }else {

            transform.LookAt(Door.transform, Vector3.zero);
            hasdoor = true;
            doorname = Door.ToString();

       }
    }

}