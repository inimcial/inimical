﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ReloadLevel : MonoBehaviour {

    public void RestartLevel()
    {
        foreach(GameObject player in GameObject.FindGameObjectsWithTag("Player"))
        {
            player.transform.Find("Graphics").gameObject.SetActive(false);
        }

        StartCoroutine(DoReload());
    }

    private IEnumerator DoReload()
    {
        GameObject spawn = GameObject.Find("Spawn Room");
        yield return new WaitForSeconds(11);

        CustomNetworkManager manager = spawn.GetComponent<CustomNetworkManager>();

        Network.SetSendingEnabled(0, false);
        Network.isMessageQueueRunning = false;


        manager.StopServer();
        manager.StopClient();
        
        Network.Disconnect();
        Destroy(spawn);
        SceneManager.UnloadScene(0);
        SceneManager.LoadScene(0);
    }


}
